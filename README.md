# Mt. Sinai School of Medicine NGS Scripts and Support Infrastructure

## Overview

This package provides a number tools, scripts, Makefiles, etc. for automating
NGS pipelines at Mt. Sinai; specifically

* scripts

    Helper scripts for setting up you project environment, running GATK in
    standalone mode, file conversion, etc.

* Makefiles

    Makefiles for automating the complete DNA workflow. You typically do not
    interact with these Makefiles directly, instead they are include in a
    project specific Makefile you create in your project working directory

* queue

    A Mt. Sinai-customized wrapper around GATK-Queue and a series of qscripts
    for implementing standarized DNA and RNA analysis pipelines

* skeletons

    Templated directories for running pipeline validation and other tasks

* test

    Unit tests and associated input files for different components of the
    pipeline, validation infrastructure, etc.

## Using the DNA Pipeline

The *ngs* package provides a standardized DNA analysis pipeline based on the
current GATK best-practices pipeline (along with pipelines for RNASeq and
Pacific Biosciences RS-generated data). The current and best documentation for
using the pipeline can be found on the [wiki page](https://bitbucket.org/multiscale/ngs/wiki/Home).

## Developing the DNA and Other Pipelines

Please see the [wiki page](https://bitbucket.org/multiscale/ngs/wiki/Home) for
the current and best documentation on working on the various pipelines (instead
of working with them). A formal test infrastructure is in place for different
pipeline components (using [Roundup](http://bmizerany.github.com/roundup/)).
