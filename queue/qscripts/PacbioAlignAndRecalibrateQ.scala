import edu.mssm.queue.util._
import edu.mssm.queue.util.PEFastQFiles
import edu.mssm.queue.util.SEFastQFile
import net.sf.samtools.SAMFileHeader.SortOrder
import org.broadinstitute.sting.commandline.Hidden
import org.broadinstitute.sting.gatk.walkers.indels.IndelRealigner.ConsensusDeterminationModel
import org.broadinstitute.sting.queue.extensions.gatk.{AnalyzeCovariates, TableRecalibration, CountCovariates}
import org.broadinstitute.sting.queue.extensions.picard.{MergeSamFiles, AddOrReplaceReadGroups}
import org.broadinstitute.sting.queue.function.ListWriterFunction
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.utils.baq.BAQ.CalculationMode

/*
* Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
*
* License available in LICENSE.txt.
*/


class PacbioAlignAndRecalibrateQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input file list (with sample metadata)", fullName="input", shortName="I", required=true)
  var input: File = _

  /* Optional Parameters */

  @Input(doc="Path of bwa binary", fullName="path_to_bwa", shortName="bwa", required=false)
  var bwaPath: File = "bwa"

  @Input(doc="The default base qualities to use before recalibration" , shortName = "dbq", required=false)
  var dbq: Int = 20

  // Override the default value for expandIntervals
  expandIntervals = 500

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  def performAlignment(alignableInputs: Seq[AlignableInput]): Map[String, Seq[File]] = {
    val sampleTable = scala.collection.mutable.Map.empty[String, Seq[File]]

    for (alignableInput <- alignableInputs) {

      val alignedBamFile: File = alignableInput match {
        case PBFastQFile(fastq, _) =>
          val samFile = swapExt(outputDir, fastq, QueueUtils.getReadFileExtension(fastq), ".sam")
          val alignedBamFile = swapExt(outputDir, samFile, ".sam", ".bam")
          add(
            bwa_bwasw(fastq, samFile),
            addReadGroup(samFile, alignedBamFile, alignableInput.readGroup)  // addReadGroup also sorts...
          )
          alignedBamFile
        case _ =>
          assert(false, "File type of alignable input not supported by this script")
          null
      }

      // Keep track of BAM files associated with each sample
      val sample = alignableInput.readGroup.sample
      if (!sampleTable.contains(sample))
        sampleTable(sample) = Seq(alignedBamFile)
      else if (!sampleTable(sample).contains(alignedBamFile))
        sampleTable(sample) :+= alignedBamFile
    }

    sampleTable.toMap
  }

  def performRecalibration(bamFilesBySample: Map[String, Seq[File]]): Seq[File] = {
    var recalibratedBamFiles: Seq[File] = Seq()

    for ((sample, bamFiles) <- bamFilesBySample) {
      val bamFile = new File(outputDir, qscript.projectName + "." + sample + ".bam")

      add( joinBams(bamFiles, bamFile) )


      val preRecalFile    = swapExt(outputDir, bamFile, ".bam", ".pre_recal.csv")
      val postRecalFile   = swapExt(outputDir, bamFile, ".bam", ".post_recal.csv")
      val preOutPath      = swapExt(outputDir, bamFile, ".bam", ".pre")
      val postOutPath     = swapExt(outputDir, bamFile, ".bam", ".post")

      val recalBamFile   = swapExt(outputDir, bamFile, ".bam", ".recal.bam")

      add(
        countCovariates(bamFile, preRecalFile),
        recalibrate(bamFile, preRecalFile, recalBamFile),
        countCovariates(recalBamFile, postRecalFile),
        analyzeCovariates(preRecalFile, preOutPath),
        analyzeCovariates(postRecalFile, postOutPath)
      )

      recalibratedBamFiles :+= recalBamFile
    }

    // Write a BAM list with all the processed per sample files
    val cohortFile = new File(qscript.outputDir, qscript.projectName + ".cohort.list")
    add( writeList(recalibratedBamFiles, cohortFile) )

    recalibratedBamFiles
  }

  def script() {
    prepareOutputDirectory()
    extendIntervals()

    val alignableInputs = QueueUtils.createInputSeqFromFile(input)
    assert(alignableInputs.length > 0, "No input files specified")

    val bamFilesBySample: Map[String, Seq[File]] = performAlignment(alignableInputs)

    performRecalibration(bamFilesBySample)
  }

  // General arguments to GATK walkers
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class bwa_bwasw(inFastQ: File, outSam: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="FastQ file to be aligned") var fastq = inFastQ
    @Output(doc="Output aligned SAM file") var alignedSam = outSam

    def commandLine = required(qscript.bwaPath) +
      required("bwasw") +
      optional("-t", qscript.numThreads) +
      optional("-b", 5) + optional("-q", 2) + optional("-r", 1) + optional("-z", 20) +
      required(qscript.reference) +
      required(fastq) +
      required(">", escape=false) +
      required(alignedSam)

    this.nCoresRequest = qscript.numThreads
    this.residentLimit = 240  // unlike memoryLimit, resident Limit is not multiplied by 1.2 and is better for CLI functions
    this.analysisName = queueLogDir + outSam + ".bwa_sam_bwasw"
    this.jobName = queueLogDir + outSam + ".bwa_sam_bwasw"
  }

  case class addReadGroup(inBam: File, outBam: File, readGroup: ReadGroup) extends AddOrReplaceReadGroups with CommandLineJavaArgs {
    this.input = Seq(inBam)
    this.output = outBam

    this.RGID = readGroup.id
    this.RGCN = readGroup.center
    this.RGPL = readGroup.platform
    this.RGSM = readGroup.sample
    this.RGLB = readGroup.library
    this.RGPU = readGroup.platform_unit

    this.memoryLimit = 32

    this.sortOrder = SortOrder.coordinate
    this.analysisName = queueLogDir + outBam + ".rg"
    this.jobName = queueLogDir + outBam + ".rg"
  }

  case class joinBams (inBams: Seq[File], outBam: File) extends MergeSamFiles with CommandLineJavaArgs {
    this.input = inBams
    this.output = outBam
    this.analysisName = queueLogDir + outBam + ".joinBams"
    this.jobName = queueLogDir + outBam + ".joinBams"
  }

  case class countCovariates(inBam: File, outRecalFile: File) extends CountCovariates with CommandLineGATKArgs {
    this.knownSites :+= qscript.dbSNP
    this.covariate ++= Seq("ReadGroupCovariate", "QualityScoreCovariate", "HomopolymerCovariate", "DinucCovariate")
    this.input_file :+= inBam
    this.read_filter :+= "BadCigar"
    this.recal_file = outRecalFile
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outRecalFile + ".covariates"
    this.jobName = queueLogDir + outRecalFile + ".covariates"
  }

  case class recalibrate(inBam: File, inRecalFile: File, outBam: File) extends TableRecalibration with CommandLineGATKArgs {
    this.input_file :+= inBam
    this.recal_file = inRecalFile
    this.defaultBaseQualities = qscript.dbq
    this.out = outBam
    this.scatterCount = qscript.numScatterGather
    this.isIntermediate = false
    this.analysisName = queueLogDir + outBam + ".recalibration"
    this.jobName = queueLogDir + outBam + ".recalibration"
  }

  case class analyzeCovariates (inRecalFile: File, outPath: File) extends AnalyzeCovariates with CommandLineJavaArgs {
    this.ignoreQ = 5
    this.recal_file = inRecalFile
    this.output_dir = outPath.toString
    this.analysisName = queueLogDir + inRecalFile + ".analyze_covariates"
    this.jobName = queueLogDir + inRecalFile + ".analyze_covariates"
  }

  case class writeList(inBams: Seq[File], outBamList: File) extends ListWriterFunction {
    this.inputFiles = inBams
    this.listFile = outBamList
    this.analysisName = queueLogDir + outBamList + ".bamList"
    this.jobName = queueLogDir + outBamList + ".bamList"
  }
}
