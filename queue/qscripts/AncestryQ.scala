/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import edu.mssm.queue.util._
import org.broadinstitute.sting.commandline.Hidden
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.gatk.walkers.genotyper.{UnifiedGenotyperEngine, GenotypeLikelihoodsCalculationModel}
import org.broadinstitute.sting.utils.interval
import org.broadinstitute.sting.utils.baq.BAQ
import org.broadinstitute.sting.gatk.walkers.variantutils.VariantsToBinaryPed.OutputMode

import org.apache.commons.io.{FileUtils}
import java.io.{PrintWriter, FileWriter}
import scala.collection.JavaConversions._

class AncestryQ extends QScript with QScriptCommon {
  qscript => 

  @Input(doc="Input BAM file list", fullName="input", shortName="I", required=true)
  var inputBams: File = _

  @Input(doc="Input FAM file", fullName="input_fam", shortName="fam", required=true)
  var inputFamFile: File = _

  @Input(doc="Path to admixture", fullName="path_to_admixture", shortName="admixture", required=true)
	var admixturePath: File = "admixture"

  @Input(doc="Reference call sites VCF", fullName="ancestry_reference_sites", required=true)
  var ancestryReferenceSitesVCF: File = _

  @Input(doc="Reference binary plink dataset prefix, i.e. without .bed/.bim/.fam", fullName="ancestry_reference_dataset", required=true)
  var ancestryReferenceDataset: File = _

  @Input(doc="Reference admixture population file", fullName="ancestry_reference_population_fine", required=true)
  var ancestryReferencePopulationsFine: File = _

  @Input(doc="Reference admixture population file", fullName="ancestry_reference_population_coarse", required=true)
  var ancestryReferencePopulationsCoarse: File = _
    		
	@Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1
  
  /* Optional Parameters */

  @Argument(doc="Downsample to coverage", fullName="downsample_to_coverage", shortName="dcov", required=false)
  var dcov: Int = 250

  @Argument(doc="Minimum Phred-Scaled quality score for a good base", fullName="min_base_quality_score", shortName="mbq", required=false)
  var minimumBaseQuality: Int = 17

  @Argument(doc="Minimum Phred-Scaled quality score for a good genotype", fullName="min_genotype_quality_score", shortName="mgq", required=false)
  var minimumGenotypeQuality: Int = 30

  @Argument(
    doc="Minimum phred-scaled confidence threshold for calling variants not at trigger track sites",
    fullName="standard_min_confidence_threshold_for_calling",
    shortName="stand_call_conf",
    required=false
  )
  var standCallConf: Double = 30.0

  @Argument(
    doc="Minimum phred-scaled confidence threshold for emitting variants not at trigger track sites",
    fullName="standard_min_confidence_threshold_for_emitting",
    shortName="stand_emit_conf",
    required=false
  )
  var standEmitConf: Double = 30.0
  
    
  def script() {
    val rawCalledSites = new File(outputDir, qscript.projectName + ".ancestry.raw.vcf")
    val calledSites    = new File(outputDir, qscript.projectName + ".ancestry.vcf")

    val bedFile = swapExt(outputDir, calledSites, ".vcf", ".bed")
    val bimFile = swapExt(outputDir, calledSites, ".vcf", ".bim")
    val famFile = swapExt(outputDir, calledSites, ".vcf", ".fam")

    val mergedBed = swapExt(outputDir, calledSites, ".vcf", ".admix.bed")
    val mergedBim = swapExt(outputDir, calledSites, ".vcf", ".admix.bim")
    val mergedFam = swapExt(outputDir, calledSites, ".vcf", ".admix.fam")

    val bamFiles = QueueUtils.createFileSeqFromFile(inputBams)
    assert(bamFiles.length > 0, "No input files specified")

    val popFile    = swapExt(outputDir, calledSites, ".vcf", ".admix.pop")
    val admixFile  = swapExt(outputDir, calledSites, ".vcf", ".admix.172.Q")
    val admixTable = swapExt(outputDir, calledSites, ".vcf", ".admix.txt")
    val MDSFile    = swapExt(outputDir, calledSites, ".vcf", ".mds")
    val MDSTable   = swapExt(outputDir, calledSites, ".vcf", ".mds.txt")

    add(
      // Force call and then filter SNPs at ancestry informative sites
      SNPCall(List(inputBams), rawCalledSites, qscript.ancestryReferenceSitesVCF),
      SNPFilter(rawCalledSites, calledSites),
      VCFToBinaryPed(calledSites, qscript.inputFamFile, bedFile, bimFile, famFile),

      // Merge "our" data with reference data set
      MergeFiles(bedFile, bimFile, famFile, mergedBed, mergedBim, mergedFam),

      // Make a population file for ADMIXTURE's supervised learning, containing a category
      // entry for each of the known population members (e.g. West African, European, etc)
      // and a '-' for each of the unknown population members
      MakePop(bamFiles.length, popFile),

      // ADMIXTURE outputs a matrix with an entry for every member of the reference population
      // in addition to the unknown population members...
      //
      // Convert this to a text file containing a table readable by R, containing only entries
      // for the unknown population members (from the bam files) with the row names equal to the
      // sample names
      CallAdmixture(mergedBed, mergedBim, mergedFam, popFile, admixFile),
      MakeAdmixTable(popFile, bamFiles.length, admixFile, mergedFam, admixTable),

      // Calculate MDS values and append a column containing the reference populations
      // to the MDS output table
      CalcMDS(mergedBed, mergedBim, mergedFam, MDSFile),
      MakeMDSTable(MDSFile, popFile, MDSTable)
    )

  }

  // General arguments to GATK walkers
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }	

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class VCFToBinaryPed(VCFFile: File, FamFile: File, OutBed: File, OutBim: File, OutFam: File) extends VariantsToBinaryPed with CommandLineGATKArgs {
    this.variant = VCFFile
    this.metaData = FamFile
    this.bed = OutBed
    this.bim = OutBim
    this.fam = OutFam

    this.mode = OutputMode.SNP_MAJOR
    this.minGenotypeQuality = qscript.minimumGenotypeQuality

    this.analysisName = queueLogDir + OutBed + ".toplink"
    this.jobName = queueLogDir + OutBed + ".toplink"
  }

  case class MergeFiles(bedFile: File, bimFile: File, famFile: File, mergedBed: File, mergedBim: File, mergedFam: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="Input bed file") var bedfile = bedFile
    @Input(doc="Input bim file") var bimfile = bimFile
    @Input(doc="Input fam file") var famfile = famFile
    
    @Output(doc="Output bed file") var mergedbed = mergedBed
    @Output(doc="Output bim file") var mergedbim = mergedBim
    @Output(doc="Output fam file") var mergedfam = mergedFam
	  @Output(doc="Output nosex file") var mergednosex = swapExt(outputDir, mergedbed, ".bed", ".nosex")
	  @Output(doc="Output log file")   var mergedlog = swapExt(outputDir, mergedbed, ".bed", ".log")
    
    def commandLine = required("plink") +
      required("--bfile") +
      required(qscript.ancestryReferenceDataset) +
      required("--noweb") +
      optional("--geno", 0.1) +  // Only include SNPs with 90% genotyping rate
      required("--bmerge") +
      required(bedFile) +
      required(bimFile) +
      required(famFile) +
      required("--make-bed") +
      required("--out") +
      required(mergedbed.getAbsolutePath().stripSuffix(".bed"))

    this.analysisName = queueLogDir + mergedBed + "mergebed"
    this.jobName = queueLogDir + mergedBed + ".mergedbed"
  }
  
  case class MakePop(sampleCount: Int, outPop: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {
     @Output(doc="Output file")
     var outputFile: File = outPop
  
     def run() {
    	FileUtils.copyFile(qscript.ancestryReferencePopulationsFine, outputFile)

    	// Add a "-" for each sample
      val fw = new FileWriter(outputFile, true) ;
      for (i <- 0 until sampleCount) {
    		fw.write("-\n")
    	}
    	fw.close()   
    }

    this.analysisName = queueLogDir + outPop + ".makepop"
    this.jobName = queueLogDir + outPop + ".makepop"
  }
  
  case class CallAdmixture(mergedBed: File, mergedBim: File, mergedFam: File, popFile: File, admixFile: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="Input bed file") var mergedbed = mergedBed
    @Input(doc="Input bim file") var mergedbim = mergedBim
    @Input(doc="Input fam file") var mergedfam = mergedFam
    @Input(doc="Input pop file") var popfile   = popFile
    
    @Output(doc="Output P file") var pfile = swapExt(outputDir, admixFile, ".Q", ".P")
    @Output(doc="Output Q file") var qfile = admixFile

    // admixture only writes to PWD...
    def commandLine = required("cd") + required(outputDir) + required("&&", escape=false) + required(admixturePath) +
      required(mergedbed) +
      required(172) +  // TODO: Infer this from POP file
      required("--supervised") +
      optional("-j" + qscript.numThreads)

    this.analysisName = queueLogDir + admixFile + ".admixture"
    this.jobName = queueLogDir + admixFile + ".admixture"
  }
  
  case class MakeAdmixTable(popFile: File, sampleCount: Int, admixFile: File, mergedFam: File, admixTable: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {
	  @Input(doc="Input admix file") var admixfile = admixFile
    @Input(doc="Input fam file") var mergedfam = mergedFam
    @Input(doc="Input pop file") var popfile   = popFile

    @Output(doc="Output ancestry table") var admixtable = admixTable
    
    def run() {
    	// TODO: Avoid reading in the whole files, since we only care about last "sampleCount" lines
      val admixRows = FileUtils.readLines(admixfile);
		  val sampleNames = FileUtils.readLines(mergedfam);
      val popRows = FileUtils.readLines(popfile).toList.distinct.filter(x => x != "-");

		  // Add a header containing the names of the categories
      val fw = new FileWriter(admixtable, true) ;

      fw.write("NAME\t" + popRows.distinct.mkString("\t") + "\n")

      // Add a row for each sample (at the end of the file)
      for (i <- (admixRows.size() - sampleCount) until admixRows.size()) {
        val name = sampleNames.get(i).split("\\s+")(1); // Recall 2nd field of FAM file is IID
        // Get percentages from Admixture result
        fw.write(name + "\t" + admixRows.get(i).replaceAll(" ", "\t") + "\n")
       }
    	fw.close()
	  }

    this.isIntermediate = false

    this.analysisName = queueLogDir + admixTable + ".admixtable"
    this.jobName = queueLogDir + admixTable + ".admixtable"
  }
   
  case class CalcMDS(mergedBed: File, mergedBim: File, mergedFam: File, MDSFile: File) extends CommandLineFunction with ExternalCommonArgs {
	  @Input(doc="Input bed file") var mergedbed = mergedBed
	  @Input(doc="Input bim file") var mergedbim = mergedBim
    @Input(doc="Input fam file") var mergedfam = mergedFam
    
    @Output(doc="Output MDS file")   var mdsfile = MDSFile
    @Output(doc="Output nosex file") var mdsnosex = swapExt(outputDir, mdsfile, ".mds", ".nosex")
	  @Output(doc="Output log file")   var mdslog = swapExt(outputDir, mdsfile, ".mds", ".log")
	  @Output(doc="Output log file")   var mdscluster0 = swapExt(outputDir, mdsfile, ".mds", ".cluster0")
	  @Output(doc="Output log file")   var mdscluster1 = swapExt(outputDir, mdsfile, ".mds", ".cluster1")
	  @Output(doc="Output log file")   var mdscluster2 = swapExt(outputDir, mdsfile, ".mds", ".cluster2")
	  @Output(doc="Output log file")   var mdscluster3 = swapExt(outputDir, mdsfile, ".mds", ".cluster3")
    
	  //Use plink to run MDS on the merged data set
    def commandLine = required("plink") +
      required("--noweb") +
      required("--mds-plot") +
      required("4") +
      required("--bfile") +
      required(mergedbed.getAbsolutePath().stripSuffix(".bed")) +
      required("--out") +
      required(mdsfile.getAbsolutePath().stripSuffix(".mds"))

    this.analysisName = queueLogDir + MDSFile + ".mdsplot"
    this.jobName = queueLogDir + MDSFile + ".mdsplot"
  }

  case class MakeMDSTable(MDSFile: File, popFile: File, MDSTable: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {
    @Input(doc="Input MDS file") var mdsfile = MDSFile
    @Input(doc="Input pop file") var popfile = popFile

    @Output(doc="Output MDS table") var mdstable = MDSTable

    def run() {
      val MDSRows = FileUtils.readLines(MDSFile);
      val popRows = FileUtils.readLines(popFile);
      val popCoarseRows = FileUtils.readLines(qscript.ancestryReferencePopulationsCoarse);

      val fw = new FileWriter(mdstable, true) ;

      //Copy the Plink MDS header and append a POP column
      fw.write(MDSRows.get(0) + " POP POP_COARSE\n")
      for (i <- 0 until popRows.size()) {
      //Append a column to the Plink MDS table containing the reference population names
        if (i<popCoarseRows.size()) {
          fw.write(MDSRows.get(i+1) + " " + popRows.get(i) +  " " + popCoarseRows.get(i) +"\n")
        } else {
          fw.write(MDSRows.get(i+1) + " " + popRows.get(i) +  " " + "-" +"\n")
        }
      }
      fw.close()
    }
    this.isIntermediate = false

    this.analysisName = queueLogDir + MDSTable + ".mdstable"
    this.jobName = queueLogDir + MDSTable + ".mdstable"
  }

  class UnifiedGenotyperBase(inBams: Seq[File], outVCF: File, allSitesVCF: File) extends UnifiedGenotyper with CommandLineGATKArgs {
    this.input_file = inBams
    this.out = outVCF
    this.dbsnp = qscript.dbSNP

    this.output_mode = UnifiedGenotyperEngine.OUTPUT_MODE.EMIT_ALL_SITES
    this.genotyping_mode = GenotypeLikelihoodsCalculationModel.GENOTYPING_MODE.GENOTYPE_GIVEN_ALLELES
    this.alleles = allSitesVCF

    this.intervals = Seq(allSitesVCF)
    if (qscript.intervals != null) {
      this.intervals :+= qscript.intervals
      this.interval_set_rule = interval.IntervalSetRule.INTERSECTION
    }

    this.scatterCount = 1  // Scatter gather currently does not respect interval intersection so no scatter/gather
    
    this.memoryLimit = 16
    this.stand_call_conf = qscript.standCallConf
    this.stand_emit_conf = qscript.standEmitConf
  }
  
  case class SNPCall(inBams: Seq[File], outVCF: File, allSitesVCF: File) extends UnifiedGenotyperBase(inBams, outVCF, allSitesVCF) {
    this.glm = GenotypeLikelihoodsCalculationModel.Model.SNP
    this.min_base_quality_score = qscript.minimumBaseQuality
    this.baq = BAQ.CalculationMode.OFF

    this.analysisName = queueLogDir + outVCF + ".snpcall"
    this.jobName = queueLogDir + outVCF + ".snpcall"
  }

  class VariantFiltrationBase(inVCF: File, outVCF: File) extends VariantFiltration with CommandLineGATKArgs {
    this.V = inVCF
    this.out = outVCF
    this.scatterCount = qscript.numScatterGather
  }

  case class SNPFilter(inVCF: File, outVCF: File) extends VariantFiltrationBase(inVCF, outVCF) {
    this.filterName ++= List("SNPQD", "SNPMQ", "SNPFS", "SNPHaplotype", "SNPMQSum", "SNPReadPosRankSum")
    this.filterExpression ++= List("QD < 2.0", "MQ < 40.0", "FS > 60.0", "HaplotypeScore > 13.0", "MQRankSum < -12.5", "ReadPosRankSum < -8.0")

    this.analysisName = queueLogDir + outVCF + ".snpfilter"
    this.jobName = queueLogDir + outVCF + ".snpfilter"
  }
  
}
