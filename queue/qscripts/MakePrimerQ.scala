 /*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import edu.mssm.queue.function.FileCopierFunction
import edu.mssm.queue.util._
import net.sf.samtools.SAMFileHeader.SortOrder
import org.broadinstitute.sting.commandline.Hidden
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.gatk.walkers.indels.IndelRealigner.ConsensusDeterminationModel
import org.broadinstitute.sting.queue.extensions.picard._
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.queue.function.{JavaCommandLineFunction, ListWriterFunction}
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.utils.baq.BAQ.CalculationMode
import java.io.FileWriter
import org.apache.commons.io.FileUtils

class MakePrimerQ extends QScript with QScriptCommon {
  qscript =>
    /** A VCF containing the sites and alleles you want to validate. Restricted to *BI-Allelic* sites */
    @Input(fullName="validateAlleles", shortName="", doc="A VCF containing the sites and alleles you want to validate. Restricted to *BI-Allelic* sites", required=true)
    var validateAlleles: File = _

    /** A VCF containing the sites you want to MASK from the designed amplicon (e.g. by Ns or lower-cased bases) */
    @Input(fullName="maskAlleles", shortName="", doc="A VCF containing the sites you want to MASK from the designed amplicon (e.g. by Ns or lower-cased bases)", required=true)
    var maskAlleles: File = _

    /** Path to the primer resource directory **/
    @Input(fullName="resourcePath", shortName="", doc="Resource Path", required=true)
    var resPath: File  = new File(".")

    /** in silico PCR reference file prefix **/
    @Input(fullName="isPCRPrefix", shortName="", doc="in silico PCR reference file prefix", required=false)
    var isPCRPrefix: String = "ucsc.hg19.fasta.split"

		
  def script() {
    val probeIntervals = new File(outputDir, qscript.projectName + ".intervals")
    val probes = new File(outputDir, qscript.projectName + ".probes.fasta")
    val primer3Query = new File(outputDir, qscript.projectName + ".primer3.query.txt")
    val primer3Output = new File(outputDir, qscript.projectName + ".primer3.out.txt")
    val isPCRQuery = new File(outputDir, qscript.projectName + ".isPCR.query.txt")
    var isPCRRefNames = new File(resPath).list.filter(x => x contains isPCRPrefix)
    var isPCROutputNames = isPCRRefNames.map(x => qscript.projectName + ".isPCR.output." + x.substring(x.length-2, x.length) + ".txt")
    var isPCRRefs:List[File] = Nil
    var isPCROutputs:List[File] = Nil
    val finalPrimers = new File(outputDir, qscript.projectName + ".primers.txt")

    for ( i <- 0 until isPCRRefNames.size) {
      isPCRRefs = isPCRRefs ::: List(new File(resPath, isPCRRefNames(i)))
    }

    for ( i <- 0 until isPCROutputNames.size) {
      isPCROutputs = isPCROutputs ::: List(new File(outputDir, isPCROutputNames(i)))
    }

    add(MakeIntervals(validateAlleles, probeIntervals))
    add(validationAmplicons(maskAlleles, probeIntervals, validateAlleles, probes))
    add(MakePrimer3Query(probes, primer3Query))
    add(CallPrimer3(primer3Query, primer3Output))
    add(MakeisPCRQuery(primer3Output, isPCRQuery))
    for ( i <- 1 until isPCRRefs.size) {
      add(CallisPCR(isPCRRefs(i), isPCRQuery, isPCROutputs(i)))
    }
    add(FinalPrimers(isPCROutputs, probes, isPCRQuery, finalPrimers))

  }
  
  // General arguments to GATK walkers
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  
  case class MakeIntervals(validateAlleles: File, probeIntervals: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {

    @Input(doc="Input file") var inputSites: File = validateAlleles
    @Output(doc="Output file") var outputIntervals: File = probeIntervals

    def run() {
       val intervalSize = 1000;
      val vcfRows = FileUtils.readLines(inputSites);
      val fw = new FileWriter(outputIntervals, true) ;
    	fw.write("HEADERpos\tname\n")
    	
    	for (i <- 0 until vcfRows.size()) {
    	  val vcfRow = vcfRows.get(i)
    	  if (vcfRow.substring(0, 1)!="#") {
    	    val vcfFields = vcfRow.split("\t")
    	    //Remove "chr" prefix from chromosome numbver
    	    val chrom = vcfFields(0)//.substring(3, vcfFields(0).length)
    	    val allelePos = vcfFields(1)
    	    //Pick the biggest of the two alleles (what about multi-allelic?)_
    	    val alleleSize = math.max(vcfFields(3).length, vcfFields(4).length)
    	    val intervalName = chrom + "_" + allelePos //+ "_" + vcfFields(3) + "_" + vcfFields(4)
    	    //What if the interval goes off the chromosome? Does this ever happen?
    	    val intervalLower = Integer.toString(Integer.parseInt(allelePos)-intervalSize)
    	    val intervalUpper = Integer.toString(Integer.parseInt(allelePos)+intervalSize+alleleSize)
    	    
  		    fw.write(chrom +  ":" + intervalLower + "-" + intervalUpper + "\t" + intervalName + "\n");
  		  }
    	}
    	fw.close()
    }	  
  }

  case class validationAmplicons(maskAlleles: File, probeIntervals: File, validateAlleles: File, probes: File) extends ValidationAmplicons  with CommandLineGATKArgs {

    this.MaskAlleles = new TaggedFile(maskAlleles, "vcf")
    this.ProbeIntervals = new TaggedFile(probeIntervals, "table")
    this.ValidateAlleles = new TaggedFile(validateAlleles, "vcf")
    this.out = probes
    this.doNotUseBWA = false
    this.virtualPrimerSize = 22
    //this.onlyOutputValidAmplicons = true
    //this.isIntermediate = false
  }
  
  //Build a file to be sent to primer3
  case class MakePrimer3Query(probeIntervals: File, primer3Query: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {

    @Input(doc="Input file") var intervals: File = probeIntervals
    @Input(doc="Input pop file") var primer3Options = new File(resPath, "primer3_options.txt")
    @Output(doc="Output file") var outputFile: File = primer3Query

    def run() {
      
      val options = FileUtils.readLines(primer3Options);                      
      val probeRows = FileUtils.readLines(intervals);
      val fw = new FileWriter(outputFile, true) ; 
      for (i <- 0 until probeRows.size()/2) {
        //val probeHeader = probeRows.get(i*2).split(' ')(0).split('>')(1)
        val probeHeader = probeRows.get(i*2).split(' ')(probeRows.get(i*2).split(' ').length-1)
        val probeSequence = probeRows.get(i*2+1)
        val probeSequenceStart = probeSequence.substring(0, probeSequence.indexOf('['))
        val probeSequenceFinish = probeSequence.substring(probeSequence.indexOf(']')+1, probeSequence.length)
        fw.write("SEQUENCE_ID=" + probeHeader + "\n")
        fw.write("SEQUENCE_TEMPLATE=" + probeSequenceStart + probeSequenceFinish + "\n")
        for (j <- 0 until options.size()) {
          fw.write(options.get(j)+"\n")
        }
      }
      fw.close()
    }	  
  }

  //Command line call to primer3_core
  case class CallPrimer3(primer3Query: File, primer3Out: File) extends CommandLineFunction with ExternalCommonArgs {

    @Input(doc="Input file") var query: File = primer3Query
    @Output(doc="Output file") var outputFile: File = primer3Out

    def commandLine = required("primer3_core") +
      required("-output=" + primer3Out) +
      required(query)
  }

  case class MakeisPCRQuery(primer3Output: File, isPCRQuery: File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {

    @Input(doc="Input file") var primer3Out: File = primer3Output
    @Output(doc="Output file") var query: File = isPCRQuery
  
    def run() {
      val primer3Rows = FileUtils.readLines(primer3Out);
      val fw = new FileWriter(query, true) ;
    	for (i <- 0 until primer3Rows.size()) {
    	  val primer3Row = primer3Rows.get(i)
    	  if (primer3Row.indexOf('=') != -1) {
          val primer3Parts = primer3Row.split("=")
          if (primer3Parts.length >= 2) {
            if (primer3Parts(0)=="SEQUENCE_ID" || primer3Parts(0)=="PRIMER_LEFT_0_SEQUENCE") {
              fw.write(primer3Parts(1) + "\t")
            }
            if (primer3Parts(0)=="PRIMER_RIGHT_0_SEQUENCE") {
              fw.write(primer3Parts(1) + "\n")
            }
          }
        }
    	}
    	fw.close()
    }	  
  }
  
  case class CallisPCR(isPCRRef: File, isPCRQuery: File, isPCROut: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="Input file") var ref: File = isPCRRef
    @Input(doc="Input file") var query: File = isPCRQuery
    @Output(doc="Output file") var outputFile: File = isPCROut

	  def commandLine = required("isPcr") +
	    required(ref) +
	    required(query) +
	    required(isPCROut)
    }

  case class FinalPrimers(isPCROutputs: List[File], inputProbes: File, isPCRQuery: File, finalPrimers:File)  extends org.broadinstitute.sting.queue.function.InProcessFunction {

    @Input(doc="Input files") var isPCROut: List[File] = isPCROutputs
    @Input(doc="Input file") var probes: File = inputProbes
    @Input(doc="Input file") var query: File = isPCRQuery
    @Output(doc="Output file") var outputPrimers: File = finalPrimers

    this.isIntermediate = false

    def run() {
      var probeRows = FileUtils.readLines(probes)
      var numberOfAmplicons = Array.fill[Int](probeRows.size()/2)(0)
      val primerQueryRows = FileUtils.readLines(query)
      val fw = new FileWriter(outputPrimers, true) ;

      for ( i <- 1 until isPCROut.size) {
        val PCRRows = FileUtils.readLines(isPCROut(i))
        if (PCRRows.size()>0) {
          for ( j <- 0 until PCRRows.size()) {
            if (PCRRows.get(j).substring(0, 1)==">") {
              for ( k <- 0 until numberOfAmplicons.length) {
                var probeName = probeRows.get(2*k).split(">")(1).split(" ")(0)
                if (probeName.split(':')(0) + "_" + probeName.split(':')(1) == PCRRows.get(j).split(" ")(1)) {
                  numberOfAmplicons(k) += 1
                }
              }
            }
          }

        }
      }

      for ( i <- 0 until numberOfAmplicons.length) {
        fw.write(probeRows.get(2*i) + "\n")
        fw.write(">" + numberOfAmplicons(i) + " amplicons generated with in silico PCR\n")
        for ( j <- 0 until primerQueryRows.size()) {
          var primerQueryName = primerQueryRows.get(j).split("\t")(0)
          var probeName = probeRows.get(2*i).split(">")(1).split(" ")(0)
          if (primerQueryName == probeName.split(':')(0) + "_" + probeName.split(':')(1)) {
            fw.write(primerQueryRows.get(j).split("\t")(0)+"_F\t"+"TGTAAAACGACGGCCAGT"+primerQueryRows.get(j).split("\t")(1) + "\n")
            fw.write(primerQueryRows.get(j).split("\t")(0)+"_R\t"+"CAGGAAACAGCTATGACC"+primerQueryRows.get(j).split("\t")(2) + "\n")
          }
        }
        fw.write("\n")
      }
      fw.close()
    }
  }
}