/*
* Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
*
* License available in LICENSE.txt.
*/

import edu.mssm.queue.util.QueueUtils
import org.broadinstitute.sting.commandline.{RodBinding, Argument, Input, Hidden}
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.queue.function.JavaCommandLineFunction
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.extensions.gatk._
import edu.mssm.queue.extensions.picard._
import org.broadinstitute.sting.utils.codecs.table.TableFeature

class DetailedBaseQualityQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input BAM file list", fullName="input", shortName="I", required=true)
  var input: File = _

  /* Optional Parameters */

  @Argument(doc="Minimum mapping quality of reads to count towards depth", fullName="minMappingQuality", shortName="mmq",  required=false)
  var minMappingQuality: Int = 10

  @Argument(doc = "Minimum quality of bases to count towards depth", fullName="minBaseQuality", shortName ="mbq", required=false)
  var minBaseQuality: Int = 20

  @Argument(doc = "Maximum value for MAPQ to be considered problematic read", fullName="maxLowMAPQ", shortName ="mlmq", required=false)
  var maxLowMAPQ: Int = 1

  /**
   * If the number of QC+ bases (on reads with MAPQ > minMappingQuality and with base quality > minBaseQuality) exceeds this
   * value and is less than maxDepth the site is considered PASS.
   */
  @Argument(fullName = "minDepth", shortName = "minDepth", doc = "Minimum QC+ read depth before a locus is considered callable", required = false)
  var minDepthForCallable: Int = 20

  @Argument(doc="Bait intervals file", fullName="bait_intervals", shortName="bait", required=false)
  var baitIntervals: File = _

  @Argument(doc="Path to BED tools executable", fullName="path_to_bedtools", shortName="bedtools", required=false)
  var bedtoolsPath: String = "bedtools"

  @Input(doc="BED file with genomic intervals of interest, e.g. exons", shortName="geneBed", required=false)
  var geneBeds: Seq[File] = _

  // Override default for expand intervals to prevent default expansion during metrics calculation. We are interested
  // in metrics for just the specified target regions.
  expandIntervals = 0

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  /* Global Variables */


  def script() {
    prepareOutputDirectory()
    extendIntervals()

    val bamFiles = QueueUtils.createFileSeqFromFile(input)
    assert(bamFiles.length > 0, "No input files specified")

    for (sampleBamFile <- bamFiles) {
      val callReportTable = swapExt(outputDir, sampleBamFile, ".bam", ".call.bed")
      val callSummary     = swapExt(outputDir, sampleBamFile, ".bam", ".call.summary")
      add( callable(Seq(sampleBamFile), callReportTable, callSummary) )

      for (bed:File <- geneBeds) {
        val geneCallReportTable = swapExt(outputDir, callReportTable, ".bed", "." + bed.getName)
        add( callableByBed(callReportTable, bed, geneCallReportTable) )
      }

      add( multMetrics(sampleBamFile, swapExt(outputDir, sampleBamFile, ".bam", ".metrics")) )
      add( insertMetrics(
        sampleBamFile,
        swapExt(outputDir, sampleBamFile, ".bam", ".metrics.insert_size_metrics"),
        swapExt(outputDir, sampleBamFile, ".bam", ".metrics.insert_size.pdf")
      ) )
      add( alignMetrics(sampleBamFile, swapExt(outputDir, sampleBamFile, ".bam", ".metrics.alignment_summary_metrics")) )
      add( gcMetrics(
        sampleBamFile,
        swapExt(outputDir, sampleBamFile, ".bam", ".metrics.gc_bias_metrics"),
        swapExt(outputDir, sampleBamFile, ".bam", ".metrics.gc_bias.pdf"),
        swapExt(outputDir, sampleBamFile, ".bam", ".metrics.gc_bias_summary")
      ) )
      if (qscript.baitIntervals != null && qscript.intervals != null) {
        add( hsMetrics(
          sampleBamFile,
          swapExt(outputDir, sampleBamFile, ".bam", ".metrics.hs_metrics")
        ) )
      }
    }

    // Depth of coverage
    add(
      new covDepth(List(input), new File(outputDir, qscript.projectName + ".dcov"))
    )
  }

  // General arguments to GATK walkers
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = false
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class callable(inBams: Seq[File], outTable: File, outSummary: File) extends CallableLoci with CommandLineGATKArgs {
    this.input_file = inBams
    this.out = outTable
    this.summary = outSummary
    this.minMappingQuality = qscript.minMappingQuality
    this.minBaseQuality = qscript.minBaseQuality
    this.maxLowMAPQ = qscript.maxLowMAPQ
    this.minDepth = qscript.minDepthForCallable
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outTable + ".callreport"
    this.jobName = queueLogDir + outTable + ".callreport"
  }

  case class callableByBed(inCallableBed: File, inIntervalsBed: File, outCoverage: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="Callability BED file produced by CallableLoci walker") var callable = inCallableBed
    @Input(doc="Intervals BED file to summarize callability over") var intervals = inIntervalsBed
    @Output(doc="Output BED file with coverage statistics") var coverage = outCoverage

    // TODO: Incorporate pipefail somehow so we know if one stage fails...
    def commandLine = required("grep") +
      required("-Fw") +
      required("CALLABLE") +
      required(callable) +
      required("| tr ' ' '\\t' |", escape=false) +
      required(qscript.bedtoolsPath) +
      required("coverage") +
      required("-a") + required("-") +
      required("-b") + required(intervals) +
      required(">", escape=false) +
      required(coverage)

    this.analysisName = queueLogDir + outCoverage + ".call_covg"
    this.jobName      = queueLogDir + outCoverage + ".call_covg"
  }

  class covDepth(inBams: Seq[File], outPrefix: File) extends CommandLineGATKArgs with ScatterGatherableFunction {
    this.analysis_type = "DepthOfCoverage"

    this.input_file = inBams

    this.scatterCount = qscript.numScatterGather
    this.scatterClass = classOf[LocusScatterFunction]

    val STATISTICS_SUFFIX   = ".sample_statistics"
    val INTERVALSTAT_SUFFIX = ".sample_interval_statistics"
    val INTERVALSUM_SUFFIX  = ".sample_interval_summary"
    val CUMULATIVE_SUFFIX   = ".sample_cumulative_coverage_counts"
    val GENE_SUMMARY_SUFFIX = ".sample_gene_summary"

    @Output
    @Gather(classOf[org.broadinstitute.sting.queue.function.scattergather.SimpleTextGatherFunction])
    var statisticsSampleOut: File = new File(outPrefix.getPath() + STATISTICS_SUFFIX)

    @Output
    @Gather(classOf[org.broadinstitute.sting.queue.function.scattergather.SimpleTextGatherFunction])
    var intervalStatSampleOut: File = new File(outPrefix.getPath() + INTERVALSTAT_SUFFIX)

    @Output
    @Gather(classOf[org.broadinstitute.sting.queue.function.scattergather.SimpleTextGatherFunction])
    var intervalSumSampleOut: File = new File(outPrefix.getPath() + INTERVALSUM_SUFFIX)

    @Output
    @Gather(classOf[org.broadinstitute.sting.queue.function.scattergather.SimpleTextGatherFunction])
    var cumulativeSampleOut: File = new File(outPrefix.getPath() + CUMULATIVE_SUFFIX)

    override def commandLine = super.commandLine +
      required("-omitBaseOutput") +
      required("-mbq", qscript.minBaseQuality) +
      required("-mmq", qscript.minMappingQuality) +
      required("--start", 1) + required("--stop", 250) + required("--nBins", 100) +
      required("-ct",20) + required("-ct",30) + required("-ct",60) +
      required("--includeRefNSites") +
      required("-o", statisticsSampleOut.getPath().stripSuffix(STATISTICS_SUFFIX))

    override def shortDescription = "DepthOfCoverage: " + outPrefix

    this.analysisName = queueLogDir + outPrefix + ".dcovreport"
    this.jobName = queueLogDir + outPrefix + ".dcovreport"
  }


  case class multMetrics(inBam: File, outMetrics: File) extends CollectMultipleMetrics with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outMetrics
    this.REFERENCE_SEQUENCE = qscript.reference
    this.PROGRAM = List("QualityScoreDistribution", "MeanQualityByCycle")
    this.analysisName = queueLogDir + outMetrics + ".multmetrics"
    this.jobName = queueLogDir + outMetrics + ".multmetrics"
  }

  case class insertMetrics(inBam: File, outMetrics: File, outChart: File) extends CollectInsertSizeMetrics with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outMetrics
    this.HISTOGRAM_FILE = outChart
    this.METRIC_ACCUMULATION_LEVEL = List("SAMPLE")
    this.analysisName = queueLogDir + outMetrics + ".insertmetrics"
    this.jobName = queueLogDir + outMetrics + ".insertmetrics"
  }

  case class alignMetrics(inBam: File, outMetrics: File) extends CollectAlignmentSummaryMetrics with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outMetrics
    this.METRIC_ACCUMULATION_LEVEL = List("SAMPLE")
    this.analysisName = queueLogDir + outMetrics + ".insertmetrics"
    this.jobName = queueLogDir + outMetrics + ".insertmetrics"
  }

  case class gcMetrics(inBam: File, outMetrics: File, outChart: File, outSummary: File) extends CollectGcBiasMetrics with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outMetrics
    this.CHART_OUTPUT = outChart
    this.SUMMARY_OUTPUT = outSummary
    this.REFERENCE_SEQUENCE = qscript.reference
    this.analysisName = queueLogDir + outMetrics + ".gcmetrics"
    this.jobName = queueLogDir + outMetrics + ".gcmetrics"
  }

  case class hsMetrics(inBam: File, outMetrics: File) extends CalculateHsMetrics with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outMetrics
    this.REFERENCE_SEQUENCE = qscript.reference
    this.BAIT_INTERVALS = qscript.baitIntervals
    this.TARGET_INTERVALS = qscript.intervals
    this.METRIC_ACCUMULATION_LEVEL = List("SAMPLE")
    this.memoryLimit = 8
    this.analysisName = queueLogDir + outMetrics + ".gcmetrics"
    this.jobName = queueLogDir + outMetrics + ".gcmetrics"
  }
}

