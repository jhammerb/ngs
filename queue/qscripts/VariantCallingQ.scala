/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import edu.mssm.queue.extensions.gatk.RemoveConcordantVariants
import edu.mssm.queue.util.QueueUtils
import edu.mssm.queue.function._
import org.broadinstitute.sting.commandline.{Input, Hidden}
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.gatk.walkers.genotyper.{UnifiedGenotyperEngine, GenotypeLikelihoodsCalculationModel}
import org.broadinstitute.sting.gatk.walkers.variantrecalibration.VariantRecalibratorArgumentCollection
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.utils.baq.BAQ
import org.broadinstitute.sting.utils.interval
import org.broadinstitute.sting.utils.interval.IntervalSetRule
import org.broadinstitute.sting.utils.variantcontext.VariantContextUtils

class VariantCallingQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input BAM file list", fullName="input", shortName="I", required=true)
  var input: File = _

  @Input(doc="Variant calling pipelie, one of: TARGETED, EXOME, GENOME", shortName="pipeline", required=true)
  var pipeline: String = _

  /* Optional Parameters */

  @Input(doc="Call indels", shortName="indels", required=false)
  var indels: Boolean = true

  @Input(doc="Downsample to coverage", fullName="downsample_to_coverage", shortName="dcov", required=false)
  var dcov: Int = 250

  @Input(doc="Minimum Phred-Scaled quality score for a good base", fullName="min_base_quality_score", shortName="mbq", required=false)
  var minimumBaseQuality: Int = 17

  @Input(
    doc="Minimum phred-scaled confidence threshold for calling variants not at trigger track sites",
    fullName="standard_min_confidence_threshold_for_calling",
    shortName="stand_call_conf",
    required=false
  )
  var standCallConf: Double = 30.0

  @Input(
    doc="Minimum phred-scaled confidence threshold for emitting variants not at trigger track sites",
    fullName="standard_min_confidence_threshold_for_emitting",
    shortName="stand_emit_conf",
    required=false
  )
  var standEmitConf: Double = 30.0

  @Input(
    doc="Maximum deletion fraction allowed at a site to call a genotype.",
    fullName="max_deletion_fraction",
    shortName="deletions",
    required=false
  )
  var deletions: Double = 0.05

  @Input(doc="Perform BAQ calculation", shortName="doBAQ", required=false)
  var doBAQ: Boolean = true

  @Input(doc="Truth sensitivity threshold for SNP filtering", shortName="ts_snp_filter_level", required=false)
  var tsSNPFilterLevel: Double = 98.5

  @Input(doc="Truth sensitivity threshold for indel filtering", shortName="ts_indel_filter_level", required=false)
  var tsIndelFilterLevel: Double = 99.0

  @Input(doc="HapMap sites VCF File for SNP recalibration", shortName="hapmap_snps_file", required=false)
  var hapmapSnpSitesFile: File = _

  @Input(doc="Omni sites VCF File for SNP recalibration", shortName="omni_snps_file", required=false)
  var omniSnpSitesFile: File = _

  @Input(doc="Standard indel sites VCF File for indel recalibration", shortName="standard_indels_file", required=false)
  var indelSitesFile: File = _

  @Input(doc="SNP sites at which to specifically genotype", shortName="emit_all_snp_sites", required=false)
  var emitAllSNPsFile: File = _

  @Input(doc="Indel sites at which to specifically genotype", shortName="emit_all_indel_sites", required=false)
  var emitAllIndelsFile: File = _

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  /* Global Variables */

  // Is this pulldown data, e.g. targeted, exome, etc.
  var pulldown = false


  def variantCall(bamFiles: List[File], outSNPFile: File, outIndelFile: File) {
    if (qscript.emitAllSNPsFile != null) {
      val discoverySNPFile   = swapExt(outputDir, outSNPFile,   ".vcf", ".discovery.vcf")
      val emitallSNPFile     = swapExt(outputDir, outSNPFile,   ".vcf", ".emitall.vcf")
      val filtEmitallSNPFile = swapExt(outputDir, emitallSNPFile, ".vcf", ".filt.vcf")
      add(
        snpCall(bamFiles, discoverySNPFile),
        snpCall(bamFiles, emitallSNPFile, qscript.emitAllSNPsFile),
        filterEmitAllVariants(discoverySNPFile, emitallSNPFile, filtEmitallSNPFile, qscript.emitAllSNPsFile),
        combineVariants(List(new TaggedFile(discoverySNPFile, "disc"), new TaggedFile(filtEmitallSNPFile, "emit")), outSNPFile, false)
      )
    } else {
      add( snpCall(bamFiles, outSNPFile) )
    }


    if (qscript.emitAllIndelsFile != null) {
      val discoveryIndelFile   = swapExt(outputDir, outIndelFile, ".vcf", ".discovery.vcf")
      val emitallIndelFile     = swapExt(outputDir, outIndelFile, ".vcf", ".emitall.vcf")
      val filtEmitallIndelFile = swapExt(outputDir, emitallIndelFile, ".vcf", ".filt.vcf")
      add(
        indelCall(bamFiles, discoveryIndelFile),
        indelCall(bamFiles, emitallIndelFile, qscript.emitAllIndelsFile),
        filterEmitAllVariants(discoveryIndelFile, emitallIndelFile, filtEmitallIndelFile, qscript.emitAllIndelsFile),
        combineVariants(List(new TaggedFile(discoveryIndelFile, "disc"), new TaggedFile(filtEmitallIndelFile, "emit")), outSNPFile, false)
      )
    } else {
      add( indelCall(bamFiles, outIndelFile) )
    }
  }


  def script() {
    prepareOutputDirectory()
    extendIntervals()

    // val bamFiles = QueueUtils.createFileSeqFromFile(input)
    val bamFiles = List(input)
    assert(bamFiles.length > 0, "No input files specified")

    val rawSNPVCFFile   = new File(outputDir, qscript.projectName + ".raw.SNP.vcf")
    val filtSNPVCFFile  = swapExt(outputDir, rawSNPVCFFile, "raw.SNP.vcf", "filt.SNP.vcf")
    val recalSNPVCFFile = swapExt(outputDir, filtSNPVCFFile, "filt.SNP.vcf", "recal.SNP.vcf")

    val tranchesSNPFile = swapExt(outputDir, rawSNPVCFFile, ".vcf", ".tranches")
    val tranchesSNPFileFinal = swapExt(outputDir, rawSNPVCFFile, ".vcf", ".vqsr.tranches")
    val rscriptSNPFile  = swapExt(outputDir, rawSNPVCFFile, ".vcf", ".R")
    val rscriptSNPFileFinal = swapExt(outputDir, rawSNPVCFFile, ".vcf", ".vqsr.R")
    val recalSNPFile    = swapExt(outputDir, rawSNPVCFFile, ".vcf", ".recal")

    val rawINDVCFFile  = new File(outputDir, qscript.projectName + ".raw.IND.vcf")
    val filtINDVCFFile = swapExt(outputDir, rawINDVCFFile, "raw.IND.vcf", "filt.IND.vcf")
    val recalINDVCFFile = swapExt(outputDir, filtINDVCFFile, "filt.IND.vcf", "recal.IND.vcf")

    val tranchesINDFile = swapExt(outputDir, rawINDVCFFile, ".vcf", ".tranches")
    val tranchesINDFileFinal = swapExt(outputDir, rawINDVCFFile, ".vcf", ".vqsr.tranches")
    val rscriptINDFile  = swapExt(outputDir, rawINDVCFFile, ".vcf", ".R")
    val rscriptINDFileFinal  = swapExt(outputDir, rawINDVCFFile, ".vcf", ".vqsr.R")
    val recalINDFile    = swapExt(outputDir, rawINDVCFFile, ".vcf", ".recal")

    val resultVCFFile   = new File(outputDir, qscript.projectName + ".combined.vcf")

    pipeline match {
      case "PACBIO" =>
        qscript.doBAQ = false
        add(
          snpCall(bamFiles, rawSNPVCFFile),
          combineVariants(List(new TaggedFile(rawSNPVCFFile, "SNP")), resultVCFFile)
        )
      case "TARGETED" =>
        // Targeted sequencing often does not have enough data to use BAQ, VQSR
        qscript.pulldown = true
        qscript.doBAQ = false

        variantCall(bamFiles, rawSNPVCFFile, rawINDVCFFile)
        add(
          snpFilter(rawSNPVCFFile, filtSNPVCFFile),
          indelFilter(rawINDVCFFile, filtINDVCFFile),
          combineVariants(List(new TaggedFile(filtSNPVCFFile, "SNP"), new TaggedFile(filtINDVCFFile, "Indel")), resultVCFFile)
        )
      case "EXOME" =>
        // Exome sequencing often does not have enough data to use BAQ or VQSR for indels
        qscript.pulldown = true
        qscript.doBAQ = false

        variantCall(bamFiles, rawSNPVCFFile, rawINDVCFFile)
        add(
          snpRecalibrate(rawSNPVCFFile, tranchesSNPFile, recalSNPFile, rscriptSNPFile),
          copyFile(tranchesSNPFile, tranchesSNPFileFinal),
          copyFile(rscriptSNPFile, rscriptSNPFileFinal),
          snpCut(rawSNPVCFFile, tranchesSNPFile, recalSNPFile, recalSNPVCFFile),
          indelFilter(rawINDVCFFile, filtINDVCFFile),
          combineVariants(List(new TaggedFile(recalSNPVCFFile, "SNP"), new TaggedFile(filtINDVCFFile, "Indel")), resultVCFFile)
        )
      case "GENOME" =>
        variantCall(bamFiles, rawSNPVCFFile, rawINDVCFFile)
        add(
          snpRecalibrate(rawSNPVCFFile, tranchesSNPFile, recalSNPFile, rscriptSNPFile),
          copyFile(tranchesSNPFile, tranchesSNPFileFinal),
          copyFile(rscriptSNPFile, rscriptSNPFileFinal),
          snpCut(rawSNPVCFFile, tranchesSNPFile, recalSNPFile, recalSNPVCFFile),
          indelRecalibrate(rawINDVCFFile, tranchesINDFile, recalINDFile, rscriptINDFile),
          copyFile(rscriptINDFile, rscriptINDFileFinal),
          copyFile(tranchesINDFile, tranchesINDFileFinal),
          indelCut(rawINDVCFFile, tranchesINDFile, recalINDFile, recalINDVCFFile),
          combineVariants(List(new TaggedFile(recalSNPVCFFile, "SNP"), new TaggedFile(recalINDVCFFile, "Indel")), resultVCFFile)
        )
      case _ => assert(false, "Unknown sequencing pipeline")
    }
  }


  // General arguments to non-GATK tools
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  class UnifiedGenotyperBase(inBams: Seq[File], outVCF: File, allSitesVCF: File) extends UnifiedGenotyper with CommandLineGATKArgs {
    this.input_file = inBams
    this.out = outVCF
    this.dbsnp = qscript.dbSNP

    if (allSitesVCF != null) {
      this.output_mode = UnifiedGenotyperEngine.OUTPUT_MODE.EMIT_ALL_SITES
      this.genotyping_mode = GenotypeLikelihoodsCalculationModel.GENOTYPING_MODE.GENOTYPE_GIVEN_ALLELES
      this.alleles = allSitesVCF

      // Look for associated interval file to speedup calling at just 'alleles' sites
      var allSitesIntervals = swapExt(allSitesVCF.getParent, allSitesVCF, ".vcf", ".interval_list")
      if (allSitesIntervals.exists()) {
        this.intervals = Seq(allSitesIntervals)
        if (qscript.intervals != null) {
          this.intervals :+= qscript.intervals
        }
        this.interval_set_rule = interval.IntervalSetRule.INTERSECTION
      }

      this.scatterCount = 1  // Scatter gather currently does not respect interval intersection so no scatter/gather
    } else {
      this.scatterCount = qscript.numScatterGather
    }

    this.memoryLimit = 16
    this.stand_call_conf = qscript.standCallConf
    this.stand_emit_conf = qscript.standEmitConf
  }

  case class snpCall(inBams: Seq[File], outVCF: File, allSitesVCF: File = null) extends UnifiedGenotyperBase(inBams, outVCF, allSitesVCF) {
    this.glm = GenotypeLikelihoodsCalculationModel.Model.SNP
    this.min_base_quality_score = qscript.minimumBaseQuality
    this.max_deletion_fraction = qscript.deletions
    this.baq = if (qscript.doBAQ) {
      BAQ.CalculationMode.CALCULATE_AS_NECESSARY
    } else {
      BAQ.CalculationMode.OFF
    }
    this.analysisName = queueLogDir + outVCF + ".snpcall"
    this.jobName = queueLogDir + outVCF + ".snpcall"
  }


  case class indelCall(inBams: Seq[File], outVCF: File, allSitesVCF: File = null) extends UnifiedGenotyperBase(inBams, outVCF, allSitesVCF) {
    this.glm = GenotypeLikelihoodsCalculationModel.Model.INDEL
    this.baq = BAQ.CalculationMode.OFF
    this.analysisName = queueLogDir + outVCF + ".indelcall"
    this.jobName = queueLogDir + outVCF + ".indelcall"
  }

  class VariantFiltrationBase(inVCF: File, outVCF: File) extends VariantFiltration with CommandLineGATKArgs {
    this.V = inVCF
    this.out = outVCF
    this.scatterCount = qscript.numScatterGather
  }

  case class snpFilter(inVCF: File, outVCF: File) extends VariantFiltrationBase(inVCF, outVCF) with CommandLineGATKArgs {
    this.filterName ++= List("SNPQD", "SNPMQ", "SNPFS", "SNPHaplotype", "SNPMQSum", "SNPReadPosRankSum")
    this.filterExpression ++= List("QD < 2.0", "MQ < 40.0", "FS > 60.0", "HaplotypeScore > 13.0", "MQRankSum < -12.5", "ReadPosRankSum < -8.0")
    this.analysisName = queueLogDir + outVCF + ".snpfilter"
    this.jobName = queueLogDir + outVCF + ".snpfilter"
  }

  case class indelFilter(inVCF: File, outVCF: File) extends VariantFiltrationBase(inVCF, outVCF) with CommandLineGATKArgs {
    this.filterName ++= List("IndelQD", "IndelReadPosRankSum", "IndelFS")
    this.filterExpression ++= List("QD < 2.0", "ReadPosRankSum < -20.0", "FS > 200.0")
    this.analysisName = queueLogDir + outVCF + ".indelfilter"
    this.jobName = queueLogDir + outVCF + ".indelfilter"
  }

  class VQSRBase(inVCF: File, outTranches: File, outRecal: File, outRscript: File) extends VariantRecalibrator with CommandLineGATKArgs {
    this.input :+= inVCF
    this.tranches_file = outTranches
    this.recal_file    = outRecal
    this.rscript_file  = outRscript
    this.allPoly = true
    this.tranche ++= List(
      "100.0", "99.9", "99.5", "99.3", "99.0", "98.9", "98.8", "98.5", "98.4", "98.3",
      "98.2", "98.1", "98.0", "97.9", "97.8", "97.5", "97.0", "95.0", "90.0"
    )
  }

  case class snpRecalibrate(inVCF: File, outTranches: File, outRecal: File, outRscript: File)
    extends VQSRBase(inVCF, outTranches, outRecal, outRscript) {

    this.resource :+= new TaggedFile( qscript.dbSNP, "known=true,prior=4.0" )
    if (qscript.hapmapSnpSitesFile != null)
      this.resource :+= new TaggedFile( qscript.hapmapSnpSitesFile, "known=false,training=true,truth=true,prior=15.0" )
    if (qscript.omniSnpSitesFile != null)
      this.resource :+= new TaggedFile( qscript.omniSnpSitesFile, "known=false,training=true,truth=true,prior=12.0" )

    this.use_annotation ++= List("QD", "HaplotypeScore", "MQRankSum", "ReadPosRankSum", "MQ", "FS")
    if(!qscript.pulldown) {
      this.use_annotation ++= List("DP")
    } else {  // Exome/pulldown specific parameters
      this.mG = 6
      this.percentBad = 0.04
    }
    this.trustAllPolymorphic = true
    this.mode = VariantRecalibratorArgumentCollection.Mode.SNP
    this.analysisName = queueLogDir + inVCF + ".snprecal"
    this.jobName = queueLogDir + inVCF + ".snprecal"
  }

  case class indelRecalibrate(inVCF: File, outTranches: File, outRecal: File, outRscript: File)
    extends VQSRBase(inVCF, outTranches, outRecal, outRscript) {

    this.resource :+= new TaggedFile( qscript.dbSNP, "known=true,prior=2.0" )
    if (qscript.indelSitesFile != null)
      this.resource :+= new TaggedFile( qscript.indelSitesFile, "known=false,training=true,truth=true,prior=12.0" )

    this.use_annotation ++= List("QD", "HaplotypeScore", "ReadPosRankSum", "FS")
    this.mode = VariantRecalibratorArgumentCollection.Mode.INDEL
    this.analysisName = queueLogDir + inVCF + ".indelrecal"
    this.jobName = queueLogDir + inVCF + ".indelrecal"
  }

  class ApplyVQSRBase(inVCF: File, inTranches: File, inRecal: File, outVCF: File) extends ApplyRecalibration with CommandLineGATKArgs {
    this.input :+= inVCF
    this.tranches_file = inTranches
    this.recal_file = inRecal
    this.out = outVCF
    this.memoryLimit = 6
    this.scatterCount = qscript.numScatterGather
  }

  case class snpCut(inVCF: File, inTranches: File, inRecal: File, outVCF: File) extends ApplyVQSRBase(inVCF, inTranches, inRecal, outVCF) {
    this.ts_filter_level = qscript.tsSNPFilterLevel
    this.mode = VariantRecalibratorArgumentCollection.Mode.SNP
    this.analysisName = queueLogDir + outVCF + ".snpcut"
    this.jobName = queueLogDir + outVCF + ".snpcut"
  }

  case class indelCut(inVCF: File, inTranches: File, inRecal: File, outVCF: File) extends ApplyVQSRBase(inVCF, inTranches, inRecal, outVCF) {
    this.ts_filter_level = qscript.tsIndelFilterLevel
    this.mode = VariantRecalibratorArgumentCollection.Mode.INDEL
    this.analysisName = queueLogDir + outVCF + ".indelcut"
    this.jobName = queueLogDir + outVCF + ".indelcut"
  }

  case class filterEmitAllVariants(inDiscVCF: File, inEmitVCF: File, outVCF: File, allSitesVCF: File) extends RemoveConcordantVariants with CommandLineGATKArgs {
    this.out = outVCF
    this.variant = inEmitVCF
    this.concordance = inDiscVCF
    this.reset_filter = true

    // Look for associated interval file to speedup calling at just 'alleles' sites
    var allSitesIntervals = swapExt(allSitesVCF.getParent, allSitesVCF, ".vcf", ".interval_list")
    if (allSitesIntervals.exists()) {
      this.intervals = Seq(allSitesIntervals)
      if (qscript.intervals != null) {
        this.intervals :+= qscript.intervals
      }
      this.interval_set_rule = interval.IntervalSetRule.INTERSECTION
    }

    this.analysisName = queueLogDir + outVCF + ".removeconc"
    this.jobName = queueLogDir + outVCF + ".removeconc"
  }

  case class combineDiscoveryAndEmitVariants(inDiscVCF: File, inEmitVCF: File, outVCF: File, keep: Boolean = false) extends CombineVariants with CommandLineGATKArgs {
    this.out = outVCF
    this.variant = List(new TaggedFile(inDiscVCF, "Disc"), new TaggedFile(inEmitVCF, "Emit"))
    this.genotypeMergeOptions = VariantContextUtils.GenotypeMergeType.PRIORITIZE
    this.priority = "Disc,Emit"
    this.filteredrecordsmergetype = VariantContextUtils.FilteredRecordMergeType.KEEP_UNCONDITIONAL
    this.suppressCommandLineHeader = true
    this.setKey = null
    this.isIntermediate = !keep
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outVCF + ".combine"
    this.jobName = queueLogDir + outVCF + ".combine"
  }

  case class combineVariants(inFiles: Seq[TaggedFile], outVCF: File, keep: Boolean = true) extends CombineVariants with CommandLineGATKArgs {
    this.out = outVCF
    this.variant = inFiles
    this.assumeIdenticalSamples = true
    this.filteredrecordsmergetype = VariantContextUtils.FilteredRecordMergeType.KEEP_IF_ANY_UNFILTERED
    this.isIntermediate = !keep
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outVCF + ".combine"
    this.jobName = queueLogDir + outVCF + ".combine"
  }

}




