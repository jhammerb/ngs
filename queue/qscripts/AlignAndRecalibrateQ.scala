/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */


import edu.mssm.queue.extensions.picard.{MergeBamAlignment, FastqToSam}
import edu.mssm.queue.function.FileCopierFunction
import edu.mssm.queue.util._
import net.sf.picard.util.FastqQualityFormat
import net.sf.samtools.SAMFileHeader.SortOrder
import org.broadinstitute.sting.commandline.Hidden
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.gatk.walkers.indels.IndelRealigner.ConsensusDeterminationModel
import org.broadinstitute.sting.queue.extensions.picard._
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.queue.function.{JavaCommandLineFunction, ListWriterFunction}
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.utils.baq.BAQ.CalculationMode

class AlignAndRecalibrateQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input file list (with sample metadata)", fullName="input", shortName="I", required=true)
  var input: File = _

  /* Optional Parameters */

  @Input(doc="Path of bwa binary", fullName="path_to_bwa", shortName="bwa", required=false)
  var bwaPath: File = "bwa"

  @Input(doc="FASTQ score format", shortName="quality", fullName="quality_format", required=false)
  var qualityFormat: FastqQualityFormat = FastqQualityFormat.Standard

  @Input(doc="VCF files of reference indels for Indel Realignment", fullName="extra_indels", shortName="indels", required=false)
  var indels: Seq[File] = Seq()

  @Input(doc="Cleaning model: KNOWNS_ONLY, USE_READS or USE_SW", fullName="clean_model", shortName="cm", required=false)
  var cleaningModel: String = "USE_READS"

  @Input(doc="Perform validation on the BAM files", fullName="validation", shortName="v", required=false)
  var validation: Boolean = false

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  /* Global Variables */

  // Flag to turn on or off cleaning and de-duping of bam files
  var cleanAndDedup: Boolean = true

  var cleanModelEnum: ConsensusDeterminationModel = ConsensusDeterminationModel.USE_READS

  def performAlignment(alignableInputs: Seq[AlignableInput]): Map[String, Seq[File]] = {
    val sampleTable = scala.collection.mutable.Map.empty[String, Seq[File]]

    for (alignableInput <- alignableInputs) {

      // Perform (re)alignment on input files (single or paired-end)
      val alignedBamFile: File = alignableInput match {
        case SEFastQFile(fastq, _) =>
          val saiFile = swapExt(outputDir, fastq, QueueUtils.getReadFileExtension(fastq), ".sai")
          val samFile = swapExt(outputDir, saiFile, ".sai", ".sam")
          val unalignedBamFile = swapExt(outputDir, samFile, ".sam", ".unaligned.bam")
          val alignedBamFile   = swapExt(outputDir, samFile, ".sam", ".bam")
          add(
            bwa_aln(fastq, saiFile),
            bwa_samse(fastq, saiFile, samFile, alignableInput.readGroup.toString),
            fastqToBam(fastq, unalignedBamFile, alignableInput.readGroup),
            mergeAlignment(unalignedBamFile, samFile, alignedBamFile, SortOrder.coordinate, false /* Not Paired */)
          )
          alignedBamFile
        case PEFastQFiles(fastq1, fastq2, _) =>
          val saiFile1 = swapExt(outputDir, fastq1, QueueUtils.getReadFileExtension(fastq1), ".sai")
          val saiFile2 = swapExt(outputDir, fastq2, QueueUtils.getReadFileExtension(fastq2), ".sai")
          val samFile  = swapExt(outputDir, saiFile1, ".sai", ".sam")
          val unalignedBamFile = swapExt(outputDir, samFile, ".sam", ".unaligned.bam")
          val alignedBamFile   = swapExt(outputDir, samFile, ".sam", ".bam")
          add(
            bwa_aln(fastq1, saiFile1),
            bwa_aln(fastq2, saiFile2),
            bwa_sampe(fastq1, saiFile1, fastq2, saiFile2, samFile, alignableInput.readGroup.toString),
            fastqToBam(fastq1, unalignedBamFile, alignableInput.readGroup, fastq2),
            mergeAlignment(unalignedBamFile, samFile, alignedBamFile, SortOrder.coordinate, true /* Paired */)
          )
          alignedBamFile
        case PBFastQFile(fastq, _) =>
          val samFile = swapExt(outputDir, fastq, QueueUtils.getReadFileExtension(fastq), ".sam")
	        val alignedBamFile = swapExt(outputDir, samFile, ".sam", ".bam")
          cleanAndDedup = false
          add(
            bwa_bwasw(fastq, samFile),
            addReadGroup(samFile, alignedBamFile, alignableInput.readGroup)  // addReadGroup also sorts...
          )
          alignedBamFile
      }

      // Keep track of BAM files associated with each sample
      val sample = alignableInput.readGroup.sample
      if (!sampleTable.contains(sample))
        sampleTable(sample) = Seq(alignedBamFile)
      else if (!sampleTable(sample).contains(alignedBamFile))
        sampleTable(sample) :+= alignedBamFile
    }

    sampleTable.toMap
  }

  def performRecalibration(bamFilesBySample: Map[String, Seq[File]]): Seq[File] = {
    var recalibratedBamFiles: Seq[File] = Seq()

    // If this is a 'KNOWNS_ONLY' indel realignment run, do it only once for all samples
    val globalIntervals = new File(outputDir, projectName + ".intervals")
    if (cleaningModel == ConsensusDeterminationModel.KNOWNS_ONLY)
      add( target(null, globalIntervals) )

    for ((sample, bamFiles) <- bamFilesBySample) {
      val bamFile = new File(outputDir, qscript.projectName + "." + sample + ".bam")

       // dedup metric files
      val dedupMetricsFile = swapExt(outputDir, bamFile, ".bam", ".clean.dedup.metrics_tmp")
      val dedupMetricsFileFinal = swapExt(outputDir, bamFile, ".bam", ".clean.dedup.metrics")

      // Accessory files
      val targetIntervals = if (cleaningModel == ConsensusDeterminationModel.KNOWNS_ONLY) {
        globalIntervals
      } else {
        swapExt(outputDir, bamFile, ".bam", ".intervals")
      }

      val preRecalFile    = swapExt(outputDir, bamFile, ".bam", ".pre_recal.csv")
      val postRecalFile   = swapExt(outputDir, bamFile, ".bam", ".post_recal.csv")
      val preOutPath      = swapExt(outputDir, bamFile, ".bam", ".pre")
      val postOutPath     = swapExt(outputDir, bamFile, ".bam", ".post")
      val preValidateLog  = swapExt(outputDir, bamFile, ".bam", ".pre.validation")
      val postValidateLog = swapExt(outputDir, bamFile, ".bam", ".post.validation")

      val dedupedBamFile = if (cleanAndDedup) {
        val cleanedBamFile    = swapExt(outputDir, bamFile, ".bam", ".clean.bam")
        val dedupedBamFileInt = swapExt(outputDir, bamFile, ".bam", ".clean.dedup.bam")

        if (cleaningModel != ConsensusDeterminationModel.KNOWNS_ONLY)
          add( target(bamFiles, targetIntervals) )


        add(
          cleanIndels(bamFiles, targetIntervals, cleanedBamFile),
          dedup(cleanedBamFile, dedupedBamFileInt, dedupMetricsFile),
          copyFile(dedupMetricsFile, dedupMetricsFileFinal) // Make sure we save dedup metrics
        )
        dedupedBamFileInt
      } else {
        add( joinBams(bamFiles,bamFile) )
        bamFile
      }
      val recalBamFile   = swapExt(outputDir, dedupedBamFile, ".bam", ".recal.bam")

      // Validation is an optional step for the BAM file generated after
      // alignment and the final bam file of the pipeline.

      // Ignore common "MAPQ should be 0 for unmapped read" errors resulting from BWA/Picard impedance mismatch
      // http://sourceforge.net/apps/mediawiki/picard/index.php?title=Main_Page#Q:_Why_am_I_getting_errors_from_Picard_like_.22MAPQ_should_be_0_for_unmapped_read.22_or_.22CIGAR_should_have_zero_elements_for_unmapped_read.3F.22

      if (validation) {
        for (sampleBamFile <- bamFiles)
          add( validate(sampleBamFile, preValidateLog, List("INVALID_MAPPING_QUALITY")) )
        add( validate(recalBamFile, postValidateLog, List("INVALID_MAPPING_QUALITY")) )
      }

      add(
        countCovariates(dedupedBamFile, preRecalFile),
        recalibrate(dedupedBamFile, preRecalFile, recalBamFile),
        countCovariates(recalBamFile, postRecalFile),
        analyzeCovariates(preRecalFile, preOutPath),
        analyzeCovariates(postRecalFile, postOutPath)
      )

      recalibratedBamFiles :+= recalBamFile
    }

    // Write a BAM list with all the processed per sample files
    val cohortFile = new File(qscript.outputDir, qscript.projectName + ".cohort.list")
    add( writeList(recalibratedBamFiles, cohortFile) )

    recalibratedBamFiles
  }


  def script() {
    prepareOutputDirectory()
    extendIntervals()

    val alignableInputs = QueueUtils.createInputSeqFromFile(input)
    assert(alignableInputs.length > 0, "No input files specified")

    val bamFilesBySample: Map[String, Seq[File]] = performAlignment(alignableInputs)

    performRecalibration(bamFilesBySample)
  }

  // General arguments to GATK walkers
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class joinBams (inBams: Seq[File], outBam: File) extends MergeSamFiles with ExternalCommonArgs {
    this.input = inBams
    this.output = outBam
    this.analysisName = queueLogDir + outBam + ".joinBams"
    this.jobName = queueLogDir + outBam + ".joinBams"
  }

  case class target(inBams: Seq[File], outIntervals: File) extends RealignerTargetCreator with CommandLineGATKArgs {
    if (cleanModelEnum != ConsensusDeterminationModel.KNOWNS_ONLY)
      this.input_file = inBams
    this.out = outIntervals
    this.mismatchFraction = 0.0
    this.known :+= qscript.dbSNP
    if (qscript.indels != null)
      this.known ++= qscript.indels
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outIntervals + ".target"
    this.jobName = queueLogDir + outIntervals + ".target"
  }

  case class cleanIndels(inBams: Seq[File], tIntervals: File, outBam: File) extends IndelRealigner with CommandLineGATKArgs {
    this.input_file = inBams
    this.targetIntervals = tIntervals
    this.out = outBam
    this.known :+= qscript.dbSNP
    if (qscript.indels != null)
      this.known ++= qscript.indels
    this.consensusDeterminationModel = qscript.cleanModelEnum
    this.compress = 0
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outBam + ".clean"
    this.jobName = queueLogDir + outBam + ".clean"
  }

  case class dedup(inBam: File, outBam: File, metricsFile: File) extends MarkDuplicates with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outBam
    this.metrics = metricsFile
    this.memoryLimit = 30
    this.maxRecordsInRam = 4000000
    this.analysisName = queueLogDir + outBam + ".dedup"
    this.jobName = queueLogDir + outBam + ".dedup"

    // Try to speed up very slow operation by re-enabling parallel GC
    this.nCoresRequest = 4
    override def javaOpts = {
      super.javaOpts + required("-XX:ParallelGCThreads=3")
    }
  }

  case class countCovariates(inBam: File, outRecalFile: File) extends CountCovariates with CommandLineGATKArgs {
    this.knownSites :+= qscript.dbSNP
    this.covariate ++= Seq("ReadGroupCovariate", "QualityScoreCovariate", "CycleCovariate", "DinucCovariate")
    this.input_file :+= inBam
    this.recal_file = outRecalFile
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outRecalFile + ".covariates"
    this.jobName = queueLogDir + outRecalFile + ".covariates"
  }

  case class recalibrate(inBam: File, inRecalFile: File, outBam: File) extends TableRecalibration with CommandLineGATKArgs {
    this.input_file :+= inBam
    this.recal_file = inRecalFile
    this.baq = CalculationMode.CALCULATE_AS_NECESSARY
    this.out = outBam
    this.scatterCount = qscript.numScatterGather
    this.isIntermediate = false
    this.analysisName = queueLogDir + outBam + ".recalibration"
    this.jobName = queueLogDir + outBam + ".recalibration"
  }


  case class analyzeCovariates (inRecalFile: File, outPath: File) extends AnalyzeCovariates with CommandLineJavaArgs {
    this.recal_file = inRecalFile
    this.output_dir = outPath.toString
    this.analysisName = queueLogDir + inRecalFile + ".analyze_covariates"
    this.jobName = queueLogDir + inRecalFile + ".analyze_covariates"
  }

  case class bwa_aln(inFastQ: File, outSai: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="FastQ file to be aligned") var fastq = inFastQ
    @Output(doc="Output SAI file") var sai = outSai

    this.nCoresRequest = qscript.numThreads
    this.residentLimit = 8

    val illumina13_score: Boolean = qscript.qualityFormat match {
      case FastqQualityFormat.Standard => false
      case FastqQualityFormat.Illumina => true
      case _ =>
        assert(false, "BWA does not support pre-Illumina 1.3 quality scores. Please convert FASTQ.")
        false
    }

    def commandLine = required(qscript.bwaPath) +
      required("aln") +
      optional("-q", 5) +
      optional("-l", 32) + optional("-k", 2) +
      optional("-o", 1) +
      conditional(illumina13_score, "-I") +
      optional("-t",qscript.numThreads) +
      required(qscript.reference) +
      required(fastq) +
      required(">", escape=false) +
      required(sai)

    this.analysisName = queueLogDir + outSai + ".bwa_aln"
    this.jobName      = queueLogDir + outSai + ".bwa_aln"
  }

  case class bwa_samse(inFastQ: File, inSai: File, outSam: File, readGroup: String) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="FastQ file to be aligned") var fastq = inFastQ
    @Input(doc="bwa alignment index file (.sai)") var sai = inSai
    @Output(doc="Output aligned SAM file") var alignedSam = outSam

    this.residentLimit = 8

    def commandLine = required(qscript.bwaPath) +
      required("samse") +
      optional("-r", readGroup) +
      required(qscript.reference) +
      required(sai) +
      required(fastq) +
      required(">", escape=false) +
      required(alignedSam)

    this.analysisName = queueLogDir + outSam + ".bwa_samse"
    this.jobName      = queueLogDir + outSam + ".bwa_samse"
  }

  case class bwa_sampe(inFastQ1: File, inSai1: File, inFastQ2: File, inSai2:File, outSam: File, readGroup: String) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="FastQ file to be aligned for 1st mating pair") var fastq1 = inFastQ1
    @Input(doc="FastQ file to be aligned for 2nd mating pair") var fastq2 = inFastQ2
    @Input(doc="bwa alignment index file (.sai) for 1st mating pair") var sai1 = inSai1
    @Input(doc="bwa alignment index file (.sai) for 2nd mating pair") var sai2 = inSai2
    @Output(doc="Output aligned SAM file") var alignedSam = outSam

    this.residentLimit = 16

    def commandLine = required(qscript.bwaPath) +
      required("sampe") +
      required("-P") +
      optional("-r", readGroup) +
      required(qscript.reference) +
      required(sai1) + required(sai2) +
      required(fastq1) + required(fastq2) +
      required(">", escape=false) +
      required(alignedSam)

    this.analysisName = queueLogDir + outSam + ".bwa_sampe"
    this.jobName = queueLogDir + outSam + ".bwa_sampe"
  }


  case class bwa_bwasw(inFastQ: File, outSam: File) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="FastQ file to be aligned") var fastq = inFastQ
    @Output(doc="Output aligned SAM file") var alignedSam = outSam

    def commandLine = required(qscript.bwaPath) +
      required("bwasw") +
      optional("-t", qscript.numThreads) +
      optional("-b", 5) + optional("-q", 2) + optional("-r", 1) + optional("-z", 20) +
      required(qscript.reference) +
      required(fastq) +
      required(">", escape=false) +
      required(alignedSam)

    this.nCoresRequest = qscript.numThreads
    this.residentLimit = 240  // unlike memoryLimit, resident Limit is not multiplied by 1.2 and is better for CLI functions
    this.analysisName = queueLogDir + outSam + ".bwa_sam_bwasw"
    this.jobName = queueLogDir + outSam + ".bwa_sam_bwasw"
  }

  case class fastqToBam(inFastQ1: File, outBam: File, readGroup: ReadGroup, inFastQ2: File = null) extends edu.mssm.queue.extensions.picard.FastqToSam with CommandLineJavaArgs {
    this.FASTQ = inFastQ1
    if (inFastQ2 != null) {
      this.FASTQ2 = inFastQ2
    }
    this.output = outBam
    this.sortOrder = SortOrder.queryname
    this.QUALITY_FORMAT = qscript.qualityFormat

    this.READ_GROUP_NAME   = readGroup.id
    this.SAMPLE_NAME       = readGroup.sample
    this.LIBRARY_NAME      = readGroup.library
    this.PLATFORM_UNIT     = readGroup.platform_unit
    this.PLATFORM          = readGroup.platform
    this.SEQUENCING_CENTER = readGroup.center
    this.DESCRIPTION       = readGroup.description

    this.analysisName = queueLogDir + outBam + ".fastqToBam"
    this.jobName = queueLogDir + outBam + ".fastqToBam"
  }

  case class mergeAlignment(unalignedBam: File, alignedSam: File, outBam: File, sortOrderP: SortOrder, isPaired: Boolean) extends MergeBamAlignment with CommandLineJavaArgs {
    this.UNMAPPED_BAM = unalignedBam
    this.ALIGNED_BAM  = alignedSam
    this.output = outBam
    this.PAIRED_RUN = isPaired
    this.REFERENCE_SEQUENCE = qscript.reference
    this.MAX_INSERTIONS_OR_DELETIONS = -1  // Don't restrict the number of indels
    this.sortOrder = sortOrderP
    this.maxRecordsInRam = 1000000
    this.memoryLimit = 8
    this.analysisName = queueLogDir + outBam + ".mergeBam"
    this.jobName = queueLogDir + outBam + ".mergeBam"
  }

  case class sortSam(inSam: File, outBam: File, sortOrderP: SortOrder) extends SortSam with CommandLineJavaArgs {
    this.input :+= inSam
    this.output = outBam
    this.sortOrder = sortOrderP
    this.maxRecordsInRam = 4000000
    this.memoryLimit = 30
    this.analysisName = queueLogDir + outBam + ".sortSam"
    this.jobName = queueLogDir + outBam + ".sortSam"

    // Try to speed up very slow operation by re-enabling parallel GC
    this.nCoresRequest = 4
    override def javaOpts = {
      super.javaOpts + required("-XX:ParallelGCThreads=3")
    }
  }

  case class fixMate(inBam: File, outBam: File) extends edu.mssm.queue.extensions.picard.FixMateInformation with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outBam
    this.analysisName = queueLogDir + outBam + ".fixmate"
    this.jobName = queueLogDir + outBam + ".fixmate"
  }

  case class validate(inBam: File, outLog: File, ignore: Seq[String]) extends ValidateSamFile with CommandLineJavaArgs {
    this.input :+= inBam
    this.output = outLog
    this.REFERENCE_SEQUENCE = qscript.reference
    this.IGNORE = ignore
    this.isIntermediate = false
    this.analysisName = queueLogDir + outLog + ".validate"
    this.jobName = queueLogDir + outLog + ".validate"
  }

  case class writeList(inBams: Seq[File], outBamList: File) extends ListWriterFunction {
    this.inputFiles = inBams
    this.listFile = outBamList
    this.analysisName = queueLogDir + outBamList + ".bamList"
    this.jobName = queueLogDir + outBamList + ".bamList"
  }

  case class addReadGroup(inBam: File, outBam: File, readGroup: ReadGroup) extends AddOrReplaceReadGroups with CommandLineJavaArgs {
    this.input = Seq(inBam)
    this.output = outBam
    
    this.RGID = readGroup.id
    this.RGCN = readGroup.center
    this.RGPL = readGroup.platform
    this.RGSM = readGroup.sample
    this.RGLB = readGroup.library
    this.RGPU = readGroup.platform_unit

    this.sortOrder = SortOrder.coordinate
    this.analysisName = queueLogDir + outBam + ".rg"
    this.jobName = queueLogDir + outBam + ".rg"
  }
}
