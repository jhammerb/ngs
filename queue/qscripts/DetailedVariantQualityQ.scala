/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import edu.mssm.queue.util.QueueUtils
import org.broadinstitute.sting.commandline.{Input, Hidden}
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.extensions.gatk._

class DetailedVariantQualityQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input VCF file list", fullName="input", shortName="I", required=true)
  var input: File = _

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  /* Global Variables */


  def script() {
    prepareOutputDirectory()
    extendIntervals()

    val vcfFiles = QueueUtils.createFileSeqFromFile(input)
    add( coreQC(vcfFiles, new File(outputDir, qscript.projectName + ".varqc.gatkreport")) )
  }

  // General arguments to non-GATK tools
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = false
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  class VariantEvalBase(inVCFs: Seq[File], outReport: File, suffix: String) extends VariantEval with CommandLineGATKArgs {
    this.eval = inVCFs
    this.out = outReport
    this.num_threads = qscript.numThreads
    this.noST = true
    this.stratificationModule ++= List("Sample","Filter","Novelty")
    this.noEV = true
    this.analysisName = queueLogDir + outReport + "." + suffix
    this.jobName = queueLogDir + outReport + "." + suffix
  }

  case class coreQC(inVCFs: Seq[File], outReport: File) extends VariantEvalBase(inVCFs, outReport, "varqc") {
    this.dbsnp = qscript.dbSNP
    this.evalModule ++= List("CountVariants", "CompOverlap", "TiTvVariantEvaluator")
  }

}

