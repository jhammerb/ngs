import edu.mssm.queue.extensions.gatk.SnpEffToPlinkSeqAnnotation
import org.broadinstitute.sting.commandline.{Hidden, Argument, Input}
import edu.mssm.queue.extensions.snpeff.SnpEff
import org.broadinstitute.sting.gatk.samples
import org.broadinstitute.sting.gatk.samples.PedigreeValidationType
import org.broadinstitute.sting.queue.extensions.gatk.{TaggedFile, PhaseByTransmission, VariantAnnotator}
import org.broadinstitute.sting.queue.QScript
import tools.nsc.io.File

/*
* Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
*
* License available in LICENSE.txt.
*/

class VariantAnnotationQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  @Input(doc="Input VCF file", shortName="I", fullName="input", required=true)
  var input: File = _

  @Input(doc="snpEFF configuration file", shortName="snpeff_cfg", required=true)
  var snpEffConfigFile: File = _

  @Input(doc="Pedigree .ped file", shortName="pedigree", required=false)
  var pedigreeFile: File = _

  /* Optional Parameters */

  @Argument(doc="snpEFF jar", shortName="snpeff_jar", required=false)
  var snpEffJar: File = _

  @Argument(doc="snpEff genome", shortName="snpeff_genome", required=false)
  var snpEffGenomes: Seq[String] = List("hg19")

  @Argument(doc="dbNSFP database VCF", shortName="dbnsfp", required=false)
  var dbnsfpVCFFile: File = _

  @Argument(doc="GERP++ VCF", shortName="gerp", required=false)
  var gerpVCFFile: File = _

  @Argument(doc="PhyloP VCF", shortName="phylop", required=false)
  var phylopVCFFile: File = _

  /* Hidden Parameters */

  @Hidden
  @Input(doc="How many ways to scatter/gather", fullName="scatter_gather", shortName="sg", required=false)
  var numScatterGather: Int = 1

  override def script() {
    prepareOutputDirectory()
    extendIntervals()

    val fileForAnno = if (pedigreeFile != null) {
      val phaseVCFFile = swapExt(outputDir, input, ".vcf", ".phase.vcf")
      val mendelFile = swapExt(outputDir, input, ".vcf", ".mendel.gatkreport.int")
      add(
        phaseByTransmission(input, phaseVCFFile, pedigreeFile, mendelFile),
        copyFile(mendelFile, swapExt(outputDir, input, ".vcf", ".mendel.gatkreport"))
      )
      phaseVCFFile
    } else {
      input
    }

    var snpEffVCFs: Seq[File] = Seq()
    for (genome <- qscript.snpEffGenomes) {
      val snpEffVCFFile     = swapExt(outputDir, fileForAnno, ".vcf", ".snpeff." + genome + ".vcf")
      val snpEffSummaryFile = new File(outputDir, "snpEff_summary." + genome + ".html.int")
      add(
        snpEff(fileForAnno, snpEffVCFFile, snpEffSummaryFile, genome),
        copyFile(snpEffSummaryFile, new File(outputDir, "snpEff_summary." + genome + ".html"))
      )
      snpEffVCFs :+= snpEffVCFFile
    }

    val snpEffCombinedVCFFile = if (snpEffVCFs.length > 1) {
      val snpEffCombinedVCFFile = swapExt(outputDir, fileForAnno, ".vcf", ".snpeff.info.vcf")
      add( combineSnpEff(fileForAnno, snpEffVCFs, snpEffCombinedVCFFile) )
      snpEffCombinedVCFFile
    } else {
      snpEffVCFs(0)
    }

    val annotatedFile     = swapExt(outputDir, input, ".vcf", ".anno.vcf")

    add(
      snpEffToPlinkSeq(snpEffCombinedVCFFile, new File(outputDir, "snpEff_annotation.meta")),
      annotate(fileForAnno, snpEffCombinedVCFFile, annotatedFile)
    )
  }

  // General arguments to non-GATK tools
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class snpEff(inVCF: File, outVCF: File, outSummaryFile: File, genomeVer: String) extends SnpEff with CommandLineJavaArgs {
    this.inVcf = inVCF
    this.outVcf = outVCF
    this.outSummary = outSummaryFile
    this.jarFile = qscript.snpEffJar
    this.config = qscript.snpEffConfigFile
    this.genomeVersion = genomeVer
    this.memoryLimit = 4
    this.analysisName = queueLogDir + outVCF + ".snpeff"
    this.jobName = queueLogDir + outVCF + ".snpeff"
  }

  case class snpEffToPlinkSeq(inVCF: File, outTable: File) extends SnpEffToPlinkSeqAnnotation with CommandLineGATKArgs {
    this.variant = inVCF
    this.out = outTable
    this.remove_non_coding = true
    this.isIntermediate = false
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outTable + ".snpeffpseq"
    this.jobName = queueLogDir + outTable + ".snpeffpseq"
  }

  case class combineSnpEff(inVCF: File, inResources: Seq[File], outVCF: File) extends VariantAnnotator with CommandLineGATKArgs {
    this.variant = inVCF
    this.out = outVCF

    this.annotation :+= "CombineSnpEff"
    for ((r,i) <- inResources.view.zipWithIndex) {
      this.resource :+= new TaggedFile(r, "snpeff" + i)
    }

    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outVCF + ".snpeffcombine"
    this.jobName = queueLogDir + outVCF + ".snpeffcombine"
  }

  case class annotate(inVCF: File, snpEffVCF: File, outVCF: File) extends VariantAnnotator with CommandLineGATKArgs {
    this.variant = inVCF
    this.out = outVCF
    this.isIntermediate = false

    this.snpEffFile = snpEffVCF
    this.annotation :+= "SnpEff"

    if (qscript.dbnsfpVCFFile != null) {
      this.annotation :+= "DBNSFP"
      this.resource :+= new TaggedFile(qscript.dbnsfpVCFFile, "dbnsfp")
    }
    if (qscript.gerpVCFFile != null && qscript.phylopVCFFile != null) {
      this.annotation :+= "Conservation"
      this.resource :+= new TaggedFile(qscript.gerpVCFFile, "gerp")
      this.resource :+= new TaggedFile(qscript.phylopVCFFile, "phylop")
    }
    this.memoryLimit = 8
    this.scatterCount = qscript.numScatterGather
    this.analysisName = queueLogDir + outVCF + ".snpeffanno"
    this.jobName = queueLogDir + outVCF + ".snpeffanno"
  }

  case class phaseByTransmission(inVCF: File, outVCF: File, pedFile: File, mendelFile: File) extends PhaseByTransmission with CommandLineGATKArgs {
    this.MendelianViolationsFile = mendelFile
    this.variant = inVCF
    this.out = outVCF
    this.pedigreeValidationType = samples.PedigreeValidationType.SILENT
    this.pedigree :+= pedFile
    this.analysisName = queueLogDir + outVCF + ".phasebytrans"
    this.jobName = queueLogDir + outVCF + ".phasebytrans"
  }
}

