/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import org.broadinstitute.sting.queue.QScript

class HelloWorld extends QScript {
  def script() {
    add(new CommandLineFunction {
      def commandLine = "echo Hello World"
    })
  }
}