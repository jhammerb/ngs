import org.broadinstitute.sting.queue.extensions.gatk.FastaStats
import org.broadinstitute.sting.queue.QScript

/*
* Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
*
* License available in LICENSE.txt.
*/

class PrepareResourceDirectoryQ extends QScript with QScriptCommon {
  qscript =>

  /* Required Parameters */

  /* Optional Parameters */

  @Input(doc="Path of bwa binary", fullName="path_to_bwa", shortName="bwa", required=false)
  var bwaPath: File = "bwa"

  override def script() {
    val refs = List(
      new File(qscript.outputDir, "human_g1k_v37.fasta"),
      new File(qscript.outputDir, "human_g1k_v37_decoy.fasta"),
      new File(qscript.outputDir, "ucsc.hg19.fasta")
    )

    // Build indices
    for (ref <- refs) {
      add( bwa_index(ref) )
    }

  }

  // General arguments to non-GATK tools
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 3
    this.isIntermediate = false
  }

  // General arguments to Java Command Line tools
  trait CommandLineJavaArgs extends CommandLineJavaArgsHelper with ExternalCommonArgs {

  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATKArgsHelper with ExternalCommonArgs {

  }

  case class createDictandFAI (@Input ref: File) extends FastaStats with CommandLineGATKArgs {
    @Output val outDict:  File = swapExt(ref.getParent, ref, ".fasta", ".dict")
    @Output val outFai:   File = swapExt(ref.getParent, ref, ".fasta", ".fasta.fai")
    @Output val outStats: File = swapExt(ref.getParent, ref, ".fasta", ".stats")
    this.reference_sequence = ref
    this.out = outStats
  }

  case class bwa_index(@Input inFastA: File) extends CommandLineFunction with ExternalCommonArgs {
    @Output val outAMB: File = swapExt(outputDir, inFastA, ".fasta", ".fasta.amb")
    @Output val outANN: File = swapExt(outputDir, inFastA, ".fasta", ".fasta.ann")
    @Output val outBWT: File = swapExt(outputDir, inFastA, ".fasta", ".fasta.bwt")
    @Output val outPAC: File = swapExt(outputDir, inFastA, ".fasta", ".fasta.pac")
    @Output val outSA:  File = swapExt(outputDir, inFastA, ".fasta", ".fasta.sa")

    def commandLine = required(qscript.bwaPath) +
      required("index") +
      required("-a", "bwtsw") +
      required(inFastA)

    this.analysisName = queueLogDir + inFastA + ".bwa_index"
    this.jobName      = queueLogDir + inFastA + ".bwa_index"
  }

  /*
  Build compatible gene list from refSeq
  grep \"^#\" -v $unsorted_vcf | sort -n -k5 -T /scratch/tmp | $GATK_ROOT/public/perl/sortByRef.pl --k 3 --tmp /scratch/tmp - $newRef.fasta.fai


   */
}

