<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
  ~
  ~ License available in LICENSE.txt.
  -->

<project name="queue" default="all">
  
  <property environment="env"/>  
  <property file="build.properties"/>
  <!-- Uncomment the following property if no tests compilation is needed -->
  <!-- 
  <property name="skip.tests" value="true"/>
   -->
  
  <!-- Compiler options -->
  
  <property name="compiler.debug" value="on"/>
  <property name="compiler.generate.no.warnings" value="off"/>
  <property name="compiler.args" value=""/>
  <property name="compiler.max.memory" value="128m"/>
  <patternset id="ignored.files">
    <exclude name="**/CVS/**"/>
    <exclude name="**/SCCS/**"/>
    <exclude name="**/RCS/**"/>
    <exclude name="**/rcs/**"/>
    <exclude name="**/.DS_Store/**"/>
    <exclude name="**/.svn/**"/>
    <exclude name="**/.pyc/**"/>
    <exclude name="**/.pyo/**"/>
    <exclude name="**/*.pyc/**"/>
    <exclude name="**/*.pyo/**"/>
    <exclude name="**/.git/**"/>
    <exclude name="**/*.hprof/**"/>
    <exclude name="**/_svn/**"/>
    <exclude name="**/.hg/**"/>
    <exclude name="**/*.lib/**"/>
    <exclude name="**/*~/**"/>
    <exclude name="**/__pycache__/**"/>
    <exclude name="**/.bundle/**"/>
    <exclude name="**/*.rbc/**"/>
  </patternset>
  <patternset id="library.patterns">
    <include name="*.zip"/>
    <include name="*.apk"/>
    <include name="*.war"/>
    <include name="*.egg"/>
    <include name="*.ear"/>
    <include name="*.swc"/>
    <include name="*.jar"/>
  </patternset>
  <patternset id="compiler.resources">
    <include name="**/?*.properties"/>
    <include name="**/?*.xml"/>
    <include name="**/?*.gif"/>
    <include name="**/?*.png"/>
    <include name="**/?*.jpeg"/>
    <include name="**/?*.jpg"/>
    <include name="**/?*.html"/>
    <include name="**/?*.dtd"/>
    <include name="**/?*.tld"/>
    <include name="**/?*.ftl"/>
    <include name="**/?*.R"/>
  </patternset>
  
  <!-- JDK definitions -->
  
  <property name="jdk.bin.1.6" value="${jdk.home.1.6}/bin"/>
  <path id="jdk.classpath.1.6">
    <fileset dir="${jdk.home.1.6}">
      <include name="lib/deploy.jar"/>
      <include name="lib/dt.jar"/>
      <include name="lib/javaws.jar"/>
      <include name="lib/jce.jar"/>
      <include name="lib/jconsole.jar"/>
      <include name="lib/management-agent.jar"/>
      <include name="lib/plugin.jar"/>
      <include name="lib/sa-jdi.jar"/>
      <include name="../Classes/charsets.jar"/>
      <include name="../Classes/classes.jar"/>
      <include name="../Classes/jsse.jar"/>
      <include name="../Classes/ui.jar"/>
      <include name="lib/ext/apple_provider.jar"/>
      <include name="lib/ext/dnsns.jar"/>
      <include name="lib/ext/localedata.jar"/>
      <include name="lib/ext/sunjce_provider.jar"/>
      <include name="lib/ext/sunpkcs11.jar"/>
    </fileset>
  </path>
  
  <property name="project.jdk.home" value="${jdk.home.1.6}"/>
  <property name="project.jdk.bin" value="${jdk.bin.1.6}"/>
  <property name="project.jdk.classpath" value="jdk.classpath.1.6"/>
  
  
  <!-- Project Libraries -->
  
  <path id="library.sting/lib.classpath">
    <fileset dir="${path.variable.gatk_root}/dist">
      <patternset refid="library.patterns"/>
    </fileset>
  </path>
  
  <!-- Modules -->
  
  
  <!-- Module queue -->
  
  <dirname property="module.queue.basedir" file="${ant.file}"/>
  
  
  <property name="module.jdk.home.queue" value="${project.jdk.home}"/>
  <property name="module.jdk.bin.queue" value="${project.jdk.bin}"/>
  <property name="module.jdk.classpath.queue" value="${project.jdk.classpath}"/>
  
  <property name="compiler.args.queue" value="${compiler.args}"/>
  
  <property name="queue.output.dir" value="${module.queue.basedir}/out/production/queue"/>
  <property name="queue.testoutput.dir" value="${module.queue.basedir}/out/test/queue"/>
  
  <path id="queue.module.bootclasspath">
    <!-- Paths to be included in compilation bootclasspath -->
  </path>
  
  <path id="queue.module.production.classpath">
    <path refid="${module.jdk.classpath.queue}"/>
    <path refid="library.sting/lib.classpath"/>
  </path>
  
  <path id="queue.runtime.production.module.classpath">
    <pathelement location="${queue.output.dir}"/>
    <path refid="library.sting/lib.classpath"/>
  </path>
  
  <path id="queue.module.classpath">
    <path refid="${module.jdk.classpath.queue}"/>
    <pathelement location="${queue.output.dir}"/>
    <path refid="library.sting/lib.classpath"/>
  </path>
  
  <path id="queue.runtime.module.classpath">
    <pathelement location="${queue.testoutput.dir}"/>
    <pathelement location="${queue.output.dir}"/>
    <path refid="library.sting/lib.classpath"/>
  </path>
  
  
  <patternset id="excluded.from.module.queue">
    <patternset refid="ignored.files"/>
  </patternset>
  
  <patternset id="excluded.from.compilation.queue">
    <patternset refid="excluded.from.module.queue"/>
    <exclude name="**/META-INF/**"/>
  </patternset>
  
  <path id="queue.module.sourcepath">
    <dirset dir="${module.queue.basedir}">
      <include name="src"/>
    </dirset>
  </path>
  
  
  <target name="compile.module.queue" depends="compile.module.queue.production,compile.module.queue.tests" description="Compile module queue"/>
  
  <target name="compile.module.queue.production" description="Compile module queue; production classes">
    <mkdir dir="${queue.output.dir}"/>
    <javac destdir="${queue.output.dir}" debug="${compiler.debug}" nowarn="${compiler.generate.no.warnings}" memorymaximumsize="${compiler.max.memory}" fork="true" executable="${module.jdk.bin.queue}/javac">
      <compilerarg line="${compiler.args.queue}"/>
      <bootclasspath refid="queue.module.bootclasspath"/>
      <classpath refid="queue.module.production.classpath"/>
      <src refid="queue.module.sourcepath"/>
      <patternset refid="excluded.from.compilation.queue"/>
      <exclude name="**/*.scala"/>
    </javac>
    <scalac destdir="${queue.output.dir}" jvmargs="-Xmx${compiler.max.memory}" deprecation="yes" unchecked="yes">
      <classpath refid="queue.module.production.classpath"/>
      <src refid="queue.module.sourcepath"/>
      <include name="**/*.scala"/>
    </scalac>

    <copy todir="${queue.output.dir}">
      <fileset dir="${module.queue.basedir}/src">
        <patternset refid="compiler.resources"/>
        <type type="file"/>
      </fileset>
    </copy>
  </target>
  
  <target name="compile.module.queue.tests" depends="compile.module.queue.production" description="compile module queue; test classes" unless="skip.tests"/>
  
  <target name="clean.module.queue" description="cleanup module">
    <delete dir="${queue.output.dir}"/>
    <delete dir="${queue.testoutput.dir}"/>
  </target>
  
  <target name="init" description="Build initialization">
    <path id="scala.classpath">
      <fileset dir="${path.variable.gatk_root}/dist">
        <include name="scala-compiler-*.jar"/>
        <include name="scala-library-*.jar"/>
      </fileset>
    </path>
    <taskdef resource="scala/tools/ant/antlib.xml">
      <classpath refid="scala.classpath"/>
    </taskdef>
  </target>
  
  <target name="clean" depends="clean.module.queue" description="cleanup all"/>
  
  <target name="build.modules" depends="init, clean, compile.module.queue" description="build all modules"/>
  
  <target name="init.artifacts">
    <property name="artifacts.temp.dir" value="${basedir}/__artifacts_temp"/>
    <property name="artifact.output.queue:jar" value="${basedir}/dist"/>
    <mkdir dir="${artifacts.temp.dir}"/>
    <property name="temp.jar.path.Queue.jar" value="${artifacts.temp.dir}/Queue.jar"/>
  </target>

  <target name="artifact.queue:jar" depends="init.artifacts, compile.module.queue" description="Build &#39;Queue:jar&#39; artifact">
    <mkdir dir="${artifact.output.queue:jar}"/>
    <jar destfile="${temp.jar.path.Queue.jar}" duplicate="preserve" filesetmanifest="skip" manifest="${basedir}/src/META-INF/MANIFEST.MF">
      <zipfileset dir="${queue.output.dir}"/>
      <zipfileset src="${path.variable.gatk_root}/dist/Queue.jar"/>
    </jar>
    <copy file="${temp.jar.path.Queue.jar}" tofile="${artifact.output.queue:jar}/Queue.jar"/>
    <copy file="${path.variable.gatk_root}/dist/activation-1.1.jar" tofile="${artifact.output.queue:jar}/activation-1.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/Aligner.jar" tofile="${artifact.output.queue:jar}/Aligner.jar"/>
    <copy file="${path.variable.gatk_root}/dist/AnalyzeCovariates.jar" tofile="${artifact.output.queue:jar}/AnalyzeCovariates.jar"/>
    <copy file="${path.variable.gatk_root}/dist/asm-all-3.3.1.jar" tofile="${artifact.output.queue:jar}/asm-all-3.3.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/bcel-5.2.jar" tofile="${artifact.output.queue:jar}/bcel-5.2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/cofoja-1.0-20110609.jar" tofile="${artifact.output.queue:jar}/cofoja-1.0-20110609.jar"/>
    <copy file="${path.variable.gatk_root}/dist/colt-1.2.0.jar" tofile="${artifact.output.queue:jar}/colt-1.2.0.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-codec-1.5.jar" tofile="${artifact.output.queue:jar}/commons-codec-1.5.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-email-1.2.jar" tofile="${artifact.output.queue:jar}/commons-email-1.2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-httpclient-3.1.jar" tofile="${artifact.output.queue:jar}/commons-httpclient-3.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-io-2.1.jar" tofile="${artifact.output.queue:jar}/commons-io-2.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-jexl-2.0.jar" tofile="${artifact.output.queue:jar}/commons-jexl-2.0.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-lang-2.5.jar" tofile="${artifact.output.queue:jar}/commons-lang-2.5.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-logging-1.1.1.jar" tofile="${artifact.output.queue:jar}/commons-logging-1.1.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/commons-math-2.2.jar" tofile="${artifact.output.queue:jar}/commons-math-2.2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/concurrent-1.3.4.jar" tofile="${artifact.output.queue:jar}/concurrent-1.3.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/dom4j-1.6.1.jar" tofile="${artifact.output.queue:jar}/dom4j-1.6.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/drmaa-6.2u5p2.jar" tofile="${artifact.output.queue:jar}/drmaa-6.2u5p2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/freemarker-2.3.18.jar" tofile="${artifact.output.queue:jar}/freemarker-2.3.18.jar"/>
    <copy file="${path.variable.gatk_root}/dist/GenomeAnalysisTK.jar" tofile="${artifact.output.queue:jar}/GenomeAnalysisTK.jar"/>
    <copy file="${path.variable.gatk_root}/dist/google-collections-1.0.jar" tofile="${artifact.output.queue:jar}/google-collections-1.0.jar"/>
    <copy file="${path.variable.gatk_root}/dist/gson-1.4.jar" tofile="${artifact.output.queue:jar}/gson-1.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/jakarta-regexp-1.4.jar" tofile="${artifact.output.queue:jar}/jakarta-regexp-1.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/Jama-1.0.2.jar" tofile="${artifact.output.queue:jar}/Jama-1.0.2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/java-xmlbuilder-0.4.jar" tofile="${artifact.output.queue:jar}/java-xmlbuilder-0.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/javassist-3.8.0.GA.jar" tofile="${artifact.output.queue:jar}/javassist-3.8.0.GA.jar"/>
    <copy file="${path.variable.gatk_root}/dist/jets3t-0.8.1.jar" tofile="${artifact.output.queue:jar}/jets3t-0.8.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/jgrapht-jdk1.5-0.7.3.jar" tofile="${artifact.output.queue:jar}/jgrapht-jdk1.5-0.7.3.jar"/>
    <copy file="${path.variable.gatk_root}/dist/jna-3.2.7.jar" tofile="${artifact.output.queue:jar}/jna-3.2.7.jar"/>
    <copy file="${path.variable.gatk_root}/dist/log4j-1.2.15.jar" tofile="${artifact.output.queue:jar}/log4j-1.2.15.jar"/>
    <copy file="${path.variable.gatk_root}/dist/logback-classic-0.9.9.jar" tofile="${artifact.output.queue:jar}/logback-classic-0.9.9.jar"/>
    <copy file="${path.variable.gatk_root}/dist/logback-core-0.9.9.jar" tofile="${artifact.output.queue:jar}/logback-core-0.9.9.jar"/>
    <copy file="${path.variable.gatk_root}/dist/mail-1.4.4.jar" tofile="${artifact.output.queue:jar}/mail-1.4.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/picard-1.59.1066.jar" tofile="${artifact.output.queue:jar}/picard-1.59.1066.jar"/>
    <copy file="${path.variable.gatk_root}/dist/picard-private-parts-2181.jar" tofile="${artifact.output.queue:jar}/picard-private-parts-2181.jar"/>
    <copy file="${path.variable.gatk_root}/dist/poi-3.8-beta3.jar" tofile="${artifact.output.queue:jar}/poi-3.8-beta3.jar"/>
    <copy file="${path.variable.gatk_root}/dist/poi-ooxml-3.8-beta3.jar" tofile="${artifact.output.queue:jar}/poi-ooxml-3.8-beta3.jar"/>
    <copy file="${path.variable.gatk_root}/dist/poi-ooxml-schemas-3.8-beta3.jar" tofile="${artifact.output.queue:jar}/poi-ooxml-schemas-3.8-beta3.jar"/>
    <copy file="${path.variable.gatk_root}/dist/reflections-0.9.5-RC2.jar" tofile="${artifact.output.queue:jar}/reflections-0.9.5-RC2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/sam-1.59.1066.jar" tofile="${artifact.output.queue:jar}/sam-1.59.1066.jar"/>
    <copy file="${path.variable.gatk_root}/dist/scala-compiler-2.8.1.jar" tofile="${artifact.output.queue:jar}/scala-compiler-2.8.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/scala-library-2.8.1.jar" tofile="${artifact.output.queue:jar}/scala-library-2.8.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/simple-xml-2.0.4.jar" tofile="${artifact.output.queue:jar}/simple-xml-2.0.4.jar"/>
    <copy file="${path.variable.gatk_root}/dist/slf4j-api-1.5.0.jar" tofile="${artifact.output.queue:jar}/slf4j-api-1.5.0.jar"/>
    <copy file="${path.variable.gatk_root}/dist/snpeff-2.0.5.jar" tofile="${artifact.output.queue:jar}/snpeff-2.0.5.jar"/>
    <copy file="${path.variable.gatk_root}/dist/stax-1.2.0.jar" tofile="${artifact.output.queue:jar}/stax-1.2.0.jar"/>
    <copy file="${path.variable.gatk_root}/dist/stax-api-1.0.1.jar" tofile="${artifact.output.queue:jar}/stax-api-1.0.1.jar"/>
    <copy file="${path.variable.gatk_root}/dist/StingUtils.jar" tofile="${artifact.output.queue:jar}/StingUtils.jar"/>
    <copy file="${path.variable.gatk_root}/dist/tribble-53.jar" tofile="${artifact.output.queue:jar}/tribble-53.jar"/>
    <copy file="${path.variable.gatk_root}/dist/vcf.jar" tofile="${artifact.output.queue:jar}/vcf.jar"/>
    <copy file="${path.variable.gatk_root}/dist/xml-apis-1.0.b2.jar" tofile="${artifact.output.queue:jar}/xml-apis-1.0.b2.jar"/>
    <copy file="${path.variable.gatk_root}/dist/xmlbeans-2.3.0.jar" tofile="${artifact.output.queue:jar}/xmlbeans-2.3.0.jar"/>
  </target>
  
  <target name="build.all.artifacts" depends="artifact.queue:jar" description="Build all artifacts">
    <!-- Delete temporary files -->
    <delete dir="${artifacts.temp.dir}"/>
  </target>

  <target name="all" depends="build.modules, build.all.artifacts" description="build all"/>
</project>
