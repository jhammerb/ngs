# Mt. Sinai School of Medicine Queue Scripts and Supporting Infrastructure

## Developing Queue Scripts

The GATK team at the Broad provides detailed [instructions for how to setup IntelliJ](http://www.broadinstitute.org/gsa/wiki/index.php/Queue_with_IntelliJ_IDEA),
a Java IDE, to develop Queue. Note that if you clone the *ngs* repository, its version of Queue is already setup
as an IntelliJ project. Just open it as an IntelliJ project.

The best examples for getting started developing qscripts are the working qscripts included in `queue/qscripts`. A few
notes, suggestions, gotchas, etc. for the intrepid qscript developer:

* If using multithreaded `CommandLineFunction`s make sure to set `this.nCoresRequest` with the desired number of threads
so that the job runners know how many to cores to request. This information propagates automatically for GATK walkers,
but not for command line functions.

## Building Queue

The following instructions assume you are not operating on the MSSM Minerva
cluster. If you are working on Minerva, there are already modules available for
your convenience (*gatk-mssm* and *gatk-queue-mssm*).

1. Clone the Broad's GATK repository (if you haven't already done so) and build GATK Queue.jar (see
[building Queue](http://www.broadinstitute.org/gsa/wiki/index.php/GATK-Queue#Building_Queue_from_source)
in the GATK official wiki for pre-requisites, and more detail about building GATK-Queue).

        $ git clone git://github.com/broadgsa/gatk.git
        $ cd gatk
        $ ant queue

1. Clone this repository (if you haven't already done so) and build the derived Queue.jar.

        $ git clone git@bitbucket.org:multiscale/ngs.git
        $ cd ngs/queue

    You will need to create a `build.properties` file with system specific paths for GATK and the Java SDK.

        path.variable.gatk_root=/path/to/gatk
        jdk.home.1.6=/path/to/your/JDK/home

    With the build.properties set, you can build the derived Queue.jar with

        $ ant all

    after which you can find Queue.jar at `/path/to/ngs/queue/dist/Queue.jar'.
