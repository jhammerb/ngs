/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

import edu.mssm.queue.function.FileCopierFunction
import org.broadinstitute.sting.gatk.phonehome.GATKRunReport
import org.broadinstitute.sting.queue.function.JavaCommandLineFunction
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.extensions.gatk._

trait QScriptCommon extends QScript {

  /* Required Parameters */
  @Input(doc="Reference FastA file", fullName="reference", shortName="R", required=true)
  var reference: File = _

  @Input(doc="dbSNP ROD file (must be in VCF format)", fullName="dbsnp", shortName="D", required=true)
  var dbSNP: File = _

  /* Optional Parameters */

  @Argument(doc="Project name. Determines the final filenames when appropriate.", fullName="project", shortName="p", required=false)
  var projectName: String = "project"

  @Argument(doc="GATK key file", fullName="gatk_key", shortName="K", required=false)
  var gatkKeyFile: File = _

  @Argument(doc="Output path", fullName="output_directory", shortName="outputDir", required=false)
  var outputDir: File = new File(".")

  @Argument(doc="Number of threads used by BWA, GATK, etc.", fullName="num_threads", shortName="nt", required=false)
  var numThreads: Int = 1

  @Input(doc="Expand each target by specified number of bases", shortName="expand", required=false)
  var expandIntervals: Int = 50

  @Input(doc="GATK intervals file (write BAMs only over intervals)", fullName="intervals", shortName="L", required=false)
  var intervals: File = _

  /* Global Variables */

  // Expanded intervals file created during analysis
  var flankIntervalsFile: File = _

  // Gracefully hide Queue's output
  def queueLogDir(): String = {
    outputDir.toString + java.io.File.separator + ".qlog" + java.io.File.separator
  }

  def prepareOutputDirectory() {
    logger.info("Creating output directory: " + outputDir.toString())
    outputDir.mkdirs()
  }

  def extendIntervals() {
    if (QScriptCommon.this.intervals != null && QScriptCommon.this.expandIntervals > 0) {
      flankIntervalsFile = new File(outputDir, QScriptCommon.this.projectName + ".flank.intervals")
      add( extendFlanks(QScriptCommon.this.intervals, flankIntervalsFile) )
    }
  }

  trait CommandLineJavaArgsHelper extends JavaCommandLineFunction {
    // This is a hack required by hard CPU limits. In addition we need to prevent Java from creating performance
    // monitoring files in /tmp (which is hard-coded by Java, see http://bugs.sun.com/view_bug.do?bug_id=6447182).
    // TODO: Fix more fundamentally!
    this.nCoresRequest = 2
    override def javaOpts = {
      super.javaOpts + required("-XX:+UseSerialGC") + required("-XX:+PerfDisableSharedMem")
    }
  }

  trait CommandLineGATKArgsHelper extends CommandLineGATK {
    if (QScriptCommon.this.gatkKeyFile != null) {
      this.phone_home = GATKRunReport.PhoneHomeOption.NO_ET
      this.gatk_key = gatkKeyFile
    }
    // This is a hack required by hard CPU limits. In addition we need to prevent Java from creating performance
    // monitoring files in /tmp (which is hard-coded by Java, see http://bugs.sun.com/view_bug.do?bug_id=6447182).
    // TODO: Fix more fundamentally!
    this.nCoresRequest = 2
    override def javaOpts = {
      super.javaOpts + required("-XX:+UseSerialGC") + required("-XX:+PerfDisableSharedMem")
    }
    this.reference_sequence = QScriptCommon.this.reference
    if (QScriptCommon.this.intervals != null) {
      this.intervals :+= QScriptCommon.this.intervals
    }
    if (QScriptCommon.this.flankIntervalsFile != null) {
      this.intervals :+= QScriptCommon.this.flankIntervalsFile
    }
  }

  case class copyFile(inFile: File, outFile: File) extends FileCopierFunction {
    this.inputFile = inFile
    this.outputFile = outFile
    this.analysisName = queueLogDir + outFile + ".cp"
    this.jobName = queueLogDir + outFile + ".cp"
  }

  case class extendFlanks(inIntervals: File, outIntervals: File) extends WriteFlankingIntervalsFunction {
    this.inputIntervals = inIntervals
    this.outputIntervals = outIntervals
    this.flankSize = QScriptCommon.this.expandIntervals
    this.reference = QScriptCommon.this.reference
    this.analysisName = queueLogDir + outIntervals + ".expandflank"
    this.jobName = queueLogDir + outIntervals + ".expandflank"
  }
}


