/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.engine

import org.broadinstitute.sting.queue.function.CommandLineFunction
import org.broadinstitute.sting.queue.engine.drmaa.DrmaaJobManager

class TorqueJobManager extends DrmaaJobManager {
  override def create(function: CommandLineFunction) = new TorqueJobRunner(session, function)
}
