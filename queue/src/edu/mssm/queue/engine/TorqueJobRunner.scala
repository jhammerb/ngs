/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.engine

import org.ggf.drmaa.Session
import org.broadinstitute.sting.queue.function.CommandLineFunction
import org.broadinstitute.sting.queue.engine.drmaa.DrmaaJobRunner
import org.broadinstitute.sting.queue.util.Logging

class TorqueJobRunner (session: Session, function: CommandLineFunction) extends DrmaaJobRunner(session, function) with Logging {

  protected override val jobNameLength = 15
  protected override val minRunnerPriority = -1024
  protected override val maxRunnerPriority = 1023

  override protected def functionNativeSpec = {
    var nativeSpec: String = ""

    // Ensure critical environment variables are passed to Queue-created jobs. In effect we are manually simulating
    // the '-V' option to 'qsub'.
    nativeSpec += (List("PATH", "CLASSPATH", "JAVA_HOME", "LD_LIBRARY_PATH").map {
      (key) => key + "=" + System.getenv(key)
    } mkString ("-v ", ",", " "))

    val memLimit : Int = (function.residentLimit.getOrElse(4.0) * 1024).toInt
    val reqCores : Int = function.nCoresRequest.getOrElse(1)

    // We ignore the queue specification to ensure that we can effectively work with the
    // Minerva queueing system
    if (reqCores == 1)
      nativeSpec += " -q serial" + " -l mem=%dmb".format(memLimit)
    else if (reqCores <= 32)
      nativeSpec += " -q small -l nodes=1:ppn=%d".format(math.max(reqCores,2)) + " -l mem=%dmb".format(memLimit)
    else
      nativeSpec += " -q medium"

    // Pass on any job resource requests, priority, etc.
    nativeSpec += function.jobResourceRequests.map(" -l " + _).mkString

    val priority = functionPriority
    if (priority.isDefined)
      nativeSpec += " -p " + priority.get

    // Force the standard output and error streams to be merged (this should happen in the DRMAAJobRunner
    // but that doesn't seem to be the case...)
    nativeSpec += " -j oe"

    logger.debug("Native spec is: %s".format(nativeSpec))
    (nativeSpec + " " + super.functionNativeSpec).trim()
  }
}
