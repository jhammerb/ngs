/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.function

import org.broadinstitute.sting.commandline.{Input, Output}
import java.io.File
import org.apache.commons.io.FileUtils

class FileCopierFunction extends org.broadinstitute.sting.queue.function.InProcessFunction {
  analysisName = "CopyFile"

  @Input(doc="Input file")
  var inputFile: File = _

  @Output(doc="Output file")
  var outputFile: File = _

  def run() {
    FileUtils.copyFile(inputFile, outputFile)
  }

}
