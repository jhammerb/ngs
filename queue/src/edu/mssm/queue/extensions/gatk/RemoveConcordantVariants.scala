/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.gatk

import org.broadinstitute.sting.queue.function.scattergather.ScatterGatherableFunction
import org.broadinstitute.sting.queue.extensions.gatk.{TaggedFile, VcfGatherFunction, LocusScatterFunction}
import org.broadinstitute.sting.commandline.{Argument, Gather, Output, Input}
import java.io.File

class RemoveConcordantVariants extends org.broadinstitute.sting.queue.extensions.gatk.CommandLineGATK with ScatterGatherableFunction {
  analysisName = "RemoveConcordantVariants"
  analysis_type = "RemoveConcordantVariants"
  scatterClass = classOf[LocusScatterFunction]

  /** Input VCF file */
  @Input(fullName="variant", shortName="V", doc="Input VCF file", required=true, exclusiveOf="", validation="")
  var variant: File = _

  /**
   * Short name of variant
   * @return Short name of variant
   */
  def V = this.variant

  /**
   * Short name of variant
   * @param value Short name of variant
   */
  def V_=(value: File) { this.variant = value }

  /** Dependencies on the index of variant */
  @Input(fullName="variantIndex", shortName="", doc="Dependencies on the index of variant", required=false, exclusiveOf="", validation="")
  private var variantIndex: Seq[File] = Nil

  /** Output variants that were also called in this comparison track */
  @Input(fullName="concordance", shortName="conc", doc="Output variants that were also called in this comparison track", required=true, exclusiveOf="", validation="")
  var concordance: File = _

  /**
   * Short name of concordance
   * @return Short name of concordance
   */
  def conc = this.concordance

  /**
   * Short name of concordance
   * @param value Short name of concordance
   */
  def conc_=(value: File) { this.concordance = value }

  /** Dependencies on the index of concordance */
  @Input(fullName="concordanceIndex", shortName="", doc="Dependencies on the index of concordance", required=false, exclusiveOf="", validation="")
  private var concordanceIndex: Seq[File] = Nil


  /** File to which variants should be written */
  @Output(fullName="out", shortName="o", doc="File to which variants should be written", required=false, exclusiveOf="", validation="")
  @Gather(classOf[VcfGatherFunction])
  var out: File = _

  /**
   * Short name of out
   * @return Short name of out
   */
  def o = this.out

  /**
   * Short name of out
   * @param value Short name of out
   */
  def o_=(value: File) { this.out = value }

  /** Automatically generated index for out */
  @Output(fullName="outIndex", shortName="", doc="Automatically generated index for out", required=false, exclusiveOf="", validation="")
  @Gather(enabled=false)
  private var outIndex: File = _

  /** Don't output the usual VCF header tag with the command line. FOR DEBUGGING PURPOSES ONLY. This option is required in order to pass integration tests. */
  @Argument(fullName="NO_HEADER", shortName="NO_HEADER", doc="Don't output the usual VCF header tag with the command line. FOR DEBUGGING PURPOSES ONLY. This option is required in order to pass integration tests.", required=false, exclusiveOf="", validation="")
  var NO_HEADER: Boolean = _


  /** Don't include loci found to be non-variant after the subsetting procedure */
  @Argument(shortName="reset_filter", doc="Reset filter values to '.'", required=false, exclusiveOf="", validation="")
  var resetFilter: Boolean = _

  /**
   * Short name of resetFilter
   * @return Short name of resetFilter
   */
  def reset_filter = this.resetFilter

  /**
   * Short name of resetFilter
   * @param value Short name of resetFilter
   */
  def reset_filter_=(value: Boolean) { this.resetFilter = value }

  override def freezeFieldValues() {
    super.freezeFieldValues()
    if (variant != null)
      variantIndex :+= new File(variant.getPath + ".idx")
    if (concordance != null)
      concordanceIndex :+= new File(concordance.getPath + ".idx")
    if (out != null && !org.broadinstitute.sting.utils.io.IOUtils.isSpecialFile(out))
      if (!org.broadinstitute.sting.gatk.io.stubs.VCFWriterArgumentTypeDescriptor.isCompressed(out.getPath))
        outIndex = new File(out.getPath + ".idx")
  }

  override def commandLine = super.commandLine +
    required(TaggedFile.formatCommandLineParameter("-V", variant), variant, spaceSeparated=true, escape=true, format="%s") +
    optional(TaggedFile.formatCommandLineParameter("-conc", concordance), concordance, spaceSeparated=true, escape=true, format="%s") +
    optional("-o", out, spaceSeparated=true, escape=true, format="%s") + conditional(NO_HEADER, "-NO_HEADER", escape=true, format="%s") +
    conditional(resetFilter, "-reset_filter", escape=true, format="%s")

}
