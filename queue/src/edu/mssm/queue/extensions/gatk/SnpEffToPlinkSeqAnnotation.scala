/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.gatk

import org.broadinstitute.sting.queue.function.scattergather.ScatterGatherableFunction
import org.broadinstitute.sting.queue.extensions.gatk.{TaggedFile, LocusScatterFunction}
import org.broadinstitute.sting.commandline.{Argument, Gather, Output, Input}
import java.io.File

class SnpEffToPlinkSeqAnnotation extends org.broadinstitute.sting.queue.extensions.gatk.CommandLineGATK with ScatterGatherableFunction {
  analysisName = "SnpEffToPlinkSeqAnnotation"
  analysis_type = "SnpEffToPlinkSeqAnnotation"
  scatterClass = classOf[LocusScatterFunction]

  /** Input VCF file */
  @Input(fullName="variant", shortName="V", doc="Input VCF file", required=true, exclusiveOf="", validation="")
  var variant: File = _

  /**
   * Short name of variant
   * @return Short name of variant
   */
  def V = this.variant

  /**
   * Short name of variant
   * @param value Short name of variant
   */
  def V_=(value: File) { this.variant = value }

  /** Dependencies on the index of variant */
  @Input(fullName="variantIndex", shortName="", doc="Dependencies on the index of variant", required=false, exclusiveOf="", validation="")
  private var variantIndex: Seq[File] = Nil

  /** File to which results should be written */
  @Output(fullName="out", shortName="o", doc="File to which results should be written", required=false, exclusiveOf="", validation="")
  @Gather(classOf[org.broadinstitute.sting.queue.function.scattergather.SimpleTextGatherFunction])
  var out: File = _

  /**
   * Short name of out
   * @return Short name of out
   */
  def o = this.out

  /**
   * Short name of out
   * @param value Short name of out
   */
  def o_=(value: File) { this.out = value }

  /** Don't report non-coding effects */
  @Argument(shortName="remove_non_coding", doc="Do not report non-coding effects", required=false, exclusiveOf="", validation="")
  var removeNonCoding: Boolean = _

  /**
   * Short name of resetFilter
   * @return Short name of resetFilter
   */
  def remove_non_coding = this.removeNonCoding

  /**
   * Short name of resetFilter
   * @param value Short name of resetFilter
   */
  def remove_non_coding_=(value: Boolean) { this.removeNonCoding = value }

  override def freezeFieldValues() {
    super.freezeFieldValues()
    if (variant != null)
      variantIndex :+= new File(variant.getPath + ".idx")
  }

  override def commandLine = super.commandLine +
    required(TaggedFile.formatCommandLineParameter("-V", variant), variant, spaceSeparated=true, escape=true, format="%s") +
    optional("-o", out, spaceSeparated=true, escape=true, format="%s") +
    conditional(removeNonCoding, "-remove_non_coding", escape=true, format="%s")
}
