/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.snpeff

import org.broadinstitute.sting.queue.function.JavaCommandLineFunction
import java.io.File
import org.broadinstitute.sting.commandline.{Output, Argument, Input}

class SnpEff extends JavaCommandLineFunction {
  javaMainClass = "ca.mcgill.mcb.pcingola.snpEffect.commandLine.SnpEff"

  @Input(doc="VCF variant file")
  var inVcf: File = _

  @Input(doc="snpEff configuration file (with path to data directory)", required=false)
  var config: File = _

  @Argument(doc="Genome version, e.g. hg19")
  var genomeVersion: String = _

  @Argument(doc="Verbose", required=false)
  var verbose = false

  @Argument(doc="Treat all as protein coding", required=false)
  var treatAllAsProteinCoding: Option[Boolean] = None

  @Argument(doc="Do not report usage statistics to the server", required=false)
  var noLog = true

  @Output(doc="Stats/summary output file", required=false)
  var outSummary: File = _

  @Output(doc="Output VCF file")
  var outVcf: File = _

  override def commandLine = super.commandLine +
    required("eff") +
    conditional(verbose, "-v") +
    optional("-c", config) +
    required("-i", "vcf") +
    required("-o", "gatk") +
    optional("-s", outSummary) +
    conditional(noLog, "-noLog") +
    optional("-treatAllAsProteinCoding", treatAllAsProteinCoding) +
    required(genomeVersion) +
    required(inVcf) +
    required(">", escape=false) +
    required(outVcf)
}
