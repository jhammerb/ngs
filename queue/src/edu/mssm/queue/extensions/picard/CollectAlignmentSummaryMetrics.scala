/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.commandline._
import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import java.io.File

class CollectAlignmentSummaryMetrics extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "CollectAlignmentSummaryMetrics"
  javaMainClass = "net.sf.picard.analysis.CollectAlignmentSummaryMetrics"

  @Input(doc="The input SAM or BAM files to analyze.  Must be coordinate sorted.", shortName = "input", fullName = "input_bam_files", required = true)
  var input: Seq[File] = Nil

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=false)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Stop after processing N reads", shortName = "stop", fullName = "stop_after", required = false)
  var STOP_AFTER: Int = 0

  @Argument(doc="Is bisulfite sequenced?", shortName = "bi", fullName = "is_bisulfite_sequenced", required = false)
  var IS_BISULFITE_SEQUENCED: Boolean = false

  @Argument(doc="Metric accumulation level. One or more of ALL_READS, SAMPLE, LIBRARY, READ_GROUP.", shortName="acc", fullName="metric_accumulation_level", required=false)
  var METRIC_ACCUMULATION_LEVEL: Seq[String] = _

  @Argument(doc="Max insert size. Reads above this size will be considered chimeric.", shortName = "ins", fullName = "max_insert_size", required = false)
  var MAX_INSERT_SIZE: Int = 100000

  @Argument(doc="Adapter sequence", shortName = "adp", fullName = "adapter_sequence", required = false)
  var ADAPTER_SEQUENCE: String = null

  this.sortOrder = null
  override def inputBams = input
  override def outputBam = output
  override def commandLine = super.commandLine +
    optional("ADAPTER_SEQUENCE=", ADAPTER_SEQUENCE, spaceSeparated=false) +
    optional("MAX_INSERT_SIZE=", MAX_INSERT_SIZE, spaceSeparated=false) +
    optional("REFERENCE_SEQUENCE=", REFERENCE_SEQUENCE, spaceSeparated=false) +
    optional("STOP_AFTER=", STOP_AFTER, spaceSeparated=false) +
    optional("IS_BISULFITE_SEQUENCED=", IS_BISULFITE_SEQUENCED, spaceSeparated=false) +
    repeat("METRIC_ACCUMULATION_LEVEL=", METRIC_ACCUMULATION_LEVEL, spaceSeparated=false)  // repeat() returns "" for null/empty list
}
