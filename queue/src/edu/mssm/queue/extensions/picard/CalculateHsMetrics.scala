/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.commandline._
import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import java.io.File

class CalculateHsMetrics extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "CollectHsMetrics"
  javaMainClass = "net.sf.picard.analysis.directed.CalculateHsMetrics"

  @Input(doc="The input SAM or BAM files to analyze.  Must be coordinate sorted.", shortName = "input", fullName = "input_bam_files", required = true)
  var input: Seq[File] = Nil

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Argument(doc="Intervals file contain locations of baits used", shortName = "bi", fullName ="bait_intervals", required=true)
  var BAIT_INTERVALS: File = _

  @Argument(doc="Intervals file contain locations of targets", shortName = "ti", fullName ="target_intervals", required=true)
  var TARGET_INTERVALS: File = _

  @Argument(doc="Bait set name. If not provided, inferred from bait_intervals file name", shortName="bsn", fullName="bait_set_name", required=false)
  var BAIT_SET_NAME: String = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=false)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Metric accumulation level. One or more of ALL_READS, SAMPLE, LIBRARY, READ_GROUP.", shortName="mal", fullName="metric_accumulation_level", required=false)
  var METRIC_ACCUMULATION_LEVEL: Seq[String] = _

  @Argument(doc="Output file for per-target coverage", shortName="ptc", fullName="per_target_coverage", required=false)
  var PER_TARGET_COVERAGE: File = _

  this.sortOrder = null
  override def inputBams = input
  override def outputBam = output
  override def commandLine = super.commandLine +
    required("BAIT_INTERVALS=", BAIT_INTERVALS, spaceSeparated=false) +
    required("TARGET_INTERVALS=", TARGET_INTERVALS, spaceSeparated=false) +
    optional("BAIT_SET_NAME=", BAIT_SET_NAME, spaceSeparated=false) +
    optional("REFERENCE_SEQUENCE=", REFERENCE_SEQUENCE, spaceSeparated=false) +
    optional("PER_TARGET_COVERAGE=", PER_TARGET_COVERAGE, spaceSeparated=false) +
    repeat("METRIC_ACCUMULATION_LEVEL=", METRIC_ACCUMULATION_LEVEL, spaceSeparated=false)  // repeat() returns "" for null/empty list
}
