/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import org.broadinstitute.sting.commandline.{Argument, Output, Input}
import java.io.File
import net.sf.picard.util.FastqQualityFormat

class FastqToSam extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "FastqToSam"
  javaMainClass = "net.sf.picard.sam.FastqToSam"

  @Input(doc="Input fastq file for SE, or first read in PE", shortName="FASTQ", required=true)
  var FASTQ: File = _

  @Input(doc="Input fastq file for SE, or first read in PE", shortName="FASTQ2", required=false)
  var FASTQ2: File = _

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Output(doc="The output bam index", shortName = "out_index", fullName = "output_bam_index_file", required = false)
  var outputIndex: File = null

  @Argument(doc="Sample name", shortName = "sample", fullName="sample_name", required=true)
  var SAMPLE_NAME: String = _

  // Read group metadata
  var READ_GROUP_NAME: String = null
  var LIBRARY_NAME: String = null
  var PLATFORM_UNIT: String = null
  var PLATFORM: String = null
  var SEQUENCING_CENTER: String = null
  var COMMENT: String = null
  var DESCRIPTION: String = null
  var RUN_DATE: String = null

  @Argument(doc="How the quality values are encoded in the FASTQ. One of Solexa (Illumina < 1.3), Illumina (1.3 - 1.7) or Standard (Illumina 1.8+)", shortName="quality", fullName="quality_format")
  var QUALITY_FORMAT: FastqQualityFormat = FastqQualityFormat.Standard

  override def freezeFieldValues() {
    super.freezeFieldValues()
    if (outputIndex == null && output != null)
      outputIndex = new File(output.getName.stripSuffix(".bam") + ".bai")
  }

  override def inputBams = null
  override def outputBam = output
  this.createIndex = Some(true)
  override def commandLine = super.commandLine +
    required("FASTQ=" + FASTQ) +
    optional("FASTQ2=", FASTQ2, spaceSeparated = false) +
    optional("QUALITY_FORMAT=", QUALITY_FORMAT, spaceSeparated = false) +
    required("SAMPLE_NAME=" + SAMPLE_NAME) +
    optional("READ_GROUP_NAME=", READ_GROUP_NAME, spaceSeparated = false) +
    optional("LIBRARY_NAME=", LIBRARY_NAME, spaceSeparated = false) +
    optional("PLATFORM_UNIT=", PLATFORM_UNIT, spaceSeparated = false) +
    optional("PLATFORM=", PLATFORM, spaceSeparated = false) +
    optional("SEQUENCING_CENTER=", SEQUENCING_CENTER, spaceSeparated = false) +
    optional("COMMENT=", COMMENT, spaceSeparated = false) +
    optional("DESCRIPTION=", DESCRIPTION, spaceSeparated = false) +
    optional("RUN_DATE=", RUN_DATE, spaceSeparated = false)
}
