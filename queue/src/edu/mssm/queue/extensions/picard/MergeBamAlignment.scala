/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import org.broadinstitute.sting.commandline.{Input, Argument, Output}
import java.io.File
import tools.nsc.doc.model.comment.Bold


class MergeBamAlignment extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "MergeBamAlignment"
  javaMainClass = "net.sf.picard.sam.MergeBamAlignment"

  @Input(doc="Original SAM or BAM file of unmapped reads, which must be in queryname order.", shortName="unmapped", fullName="unmapped_bam", required=true)
  var UNMAPPED_BAM: File = _

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Output(doc="The output bam index", shortName = "out_index", fullName = "output_bam_index_file", required = false)
  var outputIndex: File = _

  @Input(doc="File SAM or BAM file(s) with alignment data", shortName="aligned", fullName="aligned_bam")
  var ALIGNED_BAM: File = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=true)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Is paired-end run", shortName="paired", fullName="paired_run", required=true)
  var PAIRED_RUN: Boolean = _

  var IS_BISULFITE_SEQUENCE: Boolean = false
  var ALIGNED_READS_ONLY: Boolean = false
  var MAX_INSERTIONS_OR_DELETIONS: Int = 1
  var CLIP_OVERLAPPING_READS: Boolean = true

  override def freezeFieldValues() {
    super.freezeFieldValues()
    if (outputIndex == null && output != null)
      outputIndex = new File(output.getName.stripSuffix(".bam") + ".bai")
  }

  override def inputBams = null
  override def outputBam = output
  this.createIndex = Some(true)
  override def commandLine = super.commandLine +
    required("UNMAPPED_BAM=" + UNMAPPED_BAM) +
    required("ALIGNED_BAM=" + ALIGNED_BAM) +
    required("PAIRED_RUN=" + PAIRED_RUN) +
    required("REFERENCE_SEQUENCE=" + REFERENCE_SEQUENCE) +
    optional("IS_BISULFITE_SEQUENCE=", IS_BISULFITE_SEQUENCE, spaceSeparated = false) +
    optional("ALIGNED_READS_ONLY=", ALIGNED_READS_ONLY, spaceSeparated = false) +
    optional("MAX_INSERTIONS_OR_DELETIONS=", MAX_INSERTIONS_OR_DELETIONS, spaceSeparated = false) +
    optional("CLIP_OVERLAPPING_READS=", CLIP_OVERLAPPING_READS, spaceSeparated = false)

}