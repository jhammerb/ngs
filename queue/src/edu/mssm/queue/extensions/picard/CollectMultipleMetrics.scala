/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.commandline._
import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import java.io.File

class CollectMultipleMetrics extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "CollectMultipleMetrics"
  javaMainClass = "net.sf.picard.analysis.CollectMultipleMetrics"

  @Input(doc="The input SAM or BAM files to analyze.  Must be coordinate sorted.", shortName = "input", fullName = "input_bam_files", required = true)
  var input: Seq[File] = Nil

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=false)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Stop after processing N reads", shortName = "stop", fullName = "stop_after", required = false)
  var STOP_AFTER: Int = 0

  @Argument(doc="Programs to run. One or more of CollectAlignmentSummaryMetrics, CollectInsertSizeMetrics, QualityScoreDistribution, MeanQualityByCycle.", shortName="prog", fullName="program", required=false)
  var PROGRAM: Seq[String] = _

  this.sortOrder = null
  override def inputBams = input
  override def outputBam = output
  override def commandLine = super.commandLine +
    optional("REFERENCE_SEQUENCE=", REFERENCE_SEQUENCE, spaceSeparated=false) +
    optional("STOP_AFTER=", STOP_AFTER, spaceSeparated=false) +
    repeat("PROGRAM=", PROGRAM, spaceSeparated=false)  // repeat() returns "" for null/empty list

}
