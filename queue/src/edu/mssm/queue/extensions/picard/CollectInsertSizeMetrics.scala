/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.commandline._
import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import java.io.File

class CollectInsertSizeMetrics extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "CollectInsertSizeMetrics"
  javaMainClass = "net.sf.picard.analysis.CollectInsertSizeMetrics"

  @Input(doc="The input SAM or BAM files to analyze.  Must be coordinate sorted.", shortName = "input", fullName = "input_bam_files", required = true)
  var input: Seq[File] = Nil

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=false)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Stop after processing N reads", shortName = "stop", fullName = "stop_after", required = false)
  var STOP_AFTER: Int = 0

  @Argument(doc="Metric accumulation level. One or more of ALL_READS, SAMPLE, LIBRARY, READ_GROUP.", shortName="acc", fullName="metric_accumulation_level", required=false)
  var METRIC_ACCUMULATION_LEVEL: Seq[String] = _

  @Output(doc="Histogram size chart", shortName = "chart", fullName = "histogram_file", required = true)
  var HISTOGRAM_FILE: File = _

  @Argument(doc="Deviations", shortName = "dev", fullName = "deviations", required = false)
  var DEVIATIONS: Double = 10.0

  @Argument(doc="Histogram width", shortName = "hist", fullName = "histogram_width", required = false)
  var HISTOGRAM_WIDTH: Option[Int] = None

  @Argument(doc="Minimum percentage of reads to consider read category", shortName = "pct", fullName = "minimum_pct", required = false)
  var MINIMUM_PCT: Double = 0.05

  this.sortOrder = null
  override def inputBams = input
  override def outputBam = output
  override def commandLine = super.commandLine +
    required("HISTOGRAM_FILE=", HISTOGRAM_FILE, spaceSeparated=false) +
    optional("DEVIATIONS=", DEVIATIONS, spaceSeparated=false) +
    optional("HISTOGRAM_WIDTH=", HISTOGRAM_WIDTH, spaceSeparated=false) +
    optional("MINIMUM_PCT=", MINIMUM_PCT, spaceSeparated=false) +
    optional("REFERENCE_SEQUENCE=", REFERENCE_SEQUENCE, spaceSeparated=false) +
    optional("STOP_AFTER=", STOP_AFTER, spaceSeparated=false) +
    repeat("METRIC_ACCUMULATION_LEVEL=", METRIC_ACCUMULATION_LEVEL, spaceSeparated=false)  // repeat() returns "" for null/empty list
}
