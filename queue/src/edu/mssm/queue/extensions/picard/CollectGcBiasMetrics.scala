/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.extensions.picard

import org.broadinstitute.sting.commandline._
import org.broadinstitute.sting.queue.extensions.picard.PicardBamFunction
import java.io.File

class CollectGcBiasMetrics extends org.broadinstitute.sting.queue.function.JavaCommandLineFunction with PicardBamFunction {
  analysisName = "CollectMultipleMetrics"
  javaMainClass = "net.sf.picard.analysis.CollectGcBiasMetrics"

  @Input(doc="The input SAM or BAM files to analyze.  Must be coordinate sorted.", shortName = "input", fullName = "input_bam_files", required = true)
  var input: Seq[File] = Nil

  @Output(doc="Output file", shortName = "output", fullName = "output_file", required = true)
  var output: File = _

  @Output(doc="PDF file to render chart to", shortName = "chart", fullName = "chart_output", required = true)
  var CHART_OUTPUT: File = _

  @Output(doc="Summary metrics output file", shortName = "summary", fullName = "summary_output", required = true)
  var SUMMARY_OUTPUT: File = _

  @Argument(doc="Reference sequence file", shortName = "ref", fullName ="reference_sequence", required=true)
  var REFERENCE_SEQUENCE: File = _

  @Argument(doc="Window size used to bin reads", shortName="window", fullName="windows_size", required=false)
  var WINDOW_SIZE: Int = 100

  @Argument(doc="Exclude GC windows that fraction of genome less than threshold", shortName="fraction", fullName="minimum_genome_fraction", required=false)
  var MINIMUM_GENOME_FRACTION: Double = 0.00001

  this.sortOrder = null
  override def inputBams = input
  override def outputBam = output
  override def commandLine = super.commandLine +
    required("CHART_OUTPUT=", CHART_OUTPUT, spaceSeparated = false) +
    required("SUMMARY_OUTPUT=", SUMMARY_OUTPUT, spaceSeparated = false) +
    required("REFERENCE_SEQUENCE=", REFERENCE_SEQUENCE, spaceSeparated=false) +
    optional("WINDOW_SIZE=", WINDOW_SIZE, spaceSeparated=false) +
    optional("MINIMUM_GENOME_FRACTION=", MINIMUM_GENOME_FRACTION, spaceSeparated=false)
}
