/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.variantutils;

import org.broadinstitute.sting.commandline.ArgumentCollection;
import org.broadinstitute.sting.commandline.Input;
import org.broadinstitute.sting.commandline.Output;
import org.broadinstitute.sting.commandline.RodBinding;
import org.broadinstitute.sting.gatk.arguments.StandardVariantContextInputArgumentCollection;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.Reference;
import org.broadinstitute.sting.gatk.walkers.RodWalker;
import org.broadinstitute.sting.gatk.walkers.Window;
import org.broadinstitute.sting.utils.SampleUtils;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;
import org.broadinstitute.sting.utils.variantcontext.VariantContextBuilder;
import org.broadinstitute.sting.utils.variantcontext.VariantContextUtils;

import java.util.*;

@Reference(window=@Window(start=0,stop=100))
public class ReformatESPVCFs extends RodWalker<Integer, Integer> {

    @Input(fullName="variant", shortName = "V", doc="Input VCF file", required=true)
    public List<RodBinding<VariantContext>> variants;

    @Output(doc="File to which variants should be written",required=true)
    protected VCFWriter baseWriter = null;
    private SortingVCFWriter vcfWriter;

    public void initialize() {
        vcfWriter = new SortingVCFWriter(baseWriter, 40, false);

        Map<String, VCFHeader> vcfRods = VCFUtils.getVCFHeadersFromRods(getToolkit());
        TreeSet<String> vcfSamples = new TreeSet<String>(SampleUtils.getSampleList(vcfRods, VariantContextUtils.GenotypeMergeType.REQUIRE_UNIQUE));

        // Initialize VCF header
        Set<VCFHeaderLine> headerLines = VCFUtils.smartMergeHeaders(vcfRods.values(), logger);
        headerLines.add(new VCFHeaderLine("source", "ReformatESPVCFs"));

        headerLines.add(new VCFInfoHeaderLine("AF", 1, VCFHeaderLineType.Float, "Global Allele Frequency based on AC/AN"));
        headerLines.add(new VCFInfoHeaderLine("EUR_AF", 1, VCFHeaderLineType.Float, "Allele Frequency for European American samples based on AC/AN"));
        headerLines.add(new VCFInfoHeaderLine("AFR_AF", 1, VCFHeaderLineType.Float, "Allele Frequency for African American samples based on AC/AN"));

        vcfWriter.writeHeader(new VCFHeader(headerLines, vcfSamples));
    }

    @Override
    public Integer map(RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        if ( tracker == null ) {
            return 0;
        }

        Collection<VariantContext> vcs = tracker.getValues(variants, context.getLocation());

        if ( vcs == null || vcs.size() == 0) {
            return 0;
        }

        for (VariantContext vc : vcs) {
            VariantContextBuilder builder = new VariantContextBuilder(vc);

            builder.attribute("AF", AFFromStringVector(vc.getAttribute("TAC")));
            builder.attribute("EUR_AF", AFFromStringVector(vc.getAttribute("EA_AC")));
            builder.attribute("AFR_AF", AFFromStringVector(vc.getAttribute("AA_AC")));

            vcfWriter.add(builder.make());
        }

        return 1;
    }

    static String AFFromStringVector(Object vector) {
        if (vector == null)
            return null;
        ArrayList<String> acs = (ArrayList<String>)vector;
        Integer alt = Integer.parseInt(acs.get(0)), ref = Integer.parseInt(acs.get(1));
        return String.format("%.5f", alt.doubleValue() / (alt.intValue() + ref.intValue()));
    }

    @Override
    public Integer reduceInit() {
        return 0;
    }

    @Override
    public Integer reduce(Integer value, Integer sum) {
        return sum + value;
    }
}
