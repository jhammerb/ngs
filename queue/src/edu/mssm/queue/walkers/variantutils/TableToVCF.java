/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.variantutils;

import org.broad.tribble.Feature;
import org.broadinstitute.sting.commandline.ArgumentCollection;
import org.broadinstitute.sting.commandline.Input;
import org.broadinstitute.sting.commandline.Output;
import org.broadinstitute.sting.commandline.RodBinding;
import org.broadinstitute.sting.gatk.arguments.DbsnpArgumentCollection;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.refdata.VariantContextAdaptors;
import org.broadinstitute.sting.gatk.walkers.Reference;
import org.broadinstitute.sting.gatk.walkers.RodWalker;
import org.broadinstitute.sting.gatk.walkers.Window;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.codecs.table.TableFeature;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.variantcontext.Allele;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;
import org.broadinstitute.sting.utils.variantcontext.VariantContextBuilder;

import java.util.*;

@Reference(window=@Window(start=-40,stop=40))
public class TableToVCF extends RodWalker<Integer, Integer>  {

    @Output(doc="File to which variants should be written",required=true)
    protected VCFWriter baseWriter = null;
    private SortingVCFWriter vcfWriter;

    /**
     * Variants from this input file are used by this tool as input.
     */
    @Input(fullName="sites", shortName="V", doc="Input sites file", required=true)
    public RodBinding<TableFeature> sites;

    @Input(fullName="headers", shortName="H", doc="Header lines for VCF", required=true)
    public List<String> headers;


    @ArgumentCollection
    protected DbsnpArgumentCollection dbsnp = new DbsnpArgumentCollection();

    public class CommandLineHeaderLine extends VCFInfoHeaderLine {
        public CommandLineHeaderLine(String line) {
            super(line, VCFHeaderVersion.VCF4_1);
        }
    }



    public void initialize() {
        vcfWriter = new SortingVCFWriter(baseWriter, 40, false);

        // Setup the header fields
        Set<VCFHeaderLine> hInfo = new HashSet<VCFHeaderLine>();
        hInfo.addAll(VCFUtils.getHeaderFields(getToolkit(), Arrays.asList(sites.getName())));
        for (String line : headers) {
            hInfo.add(new CommandLineHeaderLine(line));
        }
        vcfWriter.writeHeader(new VCFHeader(hInfo));
    }

    @Override
    public Integer map(RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        if (tracker == null)
            return 0;

        String rsID = null;
        if (dbsnp != null) {
            rsID = VCFUtils.rsIDOfFirstRealVariant(tracker.getValues(dbsnp.dbsnp, context.getLocation()), VariantContext.Type.SNP);
        }

        Collection<TableFeature> contexts = tracker.getValues(sites, ref.getLocus());
        if ( contexts == null || contexts.size() == 0) {
            return 0;
        }

        for (TableFeature sc : contexts) {
            HashSet<Allele> alleles = new HashSet<Allele>();
            alleles.add(Allele.create(ref.getBase(), true));
            VariantContextBuilder builder = new VariantContextBuilder(
                sites.getName(),
                sc.getChr(),
                sc.getStart(),
                sc.getEnd(),
                alleles
            );
            if (rsID != null)
                builder.id(rsID);

            List<String> keys   = sc.getHeader();
            List<String> values = sc.getAllValues();

            // First column contains position, which we don't need...
            Map<String, Object> annotations = new LinkedHashMap<String, Object>(Utils.optimumHashSize(1));
            for (int i=1; i<keys.size(); i++) {
                annotations.put(keys.get(i), values.get(i));
            }
            builder.attributes(annotations);

            vcfWriter.add(builder.make());
        }

        return 1;
    }

    @Override
    public Integer reduceInit() {
        return 0;
    }

    @Override
    public Integer reduce(Integer value, Integer sum) {
        return value + sum;
    }

    public void onTraversalDone(Integer sum) {
        vcfWriter.close();
    }
}
