/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.variantutils;

import org.broadinstitute.sting.commandline.*;
import org.broadinstitute.sting.gatk.arguments.StandardVariantContextInputArgumentCollection;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.RodWalker;
import org.broadinstitute.sting.gatk.walkers.annotator.SnpEff;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.codecs.vcf.VCFHeader;
import org.broadinstitute.sting.utils.codecs.vcf.VCFHeaderLine;
import org.broadinstitute.sting.utils.codecs.vcf.VCFUtils;
import org.broadinstitute.sting.utils.exceptions.UserException;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;


import java.io.PrintStream;
import java.util.*;

/**
 * Generate PlinkSeq compatible annotations for use in downstream analysis
 *
 * Generate PlinkSeq compatible annotations (http://atgu.mgh.harvard.edu/plinkseq/varannot.shtml#meta)
 * specifically identifying compound heterozygous variants at the transcript level.
 */

public class SnpEffToPlinkSeqAnnotation extends RodWalker<Integer, Integer> {
    @ArgumentCollection
    protected StandardVariantContextInputArgumentCollection variantCollection = new StandardVariantContextInputArgumentCollection();

    @Argument(shortName = "remove_non_coding", doc="Do not report non-coding effects", required=false)
    protected boolean removeNonCoding = true;

    @Output(doc="File to which results should be written",required=true)
    protected PrintStream out;

    public void initialize() {

        Map<String, VCFHeader> headers = VCFUtils.getVCFHeadersFromRods(getToolkit(), Arrays.asList(variantCollection.variants));
        if (headers.size() > 1) {
            throw new UserException("Only one input VCF supported at this time");
        }
        VCFHeader snpEffVCFHeader = headers.get(variantCollection.variants.getName());  // Get one and only header
        VCFHeaderLine snpEffVersionLine = snpEffVCFHeader.getOtherHeaderLine(SnpEff.SNPEFF_VCF_HEADER_VERSION_LINE_KEY);
        VCFHeaderLine snpEffCommandLine = snpEffVCFHeader.getOtherHeaderLine(SnpEff.SNPEFF_VCF_HEADER_COMMAND_LINE_KEY);

        SnpEff.checkSnpEffVersion(snpEffVersionLine);
        SnpEff.checkSnpEffCommandLine(snpEffCommandLine);

        // print out the header
        out.println("##func,.,String,\"Genomic Annotation\"");
        out.println("##transcript,.,String,\"Transcript ID\"");
    }

    @Override
    public Integer map(RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        if ( tracker == null ) {
            return 0;
        }

        Collection<VariantContext> vcs = tracker.getValues(variantCollection.variants, context.getLocation());

        if ( vcs == null || vcs.size() == 0) {
            return 0;
        }

        for (VariantContext vc : vcs) {
            List<SnpEff.SnpEffEffect> effects = SnpEff.parseSnpEffRecord(vc);
            if (effects.size() == 0) {
                continue;
            }

            List<String> funcs = new ArrayList<String>(effects.size());
            List<String> transcripts = new ArrayList<String>(effects.size());
            for (SnpEff.SnpEffEffect effect : effects) {
                if (!effect.isWellFormed() || (removeNonCoding && !effect.isCoding())) {
                    continue;
                }
                funcs.add(effect.getEffect().toString());
                transcripts.add(effect.getTranscriptID().equals("") ? "." : effect.getTranscriptID());
            }
            if (funcs.size() > 0) {
                out.println(context.getLocation().toString() + "\tfunc\t" + Utils.join(",",funcs));
            }
            if (transcripts.size() > 0) {
                out.println(context.getLocation().toString() + "\ttranscript\t" + Utils.join(",",transcripts));
            }
        }

        return 1;
    }

    @Override
    public Integer reduceInit() {
        return 0;
    }

    @Override
    public Integer reduce(Integer value, Integer sum) {
        return sum + value;
    }
}
