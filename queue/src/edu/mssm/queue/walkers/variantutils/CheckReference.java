/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.variantutils;

import org.broadinstitute.sting.commandline.ArgumentCollection;
import org.broadinstitute.sting.commandline.Output;
import org.broadinstitute.sting.gatk.arguments.StandardVariantContextInputArgumentCollection;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.Reference;
import org.broadinstitute.sting.gatk.walkers.RodWalker;
import org.broadinstitute.sting.gatk.walkers.Window;
import org.broadinstitute.sting.utils.SampleUtils;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.variantcontext.*;

import java.util.*;

@Reference(window=@Window(start=0,stop=100))
public class CheckReference extends RodWalker<Integer, Integer> {

    @Output(doc="File to which variants should be written",required=true)
    protected VCFWriter baseWriter = null;
    private SortingVCFWriter vcfWriter;

    /**
     * Variants from this input file are used by this tool as input.
     */
    @ArgumentCollection protected StandardVariantContextInputArgumentCollection variantCollection = new StandardVariantContextInputArgumentCollection();

    public void initialize() {
        vcfWriter = new SortingVCFWriter(baseWriter, 40, false);

        List<String> rodNames = Arrays.asList(variantCollection.variants.getName());

        Map<String, VCFHeader> vcfRods = VCFUtils.getVCFHeadersFromRods(getToolkit(), rodNames);
        TreeSet<String> vcfSamples = new TreeSet<String>(SampleUtils.getSampleList(vcfRods, VariantContextUtils.GenotypeMergeType.REQUIRE_UNIQUE));

        // Initialize VCF header
        Set<VCFHeaderLine> headerLines = VCFUtils.smartMergeHeaders(vcfRods.values(), logger);
        headerLines.add(new VCFHeaderLine("source", "CheckReference"));

        vcfWriter.writeHeader(new VCFHeader(headerLines, vcfSamples));
    }

    @Override
    public Integer map(RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        if ( tracker == null ) {
            return 0;
        }

        Collection<VariantContext> vcs = tracker.getValues(variantCollection.variants, context.getLocation());

        if ( vcs == null || vcs.size() == 0) {
            return 0;
        }

        for (VariantContext vc : vcs) {
            VariantContextBuilder builder = new VariantContextBuilder(vc);

            // TODO: Handle multiple alternate alleles, i.e., not just microarray case with single alternate allele
            Allele reportedRefAllele = vc.getReference();
            Allele reportedAltAllele = vc.getAlternateAllele(0);
            Allele observedRefAllele = Allele.create(ref.getBase(), true);

            Map<Allele, Allele> alleleMapping;

            if (reportedRefAllele.equals(observedRefAllele, true /* Ignore reference status */)) {
                vcfWriter.add(vc);
                continue;  // Easy case, where nothing changes...
            } else if (reportedAltAllele.equals(observedRefAllele, true /* Ignore reference status */)) {
                Allele newAltAllele = Allele.create(reportedRefAllele.getBases(), false);

                alleleMapping = new HashMap<Allele, Allele>(2);
                alleleMapping.put(reportedRefAllele, newAltAllele);
                alleleMapping.put(reportedAltAllele, observedRefAllele);

                builder.alleles(Arrays.asList(observedRefAllele, newAltAllele));
            } else {
                // Multi-allelic case: make both alleles, alternate alleles...
                Allele newAltAllele = Allele.create(reportedRefAllele.getBases(), false);

                alleleMapping = new HashMap<Allele, Allele>(1);
                alleleMapping.put(reportedRefAllele, newAltAllele);

                builder.alleles(Arrays.asList(observedRefAllele, Allele.create(reportedRefAllele.getBases(), false), reportedAltAllele));
            }

            // Reset genotypes
            GenotypesContext genotypes = GenotypesContext.create(vc.getNSamples());
            for (final Genotype g : vc.getGenotypes() ) {
                List<Allele> oldGenotypeAlleles = g.getAlleles();
                List<Allele> newGenotypeAlleles = new ArrayList<Allele>(g.getAlleles().size());
                for (Allele a : oldGenotypeAlleles) {
                    if (a.isCalled()) {
                        newGenotypeAlleles.add(alleleMapping.containsKey(a) ? alleleMapping.get(a) : a);
                    } else {
                        newGenotypeAlleles.add(Allele.NO_CALL);  // Add no-call allele
                    }
                }
                genotypes.add(new Genotype(
                    g.getSampleName(),
                    newGenotypeAlleles,
                    g.getLog10PError(),
                    g.getFilters(),
                    g.getAttributes(),
                    g.isPhased()
                ));
            }
            builder.genotypes(genotypes);

            vcfWriter.add(builder.make());
        }

        return 1;
    }

    @Override
    public Integer reduceInit() {
        return 0;
    }

    @Override
    public Integer reduce(Integer value, Integer sum) {
        return sum + value;
    }
}
