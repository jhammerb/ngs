/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.varianteval.evaluators;

import org.apache.log4j.Logger;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.varianteval.evaluators.VariantEvaluator;
import org.broadinstitute.sting.gatk.walkers.varianteval.util.*;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.variantcontext.Genotype;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

@Analysis(name = "Genotype Concordance with Filtering Distinction", description = "Determine the genotype concordance")
public class GenotypeConcordanceWithFilter extends VariantEvaluator {
    protected final static Logger logger = Logger.getLogger(GenotypeConcordanceWithFilter.class);

    @Molten(variableFormat = "%s", valueFormat = "%s")
    public final Map<Object, Object> map = new TreeMap<Object, Object>();

    // concordance counts
    public enum TypeWithPass {
        NO_CALL,
        HOM_REF_PASS,
        HET_PASS,
        HOM_VAR_PASS,
        HOM_REF_FILT,
        HET_FILT,
        HOM_VAR_FILT,
        UNAVAILABLE,
        MIXED  // no-call and call in the same genotype
    }


    private final int nGenotypeTypes = TypeWithPass.values().length;
    private final long[][] compareByCalledGenotypeCounts;

    /**
     * Initialize this object
     */
    public GenotypeConcordanceWithFilter() {
        compareByCalledGenotypeCounts = new long[nGenotypeTypes][nGenotypeTypes];
    }

    @Override
    public int getComparisonOrder() {
        return 2;
    }

    @Override
    public void update2(VariantContext eval, VariantContext comp, RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        // sanity check that we at least have either eval or validation data
        if ((eval == null && comp == null) || (comp != null && !comp.hasGenotypes())) {
            return;
        } else {

            if (eval != null) {
                for (final Genotype g : eval.getGenotypes() ) {
                    final String sample = g.getSampleName();

                    final Genotype.Type called      = g.getType();
                    final boolean called_is_passing = !eval.isFiltered();

                    final Genotype.Type compare;
                    final boolean compare_is_passing;

                    if (comp == null || !comp.hasGenotype(sample)) {
                        compare = Genotype.Type.NO_CALL;
                        compare_is_passing = false;
                    } else {
                        compare = comp.getGenotype(sample).getType();
                        compare_is_passing = !comp.isFiltered();
		            }

                    incrValue(gt2gtp(compare, compare_is_passing), gt2gtp(called, called_is_passing));
                }
            } else {
                final Genotype.Type called = Genotype.Type.NO_CALL;
                final boolean called_is_passing = false;

                for (final Genotype g : comp.getGenotypes()) {
                    final Genotype.Type compare = g.getType();
                    final boolean compare_is_passing = !comp.isFiltered();

                    incrValue(gt2gtp(compare, compare_is_passing), gt2gtp(called, called_is_passing));
                }
            }
        }
    }


    private TypeWithPass gt2gtp(final Genotype.Type gt, final boolean is_passing) {
        switch (gt) {
            case HOM_REF:
                return is_passing ? TypeWithPass.HOM_REF_PASS : TypeWithPass.HOM_REF_FILT;
            case HET:
                return is_passing ? TypeWithPass.HET_PASS     : TypeWithPass.HET_FILT;
            case HOM_VAR:
                return is_passing ? TypeWithPass.HOM_VAR_PASS : TypeWithPass.HOM_VAR_FILT;
            case NO_CALL:
                return TypeWithPass.NO_CALL;
            case UNAVAILABLE:
                return TypeWithPass.UNAVAILABLE;
            case MIXED:
                return TypeWithPass.MIXED;
        }
        return TypeWithPass.NO_CALL;
    }

    private void incrValue(final TypeWithPass compare, final TypeWithPass called) {
        compareByCalledGenotypeCounts[compare.ordinal()][called.ordinal()]++;
    }

    private long count(final TypeWithPass compare, final TypeWithPass called) {
        return compareByCalledGenotypeCounts[compare.ordinal()][called.ordinal()];
    }


    private long count(final EnumSet<TypeWithPass> compare, final TypeWithPass called) {
        return count(compare, EnumSet.of(called));
    }

    private long count(final TypeWithPass compare, final EnumSet<TypeWithPass> called) {
        return count(EnumSet.of(compare), called);
    }

    private long count(EnumSet<TypeWithPass> compare, EnumSet<TypeWithPass> called) {
        long sum = 0;
        for ( TypeWithPass compare1 : compare ) {
            for ( TypeWithPass called1 : called ) {
                sum += count(compare1, called1);
            }
        }
        return sum;
    }

    private long countDiag( final EnumSet<TypeWithPass> d1 ) {
        long sum = 0;
        for(final TypeWithPass e1 : d1 ) {
            sum += compareByCalledGenotypeCounts[e1.ordinal()][e1.ordinal()];
        }
        return sum;
    }

    @Override
    public void finalizeEvaluation() {
        final EnumSet<TypeWithPass> allPassVariantGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_PASS, TypeWithPass.HET_PASS
        );
        final EnumSet<TypeWithPass> allFiltVariantGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_FILT, TypeWithPass.HET_FILT
        );
        final EnumSet<TypeWithPass> allVariantGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_PASS, TypeWithPass.HET_PASS,
            TypeWithPass.HOM_VAR_FILT, TypeWithPass.HET_FILT
        );
        final EnumSet<TypeWithPass> allPassCalledGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_PASS, TypeWithPass.HET_PASS, TypeWithPass.HOM_REF_PASS
        );
        final EnumSet<TypeWithPass> allPassGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_PASS, TypeWithPass.HET_PASS, TypeWithPass.HOM_REF_PASS, TypeWithPass.NO_CALL
        );
        final EnumSet<TypeWithPass> allCalledGenotypes = EnumSet.of(
            TypeWithPass.HOM_VAR_PASS, TypeWithPass.HET_PASS, TypeWithPass.HOM_REF_PASS,
            TypeWithPass.HOM_VAR_FILT, TypeWithPass.HET_FILT, TypeWithPass.HOM_REF_FILT
        );
        final EnumSet<TypeWithPass> allGenotypes = EnumSet.allOf(TypeWithPass.class);

        for ( final TypeWithPass compare : TypeWithPass.values() ) {
            for ( final TypeWithPass called : TypeWithPass.values() ) {
                final String field = String.format("n_compare_%s_called_%s", compare, called);
                final Long value = count(compare, called);
                map.put(field, value.toString());
            }
        }

        // Counts of called genotypes
        for ( final TypeWithPass called : TypeWithPass.values() ) {
            final String field = String.format("total_called_%s", called);
            final Long value = count(allGenotypes, called);
            map.put(field, value.toString());
        }

        // Counts of compare genotypes
        for ( final TypeWithPass compare : TypeWithPass.values() ) {
            final String field = String.format("total_compare_%s", compare);
            final Long value = count(compare, allGenotypes);
            map.put(field, value.toString());
        }

        for ( final TypeWithPass genotype : TypeWithPass.values() ) {
            final String field = String.format("percent_%s_called_%s", genotype, genotype);
            long numer = count(genotype, genotype);
            long denom = count(EnumSet.of(genotype), allGenotypes);
            map.put(field, Utils.formattedPercent(numer, denom));
        }

        {
            final String field = "percent_non_reference_sensitivity";
            long numer = count(allPassVariantGenotypes, allPassVariantGenotypes);
            long denom = count(allPassVariantGenotypes, allGenotypes);
            map.put(field, Utils.formattedPercent(numer, denom));
        }

        {
            final String field = "percent_overall_genotype_concordance";
            long numer = countDiag(allPassCalledGenotypes);
            long denom = count(allPassCalledGenotypes, allPassCalledGenotypes);
            map.put(field, Utils.formattedPercent(numer, denom));
        }

        {
            // Non-reference discrepancy rate
            final String field = "percent_non_reference_discrepancy_rate";
            long homrefConcords = count(TypeWithPass.HOM_REF_PASS, TypeWithPass.HOM_REF_PASS);
            long denom = count(allPassCalledGenotypes, allPassCalledGenotypes) - homrefConcords;
            long numer = denom - (countDiag(allPassVariantGenotypes) - homrefConcords);
            map.put(field, Utils.formattedPercent(numer, denom));
        }
    }
}
