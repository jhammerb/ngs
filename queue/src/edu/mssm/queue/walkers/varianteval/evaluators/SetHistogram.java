/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.varianteval.evaluators;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.common.SystemCache;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.varianteval.VariantEvalWalker;
import org.broadinstitute.sting.gatk.walkers.varianteval.evaluators.VariantEvaluator;
import org.broadinstitute.sting.gatk.walkers.varianteval.util.Analysis;
import org.broadinstitute.sting.gatk.walkers.varianteval.util.Molten;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

/**
 * Build histogram of set INFO fields created by CombineVariants.
 */

@Analysis(name = "Histogram of CombineVariants set field", description = "Histogram of CombineVariants set field")
public class SetHistogram extends VariantEvaluator  {
    protected final static Logger logger = Logger.getLogger(GenotypeConcordanceWithFilter.class);

    @Molten(variableFormat = "%s", valueFormat = "%s")
    public final Map<Object, Object> map = new TreeMap<Object, Object>();

    private int nSamples;
    private Map<String,AtomicLong> setCounts;
    private long setTotal = 0;

    @Override
    public int getComparisonOrder() {
        return 1;  // Only need one variant
    }

    @Override
    public void initialize(VariantEvalWalker walker) {
        super.initialize(walker);

        nSamples = walker.getSampleNamesForEvaluation().size();
        setCounts = new HashMap<String, AtomicLong>();
    }

    @Override
    public void update1(VariantContext eval, RefMetaDataTracker tracker, ReferenceContext ref, AlignmentContext context) {
        if (eval == null) {
            return;
        }

        final String set = eval.getAttributeAsString("set","");

        AtomicLong count = setCounts.get(set);
        if (count == null) {
            setCounts.put(set, new AtomicLong(1));
        } else {
            count.incrementAndGet();
        }
        setTotal++;
    }

    private void translateAndPutTotals(Map<Integer,AtomicLong> totals, int numSamples, long count) {
        AtomicLong value = totals.get(numSamples);
        if (value == null) {
            totals.put(numSamples, new AtomicLong(count));
        } else {
            value.addAndGet(count);
        }
    }

    @Override
    public void finalizeEvaluation() {
        // Map of temporary totals as we translate from variant-variant2-... to 1_of_2
        Map<Integer,AtomicLong> fractionTotals = new HashMap<Integer,AtomicLong>();
        for (int i=0; i <= nSamples; i++) {
            fractionTotals.put(i, new AtomicLong(0));
        }

        Pattern filteredPattern = Pattern.compile("^filterIn");
        for (final Entry<String,AtomicLong> entry : setCounts.entrySet()) {
            // Special cases for variant passing or filtered in all files (as recorded by CombineVariants)
            if (entry.getKey().equals("Intersection") || entry.getKey().equals("FilteredInAll")) {
                translateAndPutTotals(fractionTotals, nSamples, entry.getValue().get());
            } else {
                String[] sets = entry.getKey().split("-");

                int filtered = 0;
                for (String setOrFilter : sets) {
                    if (filteredPattern.matcher(setOrFilter).lookingAt())
                        filtered++;
                }

                translateAndPutTotals(fractionTotals, sets.length-filtered, entry.getValue().get());
            }
        }

        for (final Entry<Integer,AtomicLong> entry : fractionTotals.entrySet()) {
            map.put(
                String.format("total_%d_of_%d", entry.getKey(), nSamples),
                entry.getValue().toString()
            );
            map.put(
                String.format("percent_%d_of_%d", entry.getKey(), nSamples),
                Utils.formattedPercent(entry.getValue().get(), setTotal)
            );
        }
    }
}
