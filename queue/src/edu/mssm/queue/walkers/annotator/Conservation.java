/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.annotator;

import org.broadinstitute.sting.commandline.RodBinding;
import org.broadinstitute.sting.gatk.GenomeAnalysisEngine;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.AnnotatorCompatibleWalker;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.InfoFieldAnnotation;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.RodRequiringAnnotation;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.exceptions.UserException;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

import java.util.*;

public class Conservation extends InfoFieldAnnotation implements RodRequiringAnnotation {
    public static final String GERP_ROD_NAME   = "gerp";
    public static final String PHYLOP_ROD_NAME = "phylop";

    public enum RodKey {
        GERP, PHYLOP;
    }

    public enum InfoFieldKey {
        GERP_NR(RodKey.GERP, "NR", "GERP_NR", VCFHeaderLineType.Float, "GERP++ neutral rate for non-zero sites"),
        GERP_RS(RodKey.GERP, "RS", "GERP_RS", VCFHeaderLineType.Float, "GERP++ RS score for non-zero sites. The larger the score, the more conserved the site."),
        PHYLOP_SCORE(RodKey.PHYLOP, "SCORE", "PHYLOP_SCORE", VCFHeaderLineType.Float, "PhyloP score for placental mammals. The larger the score, the more conserved the site");

        private final RodKey rod;
        private final String vcfName;
        private final String keyName;
        private final VCFHeaderLineType keyType;
        private final String keyDescription;

        private InfoFieldKey(RodKey rod, String vcfName, String keyName, VCFHeaderLineType keyType, String keyDescription) {
            this.rod = rod;
            this.vcfName = vcfName;
            this.keyName = keyName;
            this.keyType = keyType;
            this.keyDescription = keyDescription;
        }

        public RodKey getRod() { return rod; }

        public String getKeyName() { return keyName; }

        public String getVCFName() { return vcfName; }

        public VCFHeaderLineType getKeyType() { return keyType; }

        public String getKeyDescription() { return keyDescription; }
    }


    RodBinding<VariantContext> gerpRodBinding = null;
    RodBinding<VariantContext> phylopRodBinding = null;

    @Override
    public void initialize(AnnotatorCompatibleWalker walker, GenomeAnalysisEngine toolkit, Set<VCFHeaderLine> headerLines) {
        gerpRodBinding   = getRodBinding(walker, GERP_ROD_NAME);
        phylopRodBinding = getRodBinding(walker, PHYLOP_ROD_NAME);
    }

    RodBinding<VariantContext> getRodBinding(AnnotatorCompatibleWalker walker, String name) {
        RodBinding<VariantContext> binding = null;
        for (RodBinding<VariantContext> resource: walker.getResourceRodBindings()) {
            if (resource.getName().equals(name)) {
                binding = resource;
            }
        }
        if (binding == null) {
            throw new UserException(
                "No " + name + " rodbinding found on command line. Specify a resource on the command line"
            );
        }
        return binding;
    }

    @Override
    public Map<String, Object> annotate(RefMetaDataTracker tracker, AnnotatorCompatibleWalker walker, ReferenceContext ref, Map<String, AlignmentContext> stratifiedContexts, VariantContext vc) {
        if (!vc.isSNP()) {  // At present, conservation only relevant for SNVs...
            return null;
        }

        VariantContext gerpRecord   = getMatchingRecord(gerpRodBinding, tracker, ref, vc);
        VariantContext phylopRecord = getMatchingRecord(phylopRodBinding, tracker, ref, vc);

        if (gerpRecord == null && phylopRecord == null) {  // No need to continue if no annotation available
            return null;
        }

        Map<String, Object> annotations = new LinkedHashMap<String, Object>(Utils.optimumHashSize(InfoFieldKey.values().length));
        for (final InfoFieldKey key: InfoFieldKey.values()) {
            Object attribute = null;

            if (key.getRod() == RodKey.GERP && gerpRecord != null)
                attribute = gerpRecord.getAttribute(key.getVCFName());
            else if (key.getRod() == RodKey.PHYLOP && phylopRecord != null)
                attribute = phylopRecord.getAttribute(key.getVCFName());
            else
                continue;

            if (attribute == null || (attribute instanceof String && annotations.toString().trim().length() == 0)) {
                continue;
            }
            annotations.put(key.getKeyName(), attribute);
        }
        return annotations;
    }

    private VariantContext getMatchingRecord(RodBinding<VariantContext> binding, RefMetaDataTracker tracker, ReferenceContext ref, VariantContext vc) {
        if (binding == null) {
            return null;
        }

        // Get only records that start at this locus, not merely span it...
        List<VariantContext> records = tracker.getValues(binding, ref.getLocus());
        for (VariantContext record : records) {
            if (record.getReference().equals(vc.getReference())) {
                return record;
            }
        }
        return null;
    }

    @Override
    public List<VCFInfoHeaderLine> getDescriptions() {
        List<VCFInfoHeaderLine> headers = new ArrayList<VCFInfoHeaderLine>(InfoFieldKey.values().length);
        for (final InfoFieldKey key : InfoFieldKey.values()) {
            headers.add(new VCFInfoHeaderLine(key.getKeyName(), 1, key.getKeyType(), key.getKeyDescription()));
        }
        return headers;
    }

    @Override
    public List<String> getKeyNames() {
        List<String> names = new ArrayList<String>(InfoFieldKey.values().length);
        for (final InfoFieldKey key : InfoFieldKey.values()) {
            names.add(key.getKeyName());
        }
        return names;
    }
}
