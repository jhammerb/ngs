/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.annotator;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.broadinstitute.sting.commandline.RodBinding;
import org.broadinstitute.sting.gatk.GenomeAnalysisEngine;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.annotator.SnpEff;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.AnnotatorCompatibleWalker;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.InfoFieldAnnotation;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.RodRequiringAnnotation;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.exceptions.UserException;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

import java.util.*;

public class CombineSnpEff extends InfoFieldAnnotation implements RodRequiringAnnotation {
    public static final String SNPPEFF_ROD_PREFIX = "snpeff";

    Map<String,RodBinding<VariantContext> > snpEffBindings = null;

    @Override
    public void initialize ( AnnotatorCompatibleWalker walker, GenomeAnalysisEngine toolkit, Set<VCFHeaderLine> headerLines ) {
        snpEffBindings = new HashMap<String,RodBinding<VariantContext> >();

        String snpeffVersion = null;
        String snpeffCommand = null;

        for (RodBinding<VariantContext> resource : walker.getResourceRodBindings()) {
            if (resource.getName().startsWith(SNPPEFF_ROD_PREFIX)) {
                snpEffBindings.put(resource.getName(), resource);

                VCFHeader headers = VCFUtils.getVCFHeadersFromRods(toolkit, Arrays.asList(resource.getName())).get(resource.getName());

                // Ensure all of the header versions match
                VCFHeaderLine version = headers.getOtherHeaderLine(SnpEff.SNPEFF_VCF_HEADER_VERSION_LINE_KEY);
                if (version == null) {
                    throw new UserException("No snpEff version header line found in VCF");
                } else if (snpeffVersion == null) {
                    snpeffVersion = version.getValue();
                } else if (!snpeffVersion.equals(version.getValue())) {
                    throw new UserException("Different snpEff versions found");
                }

                // Combine the various snpEff command lines
                VCFHeaderLine command = headers.getOtherHeaderLine(SnpEff.SNPEFF_VCF_HEADER_COMMAND_LINE_KEY);
                if (command == null) {
                    throw new UserException("No snpEff command header line found");
                } else if (snpeffCommand == null) {
                    snpeffCommand = command.getValue();
                } else {
                    snpeffCommand = String.format("%s;%s", snpeffCommand, command.getValue());
                }
            }
        }

        headerLines.add(new VCFHeaderLine(SnpEff.SNPEFF_VCF_HEADER_VERSION_LINE_KEY, snpeffVersion));
        headerLines.add(new VCFHeaderLine(SnpEff.SNPEFF_VCF_HEADER_COMMAND_LINE_KEY, snpeffCommand));
    }

    @Override
    public Map<String, Object> annotate(RefMetaDataTracker tracker, AnnotatorCompatibleWalker walker, ReferenceContext ref, Map<String, AlignmentContext> stratifiedContexts, VariantContext vc) {
        List<String> field = new ArrayList<String>();

        for (RodBinding<VariantContext> binding: snpEffBindings.values()) {
            // Get only SnpEff records that start at this locus, not merely span it...
            List<VariantContext> snpEffRecords = tracker.getValues(binding, ref.getLocus());

            // and that have matching alleles...
            VariantContext matchingRecord = getMatchingSnpEffRecord(snpEffRecords, vc);
            if ( matchingRecord == null ) {
                continue;
            }

            Object effectFieldValue = matchingRecord.getAttribute(SnpEff.SNPEFF_INFO_FIELD_KEY);
            if (effectFieldValue == null) {
                continue;
            } else if (effectFieldValue instanceof List) {
                field.addAll((List<String>)effectFieldValue);
            } else {
                field.add((String)effectFieldValue);
            }
        }

        if (field.size() == 0) {
            return null;
        } else {
            HashMap<String, Object> combinedField = new HashMap<String, Object>(1);

            combinedField.put(SnpEff.SNPEFF_INFO_FIELD_KEY, StringUtils.join(field, ','));

            return combinedField;
        }
    }

    @Override
    public List<VCFInfoHeaderLine> getDescriptions() {
        /*
         * Note this assumes you are using "-o gatk" with snpEff, which omits the Amino_acid_length field. Thus
         * this header can disagree with header line written out by snpEff itself.
         */

        return Arrays.asList(
            new VCFInfoHeaderLine(
                SnpEff.SNPEFF_INFO_FIELD_KEY,
                VCFHeaderLineCount.UNBOUNDED,
                VCFHeaderLineType.String,
                "Predicted effects for this variant.Format: 'Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_change| Gene_Name | Gene_BioType | Coding | Transcript | Exon [ | ERRORS | WARNINGS ] )' "            )
        );
    }

    @Override
    public List<String> getKeyNames() {
        return Arrays.asList( SnpEff.SNPEFF_INFO_FIELD_KEY );
    }

    private VariantContext getMatchingSnpEffRecord(List<VariantContext> snpEffRecords, VariantContext vc) {
        for ( VariantContext snpEffRecord : snpEffRecords ) {
            if ( snpEffRecord.hasSameAlternateAllelesAs(vc) && snpEffRecord.getReference().equals(vc.getReference()) ) {
                return snpEffRecord;
            }
        }

        return null;
    }
}
