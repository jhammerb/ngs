/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.walkers.annotator;

import org.broadinstitute.sting.commandline.RodBinding;
import org.broadinstitute.sting.gatk.GenomeAnalysisEngine;
import org.broadinstitute.sting.gatk.contexts.AlignmentContext;
import org.broadinstitute.sting.gatk.contexts.ReferenceContext;
import org.broadinstitute.sting.gatk.refdata.RefMetaDataTracker;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.AnnotatorCompatibleWalker;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.InfoFieldAnnotation;
import org.broadinstitute.sting.gatk.walkers.annotator.interfaces.RodRequiringAnnotation;
import org.broadinstitute.sting.utils.Utils;
import org.broadinstitute.sting.utils.codecs.vcf.*;
import org.broadinstitute.sting.utils.exceptions.UserException;
import org.broadinstitute.sting.utils.variantcontext.VariantContext;

import java.util.*;

/**
 * A set of genomic annotations from the dbNSP database (https://sites.google.com/site/jpopgen/dbNSFP)
 *
 * For each variant choose the matching variant from the dbNSP database VCF and adds
 * SIFT, LRT, MutationTaster, GERP, PhyloP and Polyphen2 annotations.
 *
 * Requires a dbnsfp resource to be specified on VariantAnnotater command line.
 */


public class DBNSFP extends InfoFieldAnnotation implements RodRequiringAnnotation {
    public static final String DBNSFP_ROD_NAME = "dbnsfp";
    public static final String DBNSFP_ROD_SOURCE_KEY = "source";

    public enum InfoFieldKey {
        SIFT_SCORE("SIFT_SCORE", "SIFT_SCORE", VCFHeaderLineType.Float, "SIFT Score. From dbNSFP."),
        SIFT_PRED("SIFT_PRED", "SIFT_PRED", VCFHeaderLineType.Character, "SIFT Prediction: D (damaging), T (tolerated). From dbNSFP."),
        LRT_SCORE("LRT_SCORE", "LRT_SCORE", VCFHeaderLineType.Float, "LRT Score. From dbNSFP."),
        LRT_PRED("LRT_PRED", "LRT_PRED", VCFHeaderLineType.Character, "LRT Prediction: D (deleterious), N (neutral), U (unknown). From dbNSFP."),
        MUTATIONTASTER_SCORE("MUTST_SCORE","MUTST_SCORE", VCFHeaderLineType.Float, "MutationTaster Score. From dbNSFP."),
        MUTATIONTASTER_PRED("MUTST_PRED","MUTST_PRED", VCFHeaderLineType.Character, "MutationTaster Prediction: A (disease_causing_automatic), D (disease_causing), N (polymorphism), P (polymorphism_automatic). From dbNSFP.") ,
        PHEN2_HDIV_SCORE("PHEN2_HDIV_SCORE","PHEN2_HDIV_SCORE", VCFHeaderLineType.Float, "Polyphen2 HumDiv Score. From dbNSFP."),
        PHEN2_HDIV_PRED("PHEN2_HDIV_PRED","PHEN2_HDIV_PRED", VCFHeaderLineType.Character, "Polyphen2 HumDiv Prediction: D (damaging), P (possibly damaging), B (benign). From dbNSFP."),
        PHEN2_HVAR_SCORE("PHEN2_HVAR_SCORE","PHEN2_HVAR_SCORE", VCFHeaderLineType.Float, "Polyphen2 HumVar Score. From dbNSFP."),
        PHEN2_HVAR_PRED("PHEN2_HVAR_PRED","PHEN2_HVAR_PRED", VCFHeaderLineType.Character, "Polyphen2 HumVar Prediction: D (damaging), P (possibly damaging), B (benign). From dbNSFP.");

        private final String dbnsfpName;
        private final String keyName;
        private final VCFHeaderLineType keyType;
        private final String keyDescription;

        private InfoFieldKey(String dbnsfpName, String keyName, VCFHeaderLineType keyType, String keyDescription) {
            this.dbnsfpName = dbnsfpName;
            this.keyName = keyName;
            this.keyType = keyType;
            this.keyDescription = keyDescription;
        }

        public String getKeyName() {
            return keyName;
        }

        public String getDBNSFPName() {
            return dbnsfpName;
        }

        public VCFHeaderLineType getKeyType() {
            return keyType;
        }

        public String getKeyDescription() {
            return keyDescription;
        }
    }

    RodBinding<VariantContext> dbnsfpRodBinding = null;

    @Override
    public void initialize ( AnnotatorCompatibleWalker walker, GenomeAnalysisEngine toolkit, Set<VCFHeaderLine> headerLines ) {
        for (RodBinding<VariantContext> resource: walker.getResourceRodBindings()) {
            if (resource.getName().equals(DBNSFP_ROD_NAME)) {
                dbnsfpRodBinding = resource;
            }
        }
        if (dbnsfpRodBinding == null) {
            throw new UserException(
                    "No " + DBNSFP_ROD_NAME + " rodbinding found on command line. Specify a " +
                    "resource on the command line"
            );
        }
        VCFHeader dbnsfpVCFHeader = VCFUtils.getVCFHeadersFromRods(toolkit, Arrays.asList(DBNSFP_ROD_NAME)).get(DBNSFP_ROD_NAME);
        VCFHeaderLine dbnsfpSourceLine = dbnsfpVCFHeader.getOtherHeaderLine(DBNSFP_ROD_SOURCE_KEY);
        if (dbnsfpSourceLine == null || dbnsfpSourceLine.getValue() == null || dbnsfpSourceLine.getValue().trim().length() == 0) {
            throw new UserException("No source information found in dbNSFP resource binding. Unable to track version in downstream VCF files.");
        }
        headerLines.add(new VCFHeaderLine("OriginaldbNSFP"+DBNSFP_ROD_SOURCE_KEY,dbnsfpSourceLine.getValue()));
    }

    @Override
    public Map<String, Object> annotate(RefMetaDataTracker tracker, AnnotatorCompatibleWalker walker,
                                        ReferenceContext ref, Map<String, AlignmentContext> stratifiedContexts,
                                        VariantContext vc) {

        // Get only records that start at this locus, not merely span it...
        List<VariantContext> dbnsfpRecords = tracker.getValues(dbnsfpRodBinding, ref.getLocus());
        VariantContext matchingRecord = getMatchingRecord(dbnsfpRecords, vc);
        if ( matchingRecord == null ) {
            return null;
        }

        Map<String, Object> annotations = new LinkedHashMap<String, Object>(Utils.optimumHashSize(InfoFieldKey.values().length));
        for (final InfoFieldKey key: InfoFieldKey.values()) {
            Object attribute = matchingRecord.getAttribute(key.getDBNSFPName());
            if (attribute == null || (attribute instanceof String && annotations.toString().trim().length() == 0)) {
                continue;
            }
            annotations.put(key.getKeyName(), attribute);
        }
        return annotations;
    }

    private VariantContext getMatchingRecord ( List<VariantContext> records, VariantContext vc ) {
        for ( VariantContext record : records ) {
            if ( record.hasSameAlternateAllelesAs(vc) && record.getReference().equals(vc.getReference()) ) {
                return record;
            }
        }
        return null;
    }

    @Override
    public List<VCFInfoHeaderLine> getDescriptions() {
        List<VCFInfoHeaderLine> headers = new ArrayList<VCFInfoHeaderLine>(InfoFieldKey.values().length);
        for (final InfoFieldKey key : InfoFieldKey.values()) {
            headers.add(new VCFInfoHeaderLine(key.getKeyName(), 1, key.getKeyType(), key.getKeyDescription()));
        }
        return headers;
    }

    @Override
    public List<String> getKeyNames() {
        List<String> names = new ArrayList<String>(InfoFieldKey.values().length);
        for (final InfoFieldKey key : InfoFieldKey.values()) {
            names.add(key.getKeyName());
        }
        return names;
    }
}
