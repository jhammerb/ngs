/*
 * Copyright (c) 2012 Mt. Sinai School of Medicine. All rights reserved.
 *
 * License available in LICENSE.txt.
 */

package edu.mssm.queue.util


import _root_.java.util.ResourceBundle
import java.io.File
import io.Source._
import xml.XML
import org.broadinstitute.sting.queue.QException
import org.broadinstitute.sting.commandline.Input._
import org.apache.commons.io.FilenameUtils
import org.broadinstitute.sting.gatk.datasources.reads.utilities.BAMFileStat
import org.broadinstitute.sting.utils.text.TextFormattingUtils

class ReadGroup(params:Map[String,String]) {
  val id = params("id")
  val sample   = params("sample")
  val library  = params("library")
  val platform = params("platform")
  val platform_unit = params.getOrElse("platform_unit","")
  val center        = params.getOrElse("center","")
  val description   = params.getOrElse("description","")

  override def toString = {
    var rg = "@RG\\tID:"+ id +
      "\\tSM:" + sample +
      "\\tLB:" + library +
      "\\tPL:" + platform
    if (platform_unit != "")
      rg += "\\tPU:" + platform_unit
    if (center != "")
      rg += "\\tCN:" + center
    if (description != "")
      rg += "\\tDS:" + description
    rg
  }

}

abstract class AlignableInput(readGroupParams: Map[String,String]) {
  val readGroup = new ReadGroup(readGroupParams)

  def isPaired = false
}

case class SEFastQFile(fastq1: File, readGroupParams: Map[String,String])
  extends AlignableInput(readGroupParams) {

}

case class PEFastQFiles(fastq1: File, fastq2: File, readGroupParams: Map[String,String])
  extends AlignableInput(readGroupParams) {

  override def isPaired = true
}

case class PBFastQFile(fastq1: File, readGroupParams: Map[String,String])
  extends AlignableInput(readGroupParams) {
}

case class BAMFile(bam: File, readGroupParams: Map[String,String]) extends AlignableInput(readGroupParams) {
  // TODO: Extract read groups from BAM at initialization
}

object QueueUtils {

  def createInputSeqFromFile(in: File):Seq[AlignableInput] = {
    var inputs: Seq[AlignableInput] = Seq()

    val cohort = xml.Utility.trim(XML.loadFile(in))

    val cohort_attributes = cohort.attributes.asAttrMap

    for (input <- cohort.child) {
      val input_attributes = cohort_attributes ++ input.attributes.asAttrMap

      // Construct proper input object
      var alignable = input match {
        case <fastq><fastq1/></fastq> =>
          SEFastQFile(new File((input \\ "fastq1" \ "@file").text), input_attributes)
        case <fastq><fastq1/><fastq2/></fastq> =>
          PEFastQFiles(
            new File((input \\ "fastq1" \ "@file").text),
            new File((input \\ "fastq2" \ "@file").text),
            input_attributes
          )
        case <fastq><fastqpb/></fastq> =>
          PBFastQFile(new File((input \\ "fastqpb" \ "@file").text), input_attributes)
        case <bam/> =>
          BAMFile(
            new File((input \ "bam" \ "@file").text),
            input_attributes
          )
        case _ => throw new QException("Unknown element in input specification:" + input.label)
      }

      inputs :+= alignable
    }

    inputs
  }

  def createFileSeqFromFile(in: File):Seq[File] = {
    val name = in.toString.toUpperCase
    if (
      name.endsWith(".BAM") ||
      name.endsWith(".FASTA") ||
      name.endsWith(".FQ") ||
      name.endsWith(".FASTQ") ||
      name.endsWith(".VCF")
    )
      return Seq(in)

    var list: Seq[File] = Seq()
    for (file <- fromFile(in).getLines()) {
      if (!file.startsWith("#") && !file.isEmpty) {
        list :+= new File(file.trim())
      }
    }
    list.sortWith(_.compareTo(_) < 0)
  }


  def getReadFileExtension(a: File):String = {
    val gzRE = """.*(\.[^.]+\.gz)$""".r
    a.getName() match {
      case gzRE(ext) => { ext }
      case name:String => "." + FilenameUtils.getExtension(name)
    }
  }

  def getVersionNumber: String = {
    val headerInfo: ResourceBundle = TextFormattingUtils.loadResourceBundle("MSSMQueueText")
    return if (headerInfo.containsKey("edu.mssm.queue.version")) headerInfo.getString("edu.mssm.queue.version") else "<unknown>"
  }

  def getBuildTime: String = {
    val headerInfo: ResourceBundle = TextFormattingUtils.loadResourceBundle("MSSMQueueText")
    return if (headerInfo.containsKey("build.timestamp")) headerInfo.getString("build.timestamp") else "<unknown>"
  }
}

