#!/bin/bash

# Build VCF database of non-zero GERP scores suitable for use in variant annotation

REFERENCE=hg19
DICT_FILE=$GATKRSRC_ROOT/ucsc.hg19.dict

usage()
{
	cat << EOF
usage: `basename $0` [options] PREFIX
Options:
  -R [REFERENCE] Reference; default: $REFERENCE
  -d [FILE] Dictionary file; default: $DICT_FILE

Builds VCF from GERP genome-wide download (http://mendel.stanford.edu/SidowLab/downloads/gerp/)
EOF
}

while getopts "hR:d:" Option
do
	case $Option in
		R)
			REFERENCE=$OPTARG
			;;
		d)
			DICT_FILE=$OPTARG
			;;
		h)
			usage
			exit 0
			;;
		?)
			usage
			exit 85
			;;
	esac
done

shift $((OPTIND-1))

if [ $# == "0" ]
then
  usage
	exit 85
fi

PREFIX=$1

INTERMEDIATE_FILE="${PREFIX}.tsv"

cat << EOF > $INTERMEDIATE_FILE
#INFO=<ID=RS,Number=1,Type=Float,Description="GERP++ RS score for non-zero sites. The larger the score, the more conserved the site.">
#INFO=<ID=NR,Number=1,Type=Float,Description="GERP++ neutral rate for non-zero sites.">
HEADER	NR	RS
EOF

CONTIGS=($(cut -f2 $DICT_FILE | sed 's/SN://g'))
for c in ${CONTIGS[@]}
do
	file="${c}.maf.rates"
	
	if [ ! -f $file ]
	then
		continue;
	fi
	
	awk -v chrom="${c}" '{
		if ($1 != 0 || $2 != 0) {
			print chrom ":" NR,$1,$2;
		}
	}' $file >> $INTERMEDIATE_FILE
done

gatk -R $REFERENCE -X "-XX:ParallelGCThreads=2" -m 12288m TableToVCF -V:TABLE $INTERMEDIATE_FILE \
	-H "<ID=RS,Number=1,Type=Float,Description=\"GERP++ RS score for non-zero sites. The larger the score, the more conserved the site.\">" \
	-H "<ID=RS,Number=1,Type=Float,Description=\"GERP++ neutral rate for non-zero sites\">" \
	-o "${PREFIX}.vcf"
