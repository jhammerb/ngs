#!/bin/bash

# Build VCF database of phyloP scores suitable for use in variant annotation

REFERENCE=hg19
DICT_FILE=$GATKRSRC_ROOT/ucsc.hg19.dict

usage()
{
	cat << EOF
usage: `basename $0` [options] INPUT_SUFFIX OUTPUT_PREFIX
Options:
  -R [REFERENCE] Reference; default: $REFERENCE
	-d [FILE] Dictionary file; default: $DICT_FILE

Builds VCF from PhyloP genome-wide download (http://hgdownload.cse.ucsc.edu/goldenPath/hg19/phyloP46way/placentalMammals/)
EOF
}

while getopts "hR:d:" Option
do
	case $Option in
		R)
			REFERENCE=$OPTARG
			;;
		d)
			DICT_FILE=$OPTARG
			;;
		h)
			usage
			exit 0
			;;
		?)
			usage
			exit 85
			;;
	esac
done

shift $((OPTIND-1))

if [ $# == "0" ]
then
  usage
	exit 85
fi

INPUT_SUFFIX=$1
OUTPUT_PREFIX=$2

INTERMEDIATE_FILE="${OUTPUT_PREFIX}.tsv"

CONTIGS=($(cut -f2 $DICT_FILE | sed 's/SN://g'))

cat << EOF > $INTERMEDIATE_FILE
#INFO=<ID=SCORE,Number=1,Type=Float,Description="PhyloP score for placental mammals. The larger the score, the more conserved the site.">
HEADER	SCORE
EOF

for c in ${CONTIGS[@]} 
do
	file=${c}.${INPUT_SUFFIX}
	
	if [ ! -f $file ]
	then
		continue;
	fi

	gzip -dc $file | awk -v chrom="${c}" '
		BEGIN {
			start=1;
		}
		{
			if ($0 ~ /^fixed/) {
				split($3, new_start, "=");
				start = new_start[2];
			} else {
				print chrom ":" start, $1;
				start++;
			}
		}
	' OFS=$'\t' >> $INTERMEDIATE_FILE
done

gatk -R $REFERENCE -X "-XX:ParallelGCThreads=2" -m 12288m TableToVCF -V:TABLE $INTERMEDIATE_FILE \
	-H "<ID=SCORE,Number=1,Type=Float,Description=\"PhyloP score for placental mammals. The larger the score, the more conserved the site.\">" \
	-o "${OUTPUT_PREFIX}.vcf"
