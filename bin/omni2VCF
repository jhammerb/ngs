#!/bin/bash

# Create VCF from Genome Studio Report

UPDATE_FILE=$OMNIRSRC_ROOT/update.pos

usage()
{
	cat << EOF
usage: `basename $0` [options] GENOME_STUDIO_REPORT SAMPLE_TO_EXTRACT
Options:
  -u [PATH] Path to position update file, default=${UPDATE_FILE}.

Builds VCF from Genome Studio Report. Flips strand and updates position if update file is available.

All indels are filtered out, i.e. entries with D/I.

Note that the resulting VCF may not have the correct reference alleles, and needs to have check
reference applied. For example:

gatk -R hg19 CheckReference -V NA12878.mixedref.vcf -o ../NA12878.Omni25.vcf
EOF
}

if [ $# -eq "0" ]
then
  usage
	exit 85
fi

while getopts "hu:" Option
do
	case $Option in
		u)
			UPDATE_FILE=$OPTARG
			;;
		h)
			usage
			exit 0
			;;
		?)
			usage
			exit 85
			;;
	esac
done

shift $((OPTIND-1))

SAMPLE=$2
SAMPLE_NOSPACE=$(echo "$SAMPLE" | sed 's/[[:space:]]//g')

grep -E "Chr|$SAMPLE" $1 | \
	gcol 'Chr' 'Position' 'SNP Name' 'SNP' 'Plus/Minus Strand' 'Allele1 - Design' 'Allele2 - Design' 'GT Score' 'GC Score' | \
	awk -v update="${UPDATE_FILE}" '
		
		function flip(allele) {
			if (allele == "A") return "T";
			if (allele == "C") return "G";
			if (allele == "G") return "C";
			if (allele == "T") return "A";
			return ".";
		}

		# Extract ref/alt alleles from reference in seqdb and snp in [A/A] format
		function snp2alleles(SNP, need_to_flip, snp_alleles) {
			split(SNP, snp_alleles, /[[/\]]/);
			ref_allele = need_to_flip ? flip(snp_alleles[2]) : snp_alleles[2];
			alt_allele = need_to_flip ? flip(snp_alleles[3]) : snp_alleles[3];
		}

		function alleles2GT(ref_allele, alt_allele, allele1, allele2, need_to_flip) {
			if (need_to_flip) {
				allele1 = flip(allele1);
				allele2 = flip(allele2);
			}
			return ((allele1 == ref_allele) ? "0" : "1") "/" ((allele2 == ref_allele) ? "0" : "1");
		}

		BEGIN {
			if (update != "") {
				while ((getline < update) > 0) {
					id2pos[$1] = $2 ":" $3
				}
				close(update)
			} else {
				id2pos[""] = ""
			}
		}

		NR > 1 {		
			# Try to update the position
			if ($1 == 0) {
				new_position = id2pos[$3];
				if (split(new_position, new_coordinates, ":") == 2) {
					$1 = new_coordinates[1];
					$2 = new_coordinates[2];
				} else {
					next;
				}
			}
		
			# ID cannot have special characters
			gsub(/[^[:alnum:]]/,"",$3)

			# Tweak chromosome name to enable sort order...
			if ($1 == "MT") {
				$1 = 0;
			} else if ($1 == "XY" || $1 == "X") {
				$1 = 23;
			} else if ($1 == "Y") {
				$1 = 24;
			}
	
			snp2alleles($4, $5 == "-");
			
			# Skip indels
			if (ref_allele == "D" || ref_allele == "I" || alt_allele == "D" || alt_allele == "I") {
				next;
			}

			print $1, $2, $3, ref_allele, alt_allele, 0, "PASS", "GentrainScore=" $8, "GT:GC", alleles2GT(ref_allele, alt_allele, $6, $7, $5 == "-") ":" $9;
		}

		' OFS=$'\t' | sort -t$'\t' -k 1,1n -k 2,2n | awk -v sample="$SAMPLE_NOSPACE" '
		BEGIN {
			print "##fileformat=VCFv4.1";
			print "##source=omni2array";
			print "##INFO=<ID=GentrainScore,Number=-1,Type=Float,Description=\"Gentrain Score\">";
			print "##FORMAT=<ID=GC,Number=-1,Type=Float,Description=\"Gencall Score\">";
			print "##FILTER=<ID=PASS,Description=\"Passed variant FILTERs\">";
			print "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t" sample;
		}
		{
			# Undo sorting tweaks to chromosome name
			if ($1 == 0) {
				$1 = "M"
			} else if ($1 == 23) {
				$1 = "X"
			} else if ($1 == 24) {
				$1 = "Y"
			}
			
			print "chr" $1, $2, $3, $4, $5, $6, $7, $8, $9, $10;
		}
	' OFS=$'\t' 
