#!/usr/bin/env roundup

vrwr() { 
	gatk -R hg19 VariantEval -comp "$NGS_ROOT/test/inputs/$1" -eval "$NGS_ROOT/test/inputs/$2" -strict -noEV -EV ValidationReportWithFilter -noST -ST Filter -o $3 ;
}

gcwf() {
	gatk -R hg19 VariantEval -comp "$NGS_ROOT/test/inputs/$1"  -eval "$NGS_ROOT/test/inputs/$2" -strict -noEV -EV GenotypeConcordanceWithFilter -noST -ST Filter -o $3 ;
}

hist() {
	gatk -R hg19 VariantEval -eval "$NGS_ROOT/test/inputs/$1" -strict -noEV -EV SetHistogram -noST -ST Filter -ST VariantType -o $2 ;
} 

describe "validation VariantEval tests"

before() {
	output=$(mktemp --suffix=.vcf)
}

after() {
	rm -f $output
}

# Columns ids: 4 => Filter; 5 => nComp; 6-9 => TP,FP,FN,TN; 18 => CompFiltered

it_validates_perfectly_against_self() {
	vrwr vrwr_eval.vcf vrwr_eval.vcf $output
	result=$(awk '$4 == "raw" { print $5 == ($6 + $9 + $18); }' $output | head -n 1)
	test $result -eq 1
}

it_has_expected_validation() {
	vrwr vrwr_comp.vcf vrwr_eval.vcf $output
	result=$(awk '$4 == "raw" { print ($6 == 1) && ($7 == 2) && ($8 == 3) && ($8 == 3) && ($18 == 2); }' $output | head -n 1)
	test $result -eq 1	
}

# Column ids: 4 => Filter, 5 => variable, 6 => value 

it_has_expected_concordance() {
	gcwf vrwr_comp.vcf vrwr_eval.vcf $output
	result=$(awk '
		$4 == "raw" && $5 == "percent_overall_genotype_concordance" { gc = $6; } 
		$4 == "raw" && $5 == "percent_non_reference_sensitivity" { rs = $6; } 
		END { print gc == 50.00 && rs == 25.00; }
	' $output)
	test $result -eq 1	
}

# Column ids: 4 => Filter, 5 => VariantType, 6 => variable, 7 => value 

it_has_expected_histogram_counts() {
	hist histogram.vcf $output
	result=$(awk '
		$4 == "called" && $5 == "SNP" && $6 == "total_2_of_2" { v2of2 = $7; }
		$4 == "called" && $5 == "SNP" && $6 == "total_1_of_2" { v1of2 = $7; } 
		$4 == "called" && $5 == "SNP" && $6 == "total_0_of_2" { v0of2 = $7; } 
		END { print v2of2 == 3 && v1of2 == 2 && v0of2 == 0; }
	' $output)
	test $result -eq 1
}
