#!/usr/bin/env roundup

before() {
	working=$(mktemp -d)
	cp "$NGS_ROOT/test/inputs/SampleSheet.csv" "$working/SampleSheet.csv"
}

after() {
	rm -rd $working
}

describe "seqSetup tests"

it_rejects_invalid_project_name() {
	status=$(set +e ; cd $working ; seqSetup a.b > /dev/null ; echo $?)
	test $status -eq 1
}

it_accepts_valid_project_name() {
	status=$(set +e ; cd $working ; seqSetup abc_DEF_ > /dev/null ; echo $?)
	test $status -eq 0
}
