###################################################################################
# Makefile for running various concordance metrics as part of validation. Can be
# used to generate reports, etc. for different samples.
#
# This makefile is intended to be included in a sample specific makefile that sets
# the following required variables:
#
#   SAMPLE: Sample name, e.g., NA12878
#   RUNS: Different runs, e.g., r1-1-1, r1-1-3, etc. These are the sub-directory names.
#
# In addition some subset of the available targets can be selected based on what data
# is available for that sample, e.g., some samples do not 1kG array available
###################################################################################
SHELL := /bin/bash -e -o pipefail

REFVCF_PATH ?=/projects/ngs/validation/reference

.SECONDEXPANSION:

# Note this must be exactly two lines
define newline


endef

# Microarray Concordance
##

%v1KGOmni25.concordance.gatkreport : $(REFVCF_PATH)/$(SAMPLE).1KGOmni25.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$* $*/$*.combined.vcf -comp:1KGOmni25 $< \
		-strict -noEV -EV GenotypeConcordanceWithFilter -noST -ST Filter -o $@

%v1KGOmni25.validation.gatkreport : $(REFVCF_PATH)/$(SAMPLE).1KGOmni25.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$* $*/$*.combined.vcf -comp:1KGOmni25 $< \
		-noEV -EV ValidationReportWithFilter -noST -ST Filter -o $@


%vOmni25.concordance.gatkreport : $(REFVCF_PATH)/$(SAMPLE).Omni25.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$* $*/$*.combined.vcf -comp:Omni25 $< \
		-strict -noEV -EV GenotypeConcordanceWithFilter -noST -ST Filter -o $@

%vOmni25.validation.gatkreport : $(REFVCF_PATH)/$(SAMPLE).Omni25.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$* $*/$*.combined.vcf -comp:Omni25 $< \
		-noEV -EV ValidationReportWithFilter -noST -ST Filter -o $@

# Uses annotateR to read in and reformat GATK reports to get concordance statistics in a nice table format 

# We specifically use "raw" filtering for the concordance reports to make sure that
# we capture all variants. Note that the percent_* statistics specifically treat filtered variants
# as no-calls (although it reports the counts for filtered variants separately).

define CONCORDANCE_RSCRIPT
suppressPackageStartupMessages(library(annotateR, quietly=F));
args <- commandArgs(trailingOnly=T);
concordance <- ldply(args, function(arg) {
 report <- annotateR:::.listOfGATKReports(arg);
 subset(
  dcast(report$$GenotypeConcordanceWithFilter, EvalRod + CompRod + Filter ~ variable, value.var="value"), 
  Filter == "raw", select=c("EvalRod","CompRod","percent_overall_genotype_concordance","percent_non_reference_sensitivity","percent_non_reference_discrepancy_rate")
 );
});
write.table(concordance, stdout(), quote=F, sep="\t", row.names=F);
endef

CONCORDANCE_REXPR =$(subst $(newline), ,$(CONCORDANCE_RSCRIPT))

# Note that those samples with 1KG arrays need to add those dependencies, and those smaples without
# e.g., NA10080, will need to override this rule
$(SAMPLE).array.concordance.tsv : $(foreach samp, $(RUNS),$(samp)vOmni25.concordance.gatkreport) 
	echo "#sample	in-house	broad" > $@
	join -j 1 \
		<(Rscript -e '$(CONCORDANCE_REXPR)' $(wildcard *vOmni25.concordance.gatkreport) | cut -f1,3-) \
		<(Rscript -e '$(CONCORDANCE_REXPR)' $(wildcard *v1KGOmni25.concordance.gatkreport) | cut -f1,3-) \
		>> $@


# Concordance with Sanger data for targeted ASD panel
##

%vASDSanger.concordance.gatkreport : $(REFVCF_PATH)/$(SAMPLE).autism.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -L $(REFVCF_PATH)/autism.intervals -isr INTERSECTION \
		-eval:$* $*/$*.combined.vcf -comp:ASDSanger $< -strict -noEV -EV GenotypeConcordanceWithFilter -noST -ST Filter -o $@

%vASDSanger.validation.gatkreport : $(REFVCF_PATH)/$(SAMPLE).autism.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -L $(REFVCF_PATH)/autism.intervals -isr INTERSECTION \
		-eval:$* $*/$*.combined.vcf -comp:ASDSanger $< -noEV -EV ValidationReportWithFilter -noST -ST Filter -o $@

%vASDSanger.vcf : $(REFVCF_PATH)/$(SAMPLE).autism.vcf $$*/$$*.combined.vcf
	gatk -R hg19 CombineVariants -V:$* $*/$*.combined.anno.vcf -V:ASDSanger $< $(if $(INTERVAL),-L $(INTERVAL)) -L $(REFVCF_PATH)/autism.intervals -isr INTERSECTION -o $@ 

# We focus on TP, FP, etc. in the output report since there are no unfiltered
# variants in the ASD validation sets.

define VALIDATION_RSCRIPT
suppressPackageStartupMessages(library(annotateR, quietly=F));
args <- commandArgs(trailingOnly=T);
validation <- ldply(args, function(arg) {
 report <- annotateR:::.listOfGATKReports(arg);
 subset(report$$ValidationReportWithFilter, Filter == "raw", select=c("EvalRod","nComp","TP","FP","FN","TN", "nDifferentAlleleSites"));
});
write.table(validation, stdout(), quote=F, sep="\t", row.names=F);
endef

VALIDATION_REXPR  =$(subst $(newline), ,$(VALIDATION_RSCRIPT))


# Analysis of combined runs of same sample. Relevant for NA12878 and NA18507.
##

$(SAMPLE).combined.vcf : $(foreach samp, $(filter-out $(EXTERNAL),$(RUNS)),$(samp)/$(samp).combined.vcf)
	gatk -R hg19 CombineVariants $(foreach input, $^,-V $(input)) $(if $(INTERVAL),-L $(INTERVAL)) -genotypeMergeOptions UNIQUIFY -o $@

$(SAMPLE).combined.histogram.gatkreport : $(SAMPLE).combined.vcf
	gatk -R hg19 VariantEval -eval:$(SAMPLE) $< -strict -noEV -EV SetHistogram -noST -ST Filter -ST VariantType -o $@

define COMPARE_template
$(1)v$(2).concordance.gatkreport : $(1)/$(1).combined.vcf $(2)/$(2).combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$(1) $(1)/$(1).combined.vcf -comp:$(2) $(2)/$(2).combined.vcf \
		-strict -noEV -EV GenotypeConcordanceWithFilter -noST -ST Filter -o $$@
endef

# Comparisons to MacArthur et al. LoF paper. This is only relevant for NA12878.
##

%.likelylof.vcf : $$*/$$*.pseq
	pseq $< write-vcf --noweb --silent --mask $(shell Rscript -e 'cat(suppressWarnings(annotateR:::mask.AAEffect(filter=F, geno.req="DP:ge:20,GQ:ge:30")))') > $@.tmp
	sortVCF $@.tmp $(GATKRSRC_ROOT)/ucsc.hg19.fasta > $@
	rm $@.tmp

%vScienceLoF.validation.gatkreport : $(REFVCF_PATH)/$(SAMPLE).lof.hg19.vcf $(REFVCF_PATH)/$(SAMPLE).hiconflof.hg19.vcf $(REFVCF_PATH)/$(SAMPLE).loconflof.hg19.vcf $$*.likelylof.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) -eval:$*-lof $*.likelylof.vcf -eval:$* $*/$*.combined.vcf \
		-comp:HCLoF $(REFVCF_PATH)/$(SAMPLE).hiconflof.hg19.vcf \
		-comp:LCLoF $(REFVCF_PATH)/$(SAMPLE).loconflof.hg19.vcf \
		-comp:AllLoF $(REFVCF_PATH)/$(SAMPLE).lof.hg19.vcf \
		-noEV -EV ValidationReportWithFilter -noST -ST Filter -ST EvalRod -ST CompRod -o $@ 

%vScienceLoF.count.gatkreport : $(REFVCF_PATH)/$(SAMPLE).lof.hg19.vcf $(REFVCF_PATH)/$(SAMPLE).hiconflof.hg19.vcf $(REFVCF_PATH)/$(SAMPLE).loconflof.hg19.vcf $$*.likelylof.vcf $$*/$$*.combined.vcf
	gatk -R hg19 VariantEval $(if $(INTERVAL),-L $(INTERVAL)) \
		-eval:$*-lof $*.likelylof.vcf \
		-eval:$* $*/$*.combined.vcf \
		-eval:HCLoF $(REFVCF_PATH)/$(SAMPLE).hiconflof.hg19.vcf \
                -eval:LCLoF $(REFVCF_PATH)/$(SAMPLE).loconflof.hg19.vcf \
                -eval:AllLoF $(REFVCF_PATH)/$(SAMPLE).lof.hg19.vcf \
		-noEV -EV CountVariants -noST -ST Filter -ST EvalRod -o $@ 
	 
%vScienceLoF.vcf : $(REFVCF_PATH)/$(SAMPLE).lof.hg19.vcf $$*.likelylof.vcf
	gatk -R hg19 CombineVariants -V:$* $*.likelylof.vcf -V:LoF $< $(if $(INTERVAL),-L $(INTERVAL)) -strict -o $@

%-novelvScienceLoF.vcf : %vScienceLoF.vcf
	gatk -R hg19 SelectVariants -V:$* $< --excludeFiltered -select 'set == "$*"' -o $@

define LOF_RSCRIPT
suppressPackageStartupMessages(library(annotateR, quietly=F));
args <- commandArgs(trailingOnly=T);
validation <- ldply(args, function(arg) { 
 d <- annotateR:::.listOfGATKReports(arg)$$ValidationReportWithFilter;
 r1 <- subset(d, CompRod == "HCLoF" & str_detect(EvalRod,"-lof$$") & Filter == "raw", select=c("EvalRod", "TP","FN","nDifferentAlleleSites"));
 r2 <- subset(d, CompRod == "HCLoF" & str_detect(EvalRod,"-lof$$") & Filter == "filtered", select=c("FN"));
 data.frame(r1[,c("EvalRod","TP","FN")], FNwFilt=r2$$FN, DiffAllele=r1$$nDifferentAlleleSites, sensititivity=r1$$TP/(r1$$TP + r1$$FN))
});
write.table(validation, stdout(), quote=F, sep="\t", row.names=F);
endef

LOF_REXPR=$(subst $(newline), ,$(LOF_RSCRIPT))


# Helper for printing inter/intra-run concordance results
# This is not relevant for all samples... further it is intended to be used interactively, e.g. make inter EVAL=r1-1-1

inter : $(EVAL)vr*.concordance.gatkreport
	Rscript -e '$(CONCORDANCE_REXPR)' $^


$(foreach test, $(RUNS),$(foreach truth, $(filter-out $(test),$(RUNS)),$(eval $(call COMPARE_template,$(test),$(truth)))))

OneKGOmniReport   = $(1)v1KGOmni25.concordance.gatkreport $(1)v1KGOmni25.validation.gatkreport
InHouseOmniReport = $(1)vOmni25.concordance.gatkreport $(1)vOmni25.validation.gatkreport
AutismReport      = $(1)vASDSanger.concordance.gatkreport $(1)vASDSanger.validation.gatkreport
AutismVCF         = $(1)vASDSanger.vcf
CompareReport     = $(1)v$(2).concordance.gatkreport

compare : $(foreach test, $(RUNS),$(foreach truth, $(filter-out $(test),$(RUNS)),$(call CompareReport,$(test),$(truth))))

omni : $(foreach samp, $(RUNS),$(call InHouseOmniReport,$(samp)))

g1komni : $(foreach samp, $(RUNS),$(call OneKGOmniReport,$(samp))) 

autism : $(foreach samp, $(RUNS),$(call AutismReport,$(samp))) $(foreach samp, $(RUNS),$(call AutismVCF,$(samp)))

histogram : $(SAMPLE).combined.histogram.gatkreport

.PHONY: all compare omni g1komni autism histogram


.SECONDARY :
