###################################################################################
# Makefile for running next generation sequencing workflow, including mapping, 
# variant calling, QC, and annotation.
#
# This makefile is intended to be included in a project-specific makefile that sets
# the following required variables:
#
#   PROJECT : Name of the project
#   INPUT   : Path to XML file specifying analysis inputs 
#   
# and any optional variables. For more information, type 'make help'.
###################################################################################   
SHELL := /bin/bash -e -o pipefail

REFERENCE ?=hg19

# Adjust resources based on reference. Note, setting parameter to nothing, e.g, ?=, will
# be treated as NULL (and is not used in the various commands). Any resource that doesn't exist 
# for a genome should be set to NULL in this way.
ifeq ("$(REFERENCE)", "b37")
	REFERENCE_FASTA ?=$(GATKRSRC_ROOT)/human_g1k_v37.fasta
	REFSEQ_BED      ?=
	SNPEFF_GENOME   ?=GRCh37.66
	GERP_VCF        ?=
	PHYLOP_VCF      ?=
endif
ifeq ("$(REFERENCE)", "hg18")
	REFERENCE_FASTA ?=$(GATKRSRC_ROOT)/Homo_sapiens_assembly18.fasta
	REFSEQ_BED      ?=
	SNPEFF_GENOME   ?=
	DBNSFP_VCF      ?=
	GERP_VCF        ?=
	PHYLOP_VCF      ?=
endif
ifeq ("$(REFERENCE)", "mm9")
	REFERENCE_FASTA ?=$(MOUSERSRC_ROOT)/ucsc.mm9.fasta
	DBSNP_VCF       ?=$(MOUSERSRC_ROOT)/dbsnp_132.mm9.vcf
	INDELS_VCF      ?=
	HAPMAPSNPS_VCF  ?=
	OMNISNPS_VCF    ?=
	REFSEQ_BED      ?=
	SNPEFF_GENOME   ?=NCBIM37.66
	DBNSFP_VCF      ?=
	GERP_VCF        ?=$(CONSERVE_ROOT)/gerp.mm9.vcf
	PHYLOP_VCF      ?=$(CONSERVE_ROOT)/phylop.mm9.vcf
endif
ifeq ("$(REFERENCE)", "mm10")
	REFERENCE_FASTA ?=$(MOUSERSRC_ROOT)/ucsc.mm10.fasta
	DBSNP_VCF       ?=$(MOUSERSRC_ROOT)/dbsnp_132.mm10.vcf
	INDELS_VCF      ?=
	HAPMAPSNPS_VCF  ?=
	OMNISNPS_VCF    ?=
	REFSEQ_BED      ?=
	SNPEFF_GENOME   ?=
	DBNSFP_VCF      ?=
	GERP_VCF        ?=
	PHYLOP_VCF      ?=
endif


REFERENCE_FASTA ?=$(GATKRSRC_ROOT)/ucsc.hg19.fasta
DBSNP_VCF       ?=$(GATKRSRC_ROOT)/dbsnp_135.$(REFERENCE).vcf
INDELS_VCF      ?=$(GATKRSRC_ROOT)/Mills_and_1000G_gold_standard.indels.$(REFERENCE).sites.vcf 
HAPMAPSNPS_VCF  ?=$(GATKRSRC_ROOT)/hapmap_3.3.$(REFERENCE).sites.vcf
OMNISNPS_VCF    ?=$(GATKRSRC_ROOT)/1000G_omni2.5.$(REFERENCE).sites.vcf
REFSEQ_BED      ?=$(GATKRSRC_ROOT)/refseq.exons.hg19.merged.bed $(GATKRSRC_ROOT)/refseq.codingexons.hg19.merged.bed
SNPEFF_GENOME   ?=hg19 hg19.gencode12
DBNSFP_VCF      ?=$(DBNSFP_ROOT)/$(shell cat $(DBNSFP_ROOT)/VERSION).$(REFERENCE).vcf
GERP_VCF        ?=$(CONSERVE_ROOT)/gerp.hg19.vcf
PHYLOP_VCF      ?=$(CONSERVE_ROOT)/phylop.hg19.vcf

PIPELINE ?=GENOME
QUALITYFORMAT ?=Standard

ifeq ($(PIPELINE),PACBIO)
EXPAND    ?=500
DELETIONS ?=0.8
MBQ       ?=10
else
EXPAND ?=50
endif

# If just phasing pedigree is specified, use that as the pedigree as well
ifdef PHASING_PEDIGREE
PEDIGREE ?=$(PHASING_PEDIGREE)
endif


QSCRIPTS_ROOT ?=$(GATKQUEUE_HOME)/qscripts

THREADS ?=1
SCATTERGATHER ?=1
RETRY ?=1

OUT_DIR ?=results
REPORT_DIR ?=$(OUT_DIR)/report
TMP_DIR ?=/scratch/tmp

QUEUE_MEM ?=4096m

RUN_ARGS += -K $(GATK_KEYFILE) -retry $(RETRY) -jobSGDir $(OUT_DIR)/.queueScatterGather

ifdef EMAIL
RUN_ARGS += -emailHost smtp.mountsinai.org -emailPort 25 -emailTLS -statusTo $(EMAIL) -statusFrom do-not-reply@login.hpc.mssm.edu
endif

ifdef startFromScratch
RUN_ARGS += -startFromScratch
endif

ifndef dry
RUN_ARGS += -run
endif

ifndef local
RUN_ARGS += -jobRunner Torque
endif	

ifdef status
RUN_ARGS += -status 
endif

ifdef debug
RUN_ARGS += -l DEBUG
endif

ifdef keepIntermediates
RUN_ARGS += -keepIntermediates
endif

$(OUT_DIR) :
	mkdir -p $(OUT_DIR)

$(OUT_DIR)/$(PROJECT).cohort.list : $(INPUT) | $(OUT_DIR) 
ifeq ($(PIPELINE),PACBIO)
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/PacbioAlignAndRecalibrateQ.scala \
		-jobReport $(OUT_DIR)/AlignAndRecalibrateQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -nt $(THREADS) -sg $(SCATTERGATHER) -expand $(EXPAND) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		-p $(PROJECT) -I $(INPUT) $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/AlignAndRecalibrateQ.makefile.log 
else
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/AlignAndRecalibrateQ.scala \
		-jobReport $(OUT_DIR)/AlignAndRecalibrateQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -nt $(THREADS) -sg $(SCATTERGATHER) -expand $(EXPAND) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		$(if $(INDELS_VCF),-indels $(INDELS_VCF)) -quality $(QUALITYFORMAT) \
		-p $(PROJECT) -I $(INPUT) $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/AlignAndRecalibrateQ.makefile.log 
endif

bams : $(OUT_DIR)/$(PROJECT).cohort.list

$(OUT_DIR)/$(PROJECT).combined.vcf : $(OUT_DIR)/$(PROJECT).cohort.list
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/VariantCallingQ.scala \
		-jobReport $(OUT_DIR)/VariantCallingQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -nt $(THREADS) -sg $(SCATTERGATHER) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		$(if $(HAPMAPSNPS_VCF),-hapmap_snps_file $(HAPMAPSNPS_VCF)) \
		$(if $(OMNISNPS_VCF),-omni_snps_file $(OMNISNPS_VCF)) \
		$(if $(INDELS_VCF),-standard_indels_file $(INDELS_VCF)) \
		$(if $(EMIT_ALL_SNPS),-emit_all_snp_sites $(EMIT_ALL_SNPS)) $(if $(EMIT_ALL_INDELS),-emit_all_indel_sites $(EMIT_ALL_INDELS)) \
		-expand $(EXPAND) $(if $(MBQ),-mbq $(MBQ)) $(if $(DELETIONS),-deletions $(DELETIONS)) \
		-p $(PROJECT) $(if $(INTERVAL),-L $(INTERVAL)) -I $(OUT_DIR)/$(PROJECT).cohort.list -pipeline $(PIPELINE) $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/VariantCallingQ.makefile.log 

variants : $(OUT_DIR)/$(PROJECT).combined.vcf

seqMetrics: $(OUT_DIR)/$(PROJECT).cohort.list 
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR)  -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/DetailedBaseQualityQ.scala \
		-jobReport $(OUT_DIR)/DetailedBaseQualityQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -sg $(SCATTERGATHER) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		-p $(PROJECT) -I $< \
		$(if $(MRD), -minDepth $(MRD)) \
		$(foreach bed,$(REFSEQ_BED),-geneBed $(bed)) \
		$(if $(INTERVAL),-L $(INTERVAL)) $(if $(BAIT_INTERVAL),-bait $(BAIT_INTERVAL)) $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/DetailedBaseQualityQ.makefile.log 

variantMetrics : $(OUT_DIR)/$(PROJECT).combined.vcf
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR)  -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/DetailedVariantQualityQ.scala \
		-jobReport $(OUT_DIR)/DetailedVariantQualityQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -nt $(THREADS) -sg $(SCATTERGATHER) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		-p $(PROJECT) $(if $(INTERVAL),-L $(INTERVAL)) -expand $(EXPAND) -I $< $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/VariantQualityQ.makefile.log 

$(OUT_DIR)/$(PROJECT).combined.anno.vcf : $(OUT_DIR)/$(PROJECT).combined.vcf
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR)  -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/VariantAnnotationQ.scala \
		-jobReport $(OUT_DIR)/VariantAnnotationQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) -sg $(SCATTERGATHER) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		$(if $(SNPEFF_GENOME), -snpeff_jar $(SNPEFF_HOME)/snpEff.jar -snpeff_cfg $(SNPEFFRSRC_ROOT)/snpEff.config $(foreach genome,$(SNPEFF_GENOME),-snpeff_genome $(genome))) \
		$(if $(DBNSFP_VCF),-dbnsfp $(DBNSFP_VCF)) $(if $(GERP_VCF),-gerp $(GERP_VCF)) $(if $(PHYLOP_VCF),-phylop $(PHYLOP_VCF)) \
		$(if $(PHASING_PEDIGREE),-pedigree $(PHASING_PEDIGREE)) \
		-p $(PROJECT) $(if $(INTERVAL),-L $(INTERVAL)) -expand $(EXPAND) -I $< $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/VariantAnnotationQ.makefile.log 

annotations : $(OUT_DIR)/$(PROJECT).combined.anno.vcf

$(OUT_DIR)/$(PROJECT).pseq.ped : $(PEDIGREE)
	ped2pseq $< > $@

$(OUT_DIR)/$(PROJECT).pseq : $(OUT_DIR)/$(PROJECT).combined.anno.vcf $(if $(PEDIGREE),$(OUT_DIR)/$(PROJECT).pseq.ped)
	if [ -e $(OUT_DIR)/$(PROJECT).pseq ] ; \
	then \
		rm -rf $(OUT_DIR)/$(PROJECT).pseq $(OUT_DIR)/$(PROJECT).pseq_out ; \
	fi
	pseq $@ new-project --noweb --resources $(PSEQRSRC_ROOT) --scratch $(TMP_DIR)
	pseq $@ load-vcf --noweb --vcf $<
	$(if $(PEDIGREE),pseq $@ load-pedigree --noweb --file $(OUT_DIR)/$(PROJECT).pseq.ped)
	if [ -e $(OUT_DIR)/snpEff_annotation.meta ] ; \
	then \
		pseq $@ attach-meta --noweb --file $(OUT_DIR)/snpEff_annotation.meta --group annotation --id 0 ; \
	fi
	$(if $(PHENOTYPES),pseq $@ load-pheno --noweb --file $(PHENOTYPES))

$(OUT_DIR)/$(PROJECT).chrXstats.tsv : $(OUT_DIR)/$(PROJECT).pseq
	pseq $< i-stats --noweb --mask reg.req=chrX any.filter.ex > $@

pseq : $(OUT_DIR)/$(PROJECT).pseq

$(OUT_DIR)/$(PROJECT).fam : $(PEDIGREE)
	cut -f1-6 -d$$'\t' $< > $@

$(OUT_DIR)/$(PROJECT).admix.Q : $(OUT_DIR)/$(PROJECT).cohort.list $(OUT_DIR)/$(PROJECT).fam
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/AncestryQ.scala \
		-jobReport $(OUT_DIR)/AncestryQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) \
		-R $(REFERENCE_FASTA) -D $(DBSNP_VCF) \
		-p $(PROJECT) \
		-I $(word 1,$^) -fam $(word 2,$^) \
		--path_to_admixture admixture \
		--ancestry_reference_sites $(ADMIXRSRC_ROOT)/reference.vcf \
		--ancestry_reference_dataset $(ADMIXRSRC_ROOT)/reference \
		--ancestry_reference_population_fine $(ADMIXRSRC_ROOT)/reference_fine.pop \
		--ancestry_reference_population_coarse $(ADMIXRSRC_ROOT)/reference_coarse.pop \
		-nt $(THREADS) -sg 1 \
		$(if $(INTERVAL),-L $(INTERVAL)) $(RUN_ARGS) \
		2>&1 | tee $(OUT_DIR)/AncestryQ.makefile.log

ancestry : $(OUT_DIR)/$(PROJECT).admix.Q 


report : $(OUT_DIR)/$(PROJECT).pseq $(OUT_DIR)/$(PROJECT).chrXstats.tsv $(OUT_DIR)/$(PROJECT).admix.Q
	bash <(Rscript -e 'suppressPackageStartupMessages(library(annotateR)); preparePSEQProjectForReport("$(OUT_DIR)")') $(OUT_DIR)/$(PROJECT).pseq
	R --vanilla -e 'library(annotateR); report("$(OUT_DIR)","$(REPORT_DIR)");' 


primer: $(OUT_DIR)/$(PROJECT).cohort.list
	java -Xmx$(QUEUE_MEM) -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/MakePrimerQ.scala \
		-jobReport $(OUT_DIR)/DetailedBaseQualityQ.jobreport.gatkreport \
		-outputDir $(OUT_DIR) \
		-R /projects/ngs/resources/primer/ucsc.hg19.fasta -D /projects/ngs/resources/pseq/dev/upstream/dbsnp.hg19.vcf \
		-p $(PROJECT) \
		--validateAlleles:vcf $(OUT_DIR)/primer_test.vcf \
		--maskAlleles:vcf /projects/ngs/resources/pseq/dev/upstream/dbsnp.hg19.vcf \
		--resourcePath /projects/ngs/resources/primer \
		-nt $(THREADS) \
		$(if $(INTERVAL),-L $(INTERVAL)) $(RUN_ARGS)

all : bams variants seqMetrics variantMetrics annotations report ancestry


.PRECIOUS : \
	$(OUT_DIR)/$(PROJECT).cohort.list $(OUT_DIR)/$(PROJECT).combined.vcf \
	$(OUT_DIR)/$(PROJECT).combined.anno.vcf $(OUT_DIR)/$(PROJECT).pseq \
	$(OUT_DIR)/$(PROJECT).admix.Q 


define HELP_BODY
Usage of DNA Sequence Analysis Pipline

This makefile exposes the following targerts for orchestrating your analysis:
  'bams': Alignment and recalibration, e.g., fastq.qz => bam
  'variants': Call variants, e.g., bam => vcf
  'seqMetrics': Compute sequencing-related metrics, e.g., depth of coverage
  'variantMetrics': Compute variant metrics
  'annotations': Annotate VCF with amino-acid effects, etc.
  'all': Do all of the above in the proper sequence
For example
  make bams
will run the alignment and recalibration pipeline.

The runtime behavior of the makefile can be controlled with a number of variables:
  'dry': Compile but do not run Queue script
  'startFromScratch': Force Queue script to restart from scratch
  'local': Run locally instead of pushing jobs to the cluster queueing system
  'debug': Turn on debug output
These options are invoked by appending them to the command line with '=1', e.g.,
  make bams dry=1
to run in 'dry' mode.

The behavior of the Makefile is also controlled by a number of optional variables
you can set in your project-specific makefile:
  'INTERVAL': Path to file with intervals of interest (default null)
  'BAIT_INTERVAL': Path to file with bait intervals (used in QC metric calculation) (default null)
  'EMIT_ALL_SNPS': Path to file with VCF with SNV sites, ref and alt that should be called (default null)
  'EMIT_ALL_INDELS': Path to file with VCF with indel sites, ref and alt that should be called (default null)
  'PIPELINE': One of TARGETED, EXOME, GENOME (default GENOME)
  'THREADS': Number of threads to use for multithreaded applications in pipeline, e.g. bwa (default 1)
  'SCATTERGATHER': Number of scatter gather chunks (default 1)
  'RETRY': Number of times to retry jobs that fail (default 1)
  'OUT_DIR': Where to write output files (default ./results)
  'EMAIL': Email address to which Queue should send progress updates (default null). Note Queue can send a lot of emails.
  'PEDIGREE': PED format pedigree file	
  'PHASING_PEDIGREE': PED format subset of the pedigree file with just trios to be phased
  'PHENOTYPES': PlinkSeq phenotype file phenotype information (http://atgu.mgh.harvard.edu/plinkseq/input.shtml#phe)

Additional documentation can be found on the project wiki at https://bitbucket.org/multiscale/ngs/wiki/Home
endef

export HELP_BODY

help:
	@echo "$$HELP_BODY"


.PHONY : help all bams variants seqMetrics variantMetrics annotations pseq report ancestry
