SHELL := /bin/bash -e -o pipefail

TMP_DIR ?=/scratch/tmp
RETRY   ?=1


RUN_ARGS += -K $(GATK_KEYFILE) -retry $(RETRY) --disableJobReport

ifdef startFromScratch
RUN_ARGS += -startFromScratch
endif

ifndef dry
RUN_ARGS += -run
endif

ifndef local
RUN_ARGS += -jobRunner Torque
endif	

ifdef status
RUN_ARGS += -status 
endif

references :
	java -Xmx4096m -Djava.io.tmpdir=$(TMP_DIR) -jar $(QUEUE_JAR) -S $(QSCRIPTS_ROOT)/PrepareResourceDirectoryQ.scala \
		$(RUN_ARGS)
