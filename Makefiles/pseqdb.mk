LOC_UPSTREAM_ROOT ?=.
SEQ_UPSTREAM_ROOT ?=.
REF_UPSTREAM_ROOT ?=.

REFSEQ        ?=$(LOC_UPSTREAM_ROOT)/refseq-hg19.gtf.gz
GENCODE       ?=$(LOC_UPSTREAM_ROOT)/gencodeBasicV11-hg19.gtf.gz
CCDS          ?=$(LOC_UPSTREAM_ROOT)/ccds-hg19.gtf.gz
REFSEQ2SYMBOL ?=$(LOC_UPSTREAM_ROOT)/refseq2symbol.txt
OMIM_GENES    ?=$(LOC_UPSTREAM_ROOT)/omim.genes.reg.gz

HG19FASTA     ?=$(LOC_UPSTREAM_ROOT)/ucsc.hg19.fasta

DBSNP         ?=$(REF_UPSTREAM_ROOT)/00-All.vcf.gz
G1K           ?=$(REF_UPSTREAM_ROOT)/ALL.wgs.phase1_release_v3.20101123.snps_indels_sv.sites.vcf.gz
HGMD_SNVS     ?=$(REF_UPSTREAM_ROOT)/hgmd.snvs.txt
HGMD_INDELS   ?=$(REF_UPSTREAM_ROOT)/hgmd.indels.txt
PGKB_SNVS     ?=$(REF_UPSTREAM_ROOT)/pharmgkb.snvs.txt
OMIM_SNVS     ?=$(REF_UPSTREAM_ROOT)/omim.snvs.txt
EVS           ?=$(REF_UPSTREAM_ROOT)/ESP6500.snps.vcf

locdb :
	pseq . loc-load --file $(REFSEQ) --keep-unmerged --group refseq --locdb $@
	pseq . loc-load --file $(CCDS) --keep-unmerged --group ccds --locdb $@
	pseq . loc-load --file $(GENCODE) --keep-unmerged --group gencode --locdb $@
	pseq . loc-swap-names --file $(REFSEQ2SYMBOL) --group refseq --alternate-name --locdb $@ 
	pseq . loc-load-alias --file $(REFSEQ2SYMBOL) --locdb $@
	pseq . loc-load --file $(OMIM_GENES) --group omim_genes --options PHENOCAT=integer --locdb $@

seqdb :
	pseq . seq-load --file $(HG19FASTA) --seqdb $@ --name hg19 --description "UCSC hg19 build" --format build=hg19 repeat-mode=lower

refdb :
	pseq . ref-load --file $(HGMD_SNVS) --description "HGMD SNVs" --group hgmd_snvs --format integer=TYPE --refdb $@
	pseq . ref-load --file $(HGMD_INDELS) --description "HGMD Indels" --group hgmd_indels --format integer=TYPE --refdb $@
	pseq . ref-load --file $(PGKB_SNVS) --description "PharmGKB clinically annotated SNVs" --group pharmgkb_snvs \
		--format integer=STRENGTH --refdb $@
	pseq . ref-load --file $(OMIM_SNVS) --description "OMIM SNVs" --group omim_snvs --refdb $@
	pseq . ref-load --vcf $(EVS) --description "Exome sequencing project/EVS" --group evs --refdb $@ \
		--format include-meta=AF,AFR_AF,EUR_AF
	pseq . ref-load --vcf $(DBSNP) --group dbsnp --refdb $@ \
		--format include-meta=AF,GMAF,dbSNPBuildID,CLN 
	pseq . ref-load --vcf $(G1K) --group g1k --refdb $@ \
		--format include-meta=AF,AMR_AF,ASN_AF,AFR_AF,EUR_AF
